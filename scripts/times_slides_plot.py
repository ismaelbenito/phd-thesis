import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr

files = [*(f"slides_time_registry_0{i}.csv" for i in range(5)), "slides_time_registry.csv"]
series = [pd.read_csv(f).set_index("timestamps") for f in files]

fig = plt.figure(dpi=200)
ax = fig.add_subplot(111)

presentation = series[-1]
series = np.array(series)[[0, 2, 3]]
series[2].index += np.cumsum(0.01 * np.random.random(series[2].values.shape))  # patch ints

total_slides = presentation.max()
total_slides_vector = range(0, int(total_slides)+1)
total_time = 50 
total_time_vector = np.linspace(0, total_time, int(total_slides)+1)

ax.plot(total_time_vector, total_slides_vector, color="red", linewidth=0.75,label="Reference 50 min",zorder=1, linestyle="--")

arrays = [s.slides.to_xarray().interp(timestamps=total_time_vector*60) for s in series]

y = xr.concat(arrays, dim=pd.Index([1, 2, 3], name="rehearsals"))
y_fit = total_slides_vector

# residual sum of squares
ss_res = ((y - y_fit) ** 2).sum("timestamps")

# total sum of squares
ss_tot = ((y - y.mean("timestamps")) ** 2).sum("timestamps")

# r-squared
r2 = 1 - (ss_res / ss_tot)


for k, s in enumerate(series):
    s = s.rename(columns={"slides": f"Rehearsal {k+1}, $R^2={round(r2.values[k], 3)}$"})
    s.index /= 60
    s.rolling(5).mean().plot.line(ax=ax, linewidth=0.75, linestyle="-", zorder=2)

s = presentation

y = s.slides.to_xarray().interp(timestamps=total_time_vector*60)
ss_res = ((y - y_fit) ** 2).sum("timestamps")
ss_tot = ((y - y.mean("timestamps")) ** 2).sum("timestamps")
r2 = 1 - (ss_res / ss_tot)

s = s.rename(columns={"slides": f"Presentation, $R^2={round(r2.values.tolist(), 3)}$"})
s.index /= 60
s.plot.line(ax=ax, color="black", linewidth=0.75, zorder=3)

# ax.plot(total_time_vector, total_slides_vector, color="red", linewidth=0.75,label="Reference 50 min",zorder=1, linestyle="--")
ax.set_xlabel("Minutes")
ax.set_ylabel("Slides")
ax.legend()
