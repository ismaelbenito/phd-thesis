import datetime
import time

import numpy as np
import pandas as pd
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
from pynput import keyboard

plt.ion()


def time_between_press():
    t0 = time.time()
    # The event listener will be running in this block
    with keyboard.Events() as events:
        for event in events:
            if event.key == keyboard.Key.esc:
                break
            else:
                if isinstance(event, keyboard.Events.Press):
                    t1 = time.time()
                    dt = t1 - t0
                    t0 = t1
                    yield dt, event


def strfdelta(tdelta, fmt):
    d = {"days": tdelta.days}
    d["hours"], rem = divmod(tdelta.seconds, 3600)
    d["minutes"], d["seconds"] = divmod(rem, 60)
    return fmt.format(**d)


slide = 0
time_registry = []

for dt, event in time_between_press():
    dt_str = strfdelta(datetime.timedelta(seconds=dt), "{hours}:{minutes}:{seconds}")
    if event.key == keyboard.Key.page_down or \
        event.key == keyboard.Key.right or \
        event.key == keyboard.Key.down:
        slide += 1     
    elif event.key == keyboard.Key.page_up or \
        event.key == keyboard.Key.left or \
        event.key == keyboard.Key.up:
        slide += -1
    time_registry.append((slide, dt))
    print(f"{slide:0>3}: {dt_str}")

slides, timedeltas = tuple(zip(*time_registry))

timestamps = np.hstack((0, timedeltas)).cumsum() 

time_registry_series = pd.Series(
    data=list(slides),
    index=pd.Index(timestamps[1:], name="timestamps"),
    name="slides",
)

time_registry_series.to_csv("slides_time_registry.csv")

# fig = plt.figure()
# ax = fig.add_subplot(111)
# time_registry_series.plot.line(ax=ax)



