\section{Data representation}

\subsection{Color spaces}
\label{subsec:colorspaces}

In \autoref{sec:theimageconsistency} we introduced the image consistency problem alongside with a simplified description of the reflectance model (see~\autoref{fig:colorreproduction_mini}):

\begin{equation}
   \mathrm {S_k \propto \int_{-\infty}^{\infty} I(\uplambda) \ R(\uplambda) \ D_k(\uplambda) \ d\uplambda}
   \label{eq:colorsingalintegral_bis}
\end{equation}

\noindent
where a certain light source $ \mathrm{I} (\uplambda) $ illuminates a certain object with a certain reflectance $ \mathrm{R} (\uplambda) $ this scene is captured by a sensor with its  response $ \mathrm{D_k} (\uplambda) $; and $ \mathrm {S_k}  $ represents the signal captured by this sensor. This model specifically links the definition of color to the sensor response, not only to the wavelength distribution of the reflected light. Thus, our color definition depends on the observer. 

\begin{marginfigure}
    \centering
    \includegraphics[trim={0 0 125px 20px},clip,width=\linewidth]{chapters/chapter3/figures/jpg/2.jpg}
    \caption{A reduced representation of the reflectance model. For more details see ~\autoref{fig:colorreproduction}.}
    \label{fig:colorreproduction_mini}
\end{marginfigure}

Let the sensor $ \mathrm{D_k} (\uplambda) $ be the human eye, then this model becomes the well-known tristimulus model of the human eye. In the tristimulus model, a \textit{standard observer} is defined from studying the human vision. In 1931 the International Commission of Illumination defined the CIE 1931 RGB and CIE 1931 XYZ color spaces based on the human vision \citep{Fairman1997,ASTM_CIE}. Since then, the model has been revisited many times defining new color spaces: in 1960 \citep{Fridge1960}, in 1964 \citep{Wyszecki1963}, in 1976 \citep{Robertson1977} and so on \citep{schanda2007colorimetry}.


Commonly, color spaces referred to a \textit{standard observer} are called \textit{device-independent color spaces}. As explained before, we are going to use images which are captured by digital cameras. These images will use \textit{device-dependent color spaces}, despite the efforts of their manufacturers to solve the color reproduction problem, as they try to match the camera sensor to the tristimulus model of the human eye \citep{Long2019}. 

Let a color $ \mathbf{s} $ be defined by the components of the camera sensor: 

\begin{equation}
    \mathbf{s} = (\mathrm{S_r}, \ \mathrm{S_g}, \ \mathrm{S_b})
    \label{eq:scolor}
\end{equation}

\noindent
where $ \mathrm{S_r}$ , $\mathrm{S_g}$ and $ \mathrm{S_b} $ are the responses of the three sensors of the camera for the \textit{red}, \textit{green} and \textit{blue} channels, respectively. Cameras do imitate the human tristimulus vision system by placing sensors in the wavelength bands representing those where human eyes have more sensitivity. 

\newpage

Note that $ \mathbf{s}$ is defined as a vector in \autoref{eq:scolor}. Although, its definition lacks the specification of its vector space: 

\begin{equation}
    \mathbf{s} = (r, \ g, \ b) \ \in \mathbb{R}^3
    \label{eq:scolor_vector}
\end{equation}

\noindent
where $ r $, $ g $, $ b $ is a simplified notation of the channels of the color, and $ \mathbb{R}^3 $ is a generic \textit{RGB color space}. As digital cameras store digital information in a finite discrete representation, $ \mathbb{R}^3 $ should become $ \mathbb{N}^3_{[0, 255]} $ for 8-bit images (see~\autoref{fig:rgb_cube}). This discretization process of the measured signal in the camera sensor is a well-known phenomenon in signal-processing, it is called \textit{quantization} \citep{Braquelaire1997}.  All to all, we can write some common color spaces in this notation: 

\begin{marginfigure}
    \centering
    \includegraphics[width=0.5\textwidth]{chapters/chapter3/figures/rgb_cube.jpg}
    \caption{125 colors of an RGB color space. Each channel of the color space has been sampled 5 times. Assuming the space is a 24-bit color space, the values of the sampled colors correspond to: \0, \textnum{61}, \textnum{127}, \textnum{193} and \textnum{255}. The combination \textnum{(255, 255, 255)} is the white color, and \textnum{(0, 0, 0)} the black color. }
    \label{fig:rgb_cube}
\end{marginfigure}

\begin{itemize}
    \item $ \mathbb{N}_{[0, 255]} $ is the grayscale color space of 8-bit images.
    \item $ \mathbb{N}^3_{[0, 255]} $ is the RGB color space of 24-bit images (8-bits/channel).
    \item $ \mathbb{N}^3_{[0, 4096]} $ is the RGB color space of 36-bit images (12-bits/channel).
    \item $ \mathbb{N}^3_{[0, 65536]} $ is the RGB color space of 48-bit images (16-bits/channel).
    \item $ \mathbb{N}^4_{[0, 255]} $ is the CMYK color space of 32-bit images (8-bits /channel).
    \item $ \mathbb{R}^3_{[0, 1]} $ is the RGB color space of a normalized image, specially useful when using computer vision algorithms.
\end{itemize}

\subsection{Color transformations}

The introduction of color spaces as vector spaces brings all the mathematical framework of geometric transformations. We can now define a \textit{color conversion} as the application between two color spaces. 

For example, let $ f $ be a color conversion between an RGB and a CMYK space:

\begin{equation}
    f: \mathbb{N}^3_{[0, 255]} \to \mathbb{N}^4_{[0, 255]}
\end{equation}

this color conversion can take any form. In \autoref{sec:theimageconsistency}, we saw that the reflectance spectra of the image of an object would be a linear combination of the inks reflectance spectra used to reproduce that object. If we recover that expression from \autoref{eq:reproductionsum} and combine it with the RGB color space from \autoref{eq:scolor_vector}, we obtain:

\begin{equation}
   \mathrm{R'(\uplambda)} = \sum_{j}^{c,m,y,k} f_j (r, g, b) \cdot \mathrm{R_j(\uplambda)} 
   \label{eq:reproductionsum_bis}
\end{equation}

Now, $ \mathrm{R'(\uplambda)} $ is a linear combination of the reflectance spectra of the \textit{cyan}, \textit{magenta}, \textit{yellow} and \textit{black} inks. The weights of the combination is the CMYK color derived from the RGB color. 


In turn, we can express the CMYK color also as a linear combination of the RGB color channels, $ f_i (r, g, b) $ is our color correction here, then: 

\begin{equation}
   \mathrm{R'(\uplambda)} = \sum_{j}^{c,m,y,k} \left[ \sum_{k}^{r,g,b} a_{jk} \cdot k \right] \cdot \mathrm{R_j(\uplambda)} 
   \label{eq:reproductionsum_rgb}
\end{equation}

Note that we have defined $ f_i $ as a linear transformation between the RGB and the CMYK color spaces. Doing so is the most common way to perform color transformations between color spaces. 

These are the foundations of the ICC Profile standard \citep{nielsen1998creation}. \textit{Profiling} is a common technique when reproducing colors. For example, take \autoref{fig:rgb_cube}, if the colors are seen displayed on a screen they will show the RGB space of the LED technology of the screen. However, if they have been printed, the actual colors the reader will be looking at will be the linear combination of CMYK inks representing the RGB space, following \autoref{eq:reproductionsum_rgb}. Therefore, ICC profiling is present in each color printing process. 

Alongside with the described example, here below, we present some of the most common color transformations we will use during the development of this thesis, that include \textit{normalization}, \textit{desaturation}, \textit{binarization} and \textit{colorization} transformations. 

\subsubsection{Normalization}

Normalization is the process of mapping a discrete color space with limited resolution ($ \mathbb{N}_{[0, 255]} $, $\mathbb{N}^3_{[0, 255]} $, $ \mathbb{N}^3_{[0, 4096]} $, ...) into a color space which is limited to a certain range of values, normally from 0 to 1 $\mathbb{R}_{[0, 1]}$,  but offers theoretically infinite resolution \footnote{The infinite resolution that represents $ \mathbb{R} $ is not computationally feasible. However, the computational representation of a $ \mathbb{R} $ space, a \texttt{float} number, handles a higher precision than other discrete space before normalization.}. All our computation will take place in such normalized spaces. Formally the normalization process is a mapping that follows:

\begin{equation}
    f_{normalize} : \mathbb{N}^K_{[0, \ 2^n]} \to \mathbb{R}^K_{[0, 1]}
    \label{eq:color_normalize}
\end{equation}

\setlength{\parskip}{1em}

\noindent
where $ K $ is the number of channels of the color space (i.e. $ K = 1 $ for grayscale, $K = 3$ for RGB color spaces, etc.) and $ n $ is the bit resolution of the color space (i.e. 8, 12, 16, etc.). 

Note that a normalization mapping might not be that simple so only implies a division by a constant. For example, an image can be normalized using an exponential law to compensate camera acquisition sensitivity, etc. \citep{Morgan2014, Veluchamy2019}. 

\break \setlength{\parskip}{1em}

\subsubsection{Desaturation}

Desaturation is the process of mapping a color space into a grayscale representation of this color space. Thus, formally this mapping will always be a mapping from a vector field to a scalar field. We will assume the color space has been previously normalized following a mapping (see~\autoref{eq:color_normalize}). Then:

\begin{equation}
    f_{desaturate} : \mathbb{R}^K_{[0, 1]} \to \mathbb{R}_{[0, 1]}
    \label{eq:color_desaturate}
\end{equation}

\noindent
where $ K $ is still the number of channel the input color space has. There exist several ways to desaturate color spaces, for example, each CIE standard incorporates different ways to compute their luminance model \citep{ASTM_CIE}. 

\subsubsection{Binarization}

Binarization is the process of mapping a grayscale color space into a binary color space, this means the color space gets reduced only to a representation of two values. Formally:

\begin{equation}
    f_{binarize} : \mathbb{R}_{[0, 1]} \to \mathbb{N}_{[0, 1]}
    \label{eq:color_binarize}
\end{equation}

Normally, these mappings need to define some kind of \textit{threshold} to split the color space representation into two subsets. Thresholds can be as simple as a constant threshold or more complex \citep{Roy2014}. 

\subsubsection{Colirization}

Colorization is the process of mapping a grayscale color space into a full-featured color space. We can define a colorization as: 

\begin{equation}
    f_{colorize} : \mathbb{R}_{[0, 1]} \to \mathbb{R}^K_{[0, 1]}
    \label{eq:color_colorize}
\end{equation}

\noindent
where $ K $ is now the number of channels the output color space has. This process is more unusual than the previous mappings presented here. It is often implemented in those algorithms that pursue image restoration \citep{Xiang2009}. In this work, colorization will be of a special interest in \autoref{ch:5}.

\break \setlength{\parskip}{1em}

\subsection{Images as bitmaps}

A digital image is the result of capturing a scene with an array of sensors, e.g. the camera \citep{Hunt2005}, following \autoref{eq:colorsingalintegral_bis}. A monochromatic image $ I $, means we only have one color channel in our color space. This image can be seen as a mapping between a vector field, the 2D plane of the array of sensors, and a scalar field, the intensity of light captured by each sensor: 

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter3/figures/jpg/airy_disc_profile.jpg}
    \includegraphics[width=\textwidth]{chapters/chapter3/figures/jpg/airy_disc_3D.jpg}
    \caption{An Airy disk is shown (top) as a grayscale image with a color map and (bottom) as a function with the same color map.}
    \label{fig:img_vs_profile}
\end{marginfigure}

\begin{equation}
        I: \R^2 \to \R
        \label{eq:image_mapping}
\end{equation}

\noindent
where $ \R^2 $ is the capture plane of the camera sensors and $ \R $ is a generic grayscale color space. \autoref{fig:img_vs_profile} shows an example of this: an Airy disk \citep{greivenkamp2004field} is represented first as an image, where the center of the disk is visualized as a spot; also, the Airy disk is shown to be a function of the space distribution. 

Altogether, we can extend \autoref{eq:image_mapping} definition to images that are not grayscale. This means each image can be defined as a mapping from the 2D plane of the array of sensors to a color space, which is in turn also a vector space: 

\begin{equation}
        I: \R^2 \to \R^K
\end{equation}

\noindent
where $ \R^K $ is now a vector field also, thus the color space of the image can be RGB, CMYK, etc. Note digital cameras can capture more than the above-mentioned color bands, and there exists a huge field of multi-spectral cameras \citep{Yokoya2017}, which is not the focus of our research.  

As we pointed out when defining color spaces, digital images are captured using discrete variable color spaces. But this process also affects the spatial domain of the image. The process of discretizing the plane $ \R^2 $  is called \emph{sampling}. And, the process of discretizing the illumination data in $ \R $ data is called \emph{quantization}. Following this, \autoref{eq:image_mapping} can be rewritten as: 

\begin{equation}
        I: \fin{[0, n]} \times \fin{[0,m]} \to \fin{[0, 255]}
        \label{eq:image_mapping_discrete}
\end{equation}

\noindent
which represents an 8-bit grayscale\footnote{This example uses a grayscale image of 8-bit resolution, however any of the formats specified in the subsection 3.3.2 could be used here.} image of size $ (n, m) $. This definition of an image allows us to differentiate the \textit{domain} transformations of the image (i.e. geometrical transformations to the perspective of the image); from the \textit{image} transformations (i.e. color corrections to the color space to the image). 

\break \setlength{\parskip}{1em}

In \autoref{ch:4}, when dealing with the extraction of QR Codes from challenging surfaces we used the definition in \autoref{eq:image_mapping} to refer to the capturing plane of the image and how it relates to the underneath surface where the QR Code is placed by projective laws. 

In \autoref{ch:5} we used the definition of \autoref{eq:image_mapping_discrete} to detail our proposal for the encoding process of colored QR Codes. In this context, it is interesting reducing the notation of image definition taking into account that images can be regarded as matrices. So, \autoref{eq:image_mapping_discrete} can be rewritten in a compact form as: 

\begin{equation}
        I \in [0, 255]^{n \times m}
\end{equation}

where $ I $ is now a matrix which exist in a matrix space $ [0, 255]^{n \times m} $. This vector space contains both the definition of the spatial coordinates of the image and the color space. 


As before, we can use this notation to represent different image examples:

\begin{itemize}
    \item $ I \in [0, 255]^{n \times m} $, is an 8-bit grayscale image with size $ (n, m) $.
    \item $ I \in [0, 255]^{n \times m \times 3} $, is an 8-bit RGB image with size $ (n, m) $.
    \item $ I \in [0, 1]^{n \times m} $, is a float normalized grayscale image with size $ (n, m) $.
    % \item $ G \in [0, 255]^{l \times 3} $, is a set of 8-bit RGB (in total 24-bit) colors with length $ l $.
    \item $ I \in \{0, 1\}^{n \times m} $, is a binary image with size $ (n, m) $.
\end{itemize}


Finally, we can reintroduce the color space transformations presented before, from \autoref{eq:color_normalize} to \autoref{eq:color_colorize}, for images as bitmap matrices: 

\begin{itemize}
    \item \textbf{Normalization:} 
        \begin{equation}
            % \label{eq:normalizationdef}
            f_{normalize}: [0,255]^{n \times m \times 3} \to [0,1]^{n \times m \times 3} 
        \end{equation}
    \item \textbf{Desaturation:} 
        \begin{equation}
        % \label{eq:desaturationdef}
        f_{desaturate}: [0,1]^{n \times m \times 3} \to [0,1]^{n \times m}
        \end{equation}
    \item \textbf{Binarization:} 
        \begin{equation}
        % \label{eq:binarizationdef}
         f_{binarize}: [0,1]^{n \times m} \rightarrow \{0,1\}^{n \times m}
        \end{equation}
    \item \textbf{Colorization:} 
        \begin{equation}
        % \label{eq:binarizationdef}
         f_{colorize}: [0,1]^{n \times m} \rightarrow [0,1]^{n \times m \times 3} 
        \end{equation}
\end{itemize}

% \textbf{Normalization,} the 8-bit color channels (RGB) are mapped to a normalized color representation: 

% \begin{equation}
% \label{eq:normalizationdef}
% f_{normalize}: [0,255]^{l \times 3} \to [0,1]^{l \times 3} 
% \end{equation}

% \textbf{Desaturation,} the color channels (RGB) are then mapped into a monochromatic grayscale channel (L): 

% \begin{equation}
% \label{eq:desaturationdef}
% f_{grayscale}: [0,1]^{l \times 3} \to [0,1]^{l}
% \end{equation}

% \textbf{Binarization,} the monochromatic grayscale channel (L) is converted to a monochromatic binary channel (B): 

% \begin{equation}
% \label{eq:binarizationdef}
%  f_{threshold}: [0,1]^{l} \rightarrow \{0,1\}^{l} 
% \end{equation}

% \textbf{Colorization,} the binary values of the palette colors represent the affinity to black (zero) and white (one) and can be used to create a mapping between the position in the color palette list and the position inside the QR Code matrix (a binary image). This mapping will also depend on the geometry of the QR Code matrix (where are the black and white pixels placed) and an additional matrix that protects the key zones of the QR Code (a mask which defines the key zones):  

% \begin{equation}
% \label{eq:mappingdef}
% f_{mapping} : \{0,1\}^{l} \times \{0,1\}^{n\times m} \times \{0, 1\}^{n \times m} \to  \{0, \dotsc, l+1\}^{n \times m} 
% \end{equation}

% Once the mapping is computed, a function is defined to finally colorize the QR Code, which renders an RGB image of the QR Code with embedded colors: 

% \begin{equation}
% \label{eq:colorizationdef}
% f_{color} : \{0,1\}^{n\times m} \times [0,1]^{l \times 3} \times \{0, \dotsc, l+1\}^{n \times m} \to  [0,1]^{n\times m \times 3} 
% \end{equation}


\newpage
