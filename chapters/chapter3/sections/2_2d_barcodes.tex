\section{2D Barcodes: the Quick-Response Code}

Quick-Response Codes, popularized as QR Codes, are 2D barcodes introduced in 1994 by Denso Wave \citep{iso18004_2015}, which aimed at replacing traditional 1D barcodes in the logistic processes of this company. However, the use of QR Codes has escalated in many ways and are now present in manifold industries: from manufacturing to marketing and publicity, becoming a part of the mainstream culture. In all these applications, QR Codes are either printed or displayed and later acquired by a reading device, which normally includes a digital camera or barcode scanner. Also, there has been an explosion of 2D barcode standards \citep{iso16022_2006, iso24778_2008, iso16023_2000, Berchtold2020, jancke2007} (see~\autoref{fig:2dbarcodes}).


\begin{figure*}[h!t]
\centering
\includegraphics[width=\linewidth]{chapters/chapter3/figures/jpg/qrs.jpg}
    \caption{Different 2D barcode standards. From left to right: a QR Code \citep{iso18004_2015}, a DataMatrix \citep{iso16022_2006}, an Aztec Code \citep{iso24778_2008}, a MaxiCode \citep{iso16023_2000}, a JAB Code \citep{Berchtold2020} and an HCC Barcode \citep{jancke2007}. }
    \label{fig:2dbarcodes}
\end{figure*}


The process of encoding and decoding a QR Code could be considered as a form of communication through a visual channel (see~\autoref{fig:defaultqrflow}): a certain message is created, then split into message blocks, these blocks are encoded in a binary format, and finally encoded in a 2D array. This 2D binary array is an image that is transmitted through a visual channel (printed, observed under different illuminations and environments, acquired as a digital image, located, resampled, etc.). On the decoder side, the binary data of the 2D binary array is retrieved, the binary stream is decoded, and finally the original message is obtained. 

From the standpoint of a visual communication channel, many authors before explored the data transmission capabilities of the QR Codes, especially as steganographic message carriers (data is encoded in a QR Code, then encoded in an image) due to their robust error correction algorithm \citep{Al-Otum2018, Rosales-Roldan2018}.  


\begin{figure}[h!t]
\centering
\includegraphics[width=0.85\linewidth]{chapters/chapter5/figures/jpg/3.jpg}
    \caption{Block diagram for a general encoding-decoding process of a QR Code featuring the embedding of a color layer. This color layer could be used for a wide range of applications, such as placing a brand logo inside a QR Code. The process can be seen as a global encoding process (digital encode and color encode), followed by a channel (print and capture) and a global decoding process (remove colors and decode digital information). }
    \label{fig:defaultqrflow}
\end{figure}

\clearpage
\subsection{Scalability}

Many 2D barcode standards allow modulating the amount of data encoded in the barcode. For example, the QR Code standard implements different barcode versions from version 1 to version 40. Each version increases the edges of the QR Code by 4 pixels, the so-called \textit{modules}. From the starting \textnum{21 \times 21} (\textnum{v1}) modules up to \textnum{144 \times 144} modules (\textnum{v40}) \citep{iso18004_2015}. 

For each version, the location of every computer vision feature is fully specified in the standard (see~\autoref{fig:qrversions}), in \autoref{subsec:qrfeatures}  we will focus on these features. Some other 2D barcode standards are flexible enough to cope with different shapes, such as rectangles in the DataMatrix codes (see~\autoref{fig:dmversions}), which can be easier to adapt to different substrates or physical objects \citep{iso16022_2006}. 

\begin{figure}[h!t]
    \centering
    \includegraphics[width=0.75\textwidth]{chapters/chapter3/figures/jpg/qr_versions.jpg}
    \caption{Some examples of QR Code versions. From left to right: Micro QR-Code (version M3), version 3 QR Code, and version 10 QR Code. Each of them can store up to 7, 42, 213 bytes, respectively, using a 15\% of error correction capacity.}
    \label{fig:qrversions}
\end{figure}

% \vspace{1cm}

\begin{figure}[h!t]
    \centering
    \includegraphics[width=0.75\textwidth]{chapters/chapter3/figures/jpg/datamatrix_versions.jpg}
    \caption{Some examples of DataMatrix codes. From left to right: rectangular DataMatrix code, square DataMatrix code and four square DataMatrix combined. Each of them can store up to 14, 28, 202 bytes, respectively, using approximately a 20\% of error correction capacity. }
    \label{fig:dmversions}
\end{figure}

These different possible geometries must be considered when adding colors to a 2D barcode. In the case of the QR Codes and DataMatrix codes, the larger versions are built by replicating a basic squared block. Therefore, the set of color references could be replicated in each one of these blocks, to gain in redundancy and in a more local color correction. Alternatively, different sets of color references could be used in each periodic block to facilitate a more thorough color correction based on a larger set of color references.

Regarding this size and shape modularity in 2D barcode encoding, there exist a critical relationship between the physical size of the modules and the pixels in a captured image. This is a classic sampling phenomena \citep{annadurai2007fundamentals}: for a fixed physical barcode size and a fixed capture (same pixels), as the version of the QR Code increases the amount of modules in a given space increases as well. 

Thus, when the apparent size of the module in the captured image decreases, the QR Code module is hardly a bunch of image pixels, and we start to see aliasing problems \citep{Gang2012}. In turn, this problem leads to a point where QR Codes cannot be fully recognized by the QR Code decoding algorithm. This is even more important if we substitute these black and white modules with colors, where the error in finding the right reference area may lead to huge errors in the color evaluation. Therefore, this sampling problem will accompany the implementation of our proposal taking into account the size of the final QR Code depending on the application field and the typical resolution of the cameras used in those applications.


\subsection{Data encoding in QR Codes}

The QR Code standard presents a complex encoding layout (see~\autoref{fig:qrencoding}). Encoding a message into a QR Code form implies several steps. 

First, the message is encoded as binary data and split into various bytes, namely \textit{data blocks}. Since QR Codes can support different data types, the binary encoding for those data types will be different in order to maximize the amount of data to encode in the barcode (see~\autoref{tab:qrcapacity}). 

Second, additional \textit{error correction blocks} are computed based on the Reed-Solomon error correction theory \citep{wicker1999reed}. Third, the minimal version of the QR Code is determined, which defines the size of the 2D array to “print” the error correction and data blocks, as a binary image. When this is done, the space reserved for the error correction blocks is larger than the space reserved for the data blocks (see~\autoref{fig:qrencodingsimple}). 

Finally, a binary mask is implemented in order to randomize as much as possible the QR Code encoding \citep{iso18004_2015}.  

\begin{figure}[h!t]
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter3/figures/jpg/qr_layout.jpg}
    \caption{QR Code encoding defines a complex layout with several patterns to be considered: some of them are non-variant patterns found in each QR Code, others may appear depending on the size of the QR Code, and there is an area related to the data changes for each encoding process. (a) A QR Code with high error correction level and version 5. (b) The complex  structure of the pattern. }
    \label{fig:qrencoding}
\end{figure}

\newpage

\vspace{14em}

\begin{table}[h]
\scalebox{0.95}{
    \begin{tabular}{lrrrrr}
    \textbf{Version \&} & \multicolumn{5}{c}{\textbf{Symbol capacity per data type}} \\
    \textbf{ECC level} & \textbf{Bits}   & \textbf{Numeric} & \textbf{Alphanumeric} & 
    \textbf{Binary} & \textbf{Kanji} \\
    \midrule[\heavyrulewidth]
    Version \1 \\
    \hline
    L         & 152    & 41      & 25           & 17     & 10    \\
    M         & 128    & 34      & 20           & 14     & 8     \\
    Q         & 104    & 27      & 16           & 11     & 7     \\
    H         & 72     & 17      & 10           & 7      & 4     \\
    \midrule[\heavyrulewidth]
    Version \textnum{2} \\
    \hline
    L         & 272    & 77      & 47           & 32     & 20    \\
    M         & 224    & 63      & 38           & 26     & 16    \\
    Q         & 176    & 48      & 29           & 20     & 12    \\
    H         & 128    & 34      & 20           & 14     & 8     \\
    \midrule[\heavyrulewidth]
    Version \textnum{39} \\
    \hline
    L         & 22,496 & 6,743   & 4,087        & 2,809  & 1,729 \\
    M         & 17,728 & 5,313   & 3,22         & 2,213  & 1,362 \\
    Q         & 12,656 & 3,791   & 2,298        & 1,579  & 972   \\
    H         & 9,776  & 2,927   & 1,774        & 1,219  & 750   \\
    \midrule[\heavyrulewidth]
    Version \textnum{40} \\
    \hline
    L         & 23,648 & 7,089   & 4,296        & 2,953  & 1,817 \\
    M         & 18,672 & 5,596   & 3,391        & 2,331  & 1,435 \\
    Q         & 13,328 & 3,993   & 2,42         & 1,663  & 1,024 \\
    H         & 10,208 & 3,057   & 1,852        & 1,273  & 784  \\
    \midrule[\heavyrulewidth]
    \end{tabular}
}
\caption{A summary of QR Code data encoding capacity is shown. The total capacity for each data type is expressed in \textit{symbol capacity}. Rows show differences of capacity for four QR Code versions \textnum{(1,2,39,40)} and each of the four error correction levels (ECC). Columns are ordered left to right from higher to lower capacity.}
\label{tab:qrcapacity}
\end{table}

\vspace{4em}

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter3/figures/jpg/qr_layout_simple.jpg}
    \caption{QR Code simplified areas corresponding to the encoding process. (a) A QR Code with high error correction level and version 5. (c) Simplified view of the QR patterns, the yellow frame corresponds to the “error correction” area and the dark green frame corresponds to the “data” area. }
    \label{fig:qrencodingsimple}
\end{figure}

\newpage

During the generation of a QR Code, the level of error correction (ECC) can be selected, from high to low capabilities: H (30 \%), Q (25\%), M (15\%) and L (7\%). This should be understood as the maximum number of error bits that a certain barcode can support (maximum Bit Error Ratio, detailed in \autoref{ch:5}). Notice the error correction capability is independent of the version of the QR Code. However, both combined define the maximum data storage capacity of the QR Code. For a fixed version, higher error correction implies a reduction of the data storage capacity of the QR Code. 

This error correction feature is indirectly responsible for the popularity of QR Codes, since it  makes them extremely robust while allowing for a large amount of pixel tampering to accommodate aesthetic features, like allocating brand logos inside the barcode \citep{Chu2013, Garateguy2014} (see~\autoref{fig:qrswithlogos} and \autoref{fig:qrswithlogoscolor}). In this thesis, we will take advantage of such error correction to embed reference colors inside a QR Code.

\begin{figure}[h!t]
\centering
\includegraphics[width=0.85\linewidth]{chapters/chapter3/figures/jpg/halftone_qrcodes.jpg}
    \caption{Different examples of \textit{Halftone QR Codes}, introduced by HK. Chu et al. \citep{Chu2013}. These QR Codes exploit the error correction features of the QR Code to achieve back-compatible QR Codes with apparent grayscale --halftone-- colors.}
    \label{fig:qrswithlogos}
\end{figure}

\vspace{6em}

\begin{figure}[h!t]
\centering
\includegraphics[width=0.55\linewidth]{chapters/chapter3/figures/jpg/garateguy.jpg}
    \caption{Original figure from Garateguy et al. (© 2014 IEEE) \citep{Garateguy2014}, different QR Codes with color art are shown: (a) a QR Code with a logo overlaid; (b) a \textit{QArt Code} \citep{qartcodes}, (c) a \textit{Visual QR Code}; and (d) the Garateguy et al. proposal.  }
    \label{fig:qrswithlogoscolor}
\end{figure}





\clearpage
\subsection{Computer vision features of QR Codes}
\label{subsec:qrfeatures}

Besides the data encoding introduced before, a QR Code embeds computer vision features alongside with the encoded digital data. These features play a key role when applying computer vision transformations to the acquired images containing QR codes. Usually, they are extracted to establish a correspondence between their apparent positions in the captured image plane and those in the underlying 3D surface topography. The main features we focus on this thesis are:

\begin{itemize}
    \item \textbf{Finder patterns} are the corners of the QR Code, it has 3 of them to break symmetry and orient the QR in a scene (see~\autoref{fig:qrcodeparts}.a).
    \item \textbf{Alignment patterns} are placed inside the QR Code to help in the correction of noncoplanar deformations (see~\autoref{fig:qrcodeparts}.b).
    \item \textbf{Timing patterns} are located alongside two borders of the QR Code, between a pair of finder patterns, to help in the correction of coplanar deformations (see~\autoref{fig:qrcodeparts}.c).
    \item The \textbf{fourth corner} is the one corner not marked with a finder pattern. It can be found as the crosspoint of the straight extensions of the outermost edges of two finder patterns (see~\autoref{fig:qrcodeparts}.d). It is useful in linear, coplanar and noncoplanar deformations. 
\end{itemize}


\begin{figure*}[h]
    \centering
    \includegraphics[width=.90\textwidth]{chapters/chapter4/figures/jpg/2.jpg}
    \caption{Computer vision patterns featured in a QR Code. (a) Three finder or position patterns, (b) six alignment patterns, (c) two timing patterns and (d) the fourth corner that can be inferred from the external edges of the finder patterns.  }
    \label{fig:qrcodeparts}
\end{figure*}


These features are easy to extract due to their spatial properties. They are well-defined as they do not depend on the version of the QR Code, nor the data encoding. The lateral size for a finder pattern is always \textnum{7} modules.  For an alignment pattern, \textnum{5} modules. And timing patterns grow along with each version, but their period is always \textnum{2} modules (one black, one white). 

Finder patterns implement a sequence of modules along both axes that follows: \1 black, \1 white, \textnum{3} black, \1 white and \1 black, often written as a \textnum{1:1:3:1:1} relation (see~\autoref{fig:finderpattern}). Alignment patterns implement a sequence of modules along both axes that follows: \1 black, \1 white, \1 black, \1 white and \1 black, a \textnum{1:1:1:1:1} relation (see~\autoref{fig:alignmentpattern}). 

Thus, the relationship between white and black pixels provides a path to use pattern recognition techniques to extract these features, as these relations are invariant to perspective transformations. Moreover, these linear relations can be expressed as squared area relations, and are still invariant under perspective transformations. This is specially useful when using extraction algorithms based upon contour recognition \citep{GonzalezWoodsEddins2011, itseez2015opencv}. For finder patterns the area relation becomes \textnum{7^2:5^2:3^2} (see~\autoref{fig:finderpattern}); and for alignment patterns, \textnum{5^2:3^2:1^2} (see~\autoref{fig:alignmentpattern}). 

\vspace{2em}

\begin{figure}[h]
\centering
\includegraphics[width=0.85\linewidth]{chapters/chapter3/figures/jpg/finder_pattern.jpg}
% \boxed{\huge Figure finder patterns}
    \caption{Finder pattern definition in terms of modules. Finder pattern always measures \textnum{7 \times 7} modules. If scanned with a line barcode scanner the \textnum{1:1:3:1:1} ratio is maintained no matter the direction of the scanner. If scanned using contour extraction the aspect ratio \textnum{7^2:5^2:3^2} is maintained as well if the QR Code is captured within a projective scene (i.e. a handheld smartphone).}
    \label{fig:finderpattern}
\end{figure}

\vspace{0.5cm}

\begin{figure}[h]
\centering
\includegraphics[width=0.85\linewidth]{chapters/chapter3/figures/jpg/alignment_pattern.jpg}
% \boxed{\huge Figure alignment patterns}
    \caption{Alignment pattern definition in terms of modules. Alignment pattern always measures \textnum{5 \times 5} modules. If scanned with a line barcode scanner the \textnum{1:1:1:1:1} ratio is maintained no matter the direction of the scanner. If scanned using contour extraction the aspect ratio \textnum{5^2:3^2:1^2} is maintained as well if the QR Code is captured within a projective scene (i.e. a handheld smartphone).}
    \label{fig:alignmentpattern}
\end{figure}


\clearpage
\subsection{Readout of QR Codes}

Let us explore a common pipeline towards QR Code readout. First, consider a QR Code captured from a certain point-of-view in a flat surface which is almost coplanar to the capture device (e.g. a box in a production line). Note that more complex applications, such as bottles \citep{HIGGINS201419}, all sorts of  food packaging \citep{Violino2019}, etc., which are key to this thesis, are tackled in \autoref{ch:4}. 

Due to perspective, the squared shape of the QR Code will be somehow deformed following some sort of projective transformation (see~\autoref{fig:qrcontours}.a). Then, in order to find the QR Code itself within the image field, the three finder patterns are extracted applying contour recognition algorithms based on edge detection \citep{GonzalezWoodsEddins2011, itseez2015opencv} (see~\autoref{fig:qrcontours}.b). As explained in \autoref{subsec:qrfeatures}, each finder pattern candidate must hold a very specific set of area relationships, no matter how they are projected if the projection is linear. The contours that fulfill this area relationship are labeled as candidates finder patterns (see~\autoref{fig:qrcontours}.c). 

\begin{figure}[h]
\centering
    \includegraphics[width=\linewidth]{chapters/chapter3/figures/jpg/qr_detection.jpg}
    \caption{The QR Code contour detection method. (a) A QR Code from a certain perspective. (b) All the contours detected in the image. (c) The location of the position patterns following the area rule. Their respective centers of mass are indicated.}
    \label{fig:qrcontours}
\end{figure}

Second, the orientation of the QR Code must be recognized, as in a general situation, the QR Code captured in an image can take any orientation (i.e. rotation). The above-mentioned three candidate finder patterns are used to figure out the orientation of the barcode. To do so, we should bear in mind that one of these corners will correspond to the top-left one and the other two will be the end points of the opposite diagonal (see~\autoref{fig:qrorientation}.a). By computing the distances between the three candidate finder pattern centers and comparing them we can find which distance corresponds to the diagonal and assign the role of each pattern in the QR Code accordingly. The sign of the slope of the diagonal $ m $ and the sign of the distance to the third point  $ s $ are computed and analyzed to solve the final assignment of the patterns. The four possible combinations result in 4 possible different orientations: north, east, south, west (see~\autoref{fig:qrorientation}.b). Once the orientation is found, the three corner candidates are labeled following the sequence $ {L, M, N} $. 


\begin{figure*}[h]
\centering
    \includegraphics[width=0.75\linewidth]{chapters/chapter3/figures/jpg/qr_orientation.jpg}
    \caption{The different orientations of a QR Code: (a) Representation of the diagonal connecting the corners $ m $ and the diagonal segment linked to the top-left corner $ s $. (b) The four possible orientations of a QR-Code.}
    \label{fig:qrorientation}
\end{figure*}

\newpage

Third, a projection correction is performed to retrieve the QR Code from the scene. The  finder patterns can then be used to correct the projection deformation of the image in the QR Code region. If the deformation is purely affine, e.g. a flat surface laying coplanar to the reader device, we can perform the correction with these three points.  But, if a  more general deformation is presented, e.g. handheld capture in a perspective plane, one needs at least one additional point to carry out such transformation: the remaining fourth corner $ O $ (see~\autoref{fig:qrorientation}.a). As the edges around the 3 main corners were previously determined (see~\autoref{fig:qrcorrectperspective}.a), the fourth corner $ O $ is localized using the crossing points of two straight lines from corners $ M $ and $ N $ (see~\autoref{fig:qrcorrectperspective}.b). With this set of 4 points, a projective transformation that corrects the perspective effect on the QR Code can also be carried out (see~\autoref{fig:qrcorrectperspective}.c). 

Notice the calculation of the fourth corner $ O $ can accumulate the numerical error of the previous steps. This might lead to inaccurate results in the bottom-right corner of the recovered code (see~\autoref{fig:qrcorrectperspective}.c) and, in some cases, to a poor perspective correction. This effect is especially strong in low resolution captures, where the modules of the QR Code measure a few image pixels. In order to solve this issue, the alignment patterns are localized (see~\autoref{fig:qrcorrectperspective}.d) in a more restricted and accurate contour search around the bottom-right quarter of the QR Code (see~\autoref{fig:qrcorrectperspective}.e). With this better estimation of a grid of reference points of known (i.e. tabulated) positions a second projective transformation is carried out (see~\autoref{fig:qrcorrectperspective}.f). Normally, having more reference points than strictly needed to compute projective transformations is not a problem thanks to the use of maximum likelihood estimation (MLE) solvers for the projection fitting \citep{MarquezNelia2013}. 

Finally, the QR Code readout is performed, this means the QR Code is down-sampled to a resolution where each of the modules occupies exactly one image pixel. After this, the data is extracted following a reverse process of the encoding: the data blocks are interpreted as binary data, also the error correction blocks. The Reed-Solomon technique to resolve errors is applied, and the original data is retrieved. 

\begin{figure}[h]
\centering
    \includegraphics[width=\linewidth]{chapters/chapter3/figures/jpg/qr_correction_vertical.jpg}
    \caption{The QR Code projective correction steps. (a) The orientation is deduced from the centers of the 3 finder patterns $L, \ M, \ N$. In this step, their contour corners are found. (b) The fourth corner O is found, based on the previous three corners. (c) A first projective transformation is carried out, but still subject to significant error shifts around the bottom-right corner; (d) The alignment patterns are localized in a restricted contour search. The centers of the alignment patters (shifted centers after the first projective correction (green) and the reference centers are both found (red). (e) The error committed at this stage is shown by subtraction of the images. (f) Finally, a second projective transformation recovers the final QR Code image, based on the reference, tabulated, positions of the alignment patterns.}
    \label{fig:qrcorrectperspective}
\end{figure}