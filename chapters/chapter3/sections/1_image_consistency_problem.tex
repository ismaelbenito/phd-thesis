\section{The image consistency problem}
\label{sec:theimageconsistency}

Color reproduction is one of the most studied problems in the audiovisual industry, that is present in our daily lives, long before today's smartphones, when color was introduced to the cinema, color analog cameras and color home TVs \citep{Hunt2005}. In the past years, reproducing and measuring color has also become an important challenge for other industries such as health care, food manufacturing and environmental sensing. Regarding health care, dermatology is one of the main fields where color measurement is a strategic problem, from measuring skin-tones to avoid dataset bias \citep{Newton2020} to medical image analysis to retrieve skin lesions \citep{BUNTE20111892, LI201866}. In food manufacturing, color is used as an indicator to solve quality control and freshness problems \citep{Cubero2010, Pathare2012, Wu2013}. As for environmental sensing \citep{Fernandes2020}, colorimetric indicators are widely spread to act as humidity \citep{Jung2016}, temperature \citep{Seeboth2014} and gas sensors \citep{Zhang2018, Wang2014}. 
% These environmental sensors can be measured inside fixed electronic setups, thus solving the color acquisition problem by fixing their illumination conditions \citep{Driau2019}, or instead, using smartphone technology which addresses the image consistency problem \citep{Engel2021}.

In this section, we focus on image consistency, a reduced problem from color reproduction. While \textit{color reproduction} aims at matching the color of a given object when reproduced in another device as an image (e.g. a painting, a printed photo, a digital photo on a screen, etc.), \textit{image consistency} is the problem of taking different images of the same object in different illumination conditions and with different capturing devices, to finally obtain the same apparent colors for that object. In this problem, the apparent colors of an object do not need to match its “real” spectral color,  they rather have to be just similar in each instance captured in different scenarios. In other words, all instances should match the first capture, or the reference capture, and not the real-life color. Therefore, image consistency is the actual problem to solve in the before-mentioned applications, in which it is more important to compare acquired images between them, so that consistent conclusions can be drawn with all instances, than comparing them to an actual reflectance spectrum. 

\subsection{Color reproduction}

Color reproduction is the problem of matching the reflectance of an object with an image of this object \citep{Hunt2005}. This can be seen in \autoref{fig:colorreproduction}.a, where an object (an apple), with a reflectance $ \mathrm{R (\uplambda)} $, is illuminated by a light source $ \mathrm{I (\uplambda)} $ and captured by a camera with a sensor response $ \mathrm{D (\uplambda)} $. In fact, digital cameras contain more than one sensor targeting different ranges of the visible spectrum: commonly, 3 types of sensors centered in red, green and blue colors \citep{Hunt2005}. 

% \newpage

\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{chapters/chapter3/figures/jpg/1.jpg}
    \caption{The color reproduction problem: (a) a certain light source ($ \mathrm{I} (\uplambda) $) illuminates a certain object with a certain reflectance ($ \mathrm{R} (\uplambda) $), this scene is captured by a certain camera with its sensor response ($ \mathrm{D} (\uplambda) $) and (b) the reproduced image of the object ($ \mathrm{R'} (\uplambda) $) is then illuminated and captured again. }
    \label{fig:colorreproduction}
\end{figure}


In general, the signal acquired by one of the sensors inside the camera device can be modeled as \citep{Shafer1985}:

\begin{equation}
   \mathrm {S_k \propto \int_{-\infty}^{\infty} I(\uplambda) \ R(\uplambda) \ D_k(\uplambda) \ d\uplambda}
   \label{eq:colorsingalintegral}
\end{equation}


\noindent 
where $ k \in \{ 1, \dots , \ N \} $ are the channels of the camera, $ N $ is the total number of channels and $ \mathrm{\uplambda} $ are the visible spectra wavelengths. Then, \autoref{fig:colorreproduction}.b portrays the color reproduction of the object, where now a new reflectance will be recreated and captured with the same conditions. Since our image is a printed image, the new reflectance will be:

\begin{equation}
   \mathrm{R'(\uplambda)} = \sum_{i=0}^{M} f_i (S_1, \dots, S_N) \cdot \mathrm{R_i(\uplambda)} 
   \label{eq:reproductionsum}
\end{equation}

\noindent 
where $ \mathrm{R_i (\uplambda)} $ are the reflectance spectra of the $ M $ reproduction inks, which will be printed as a function of the acquired $ \mathrm{(S_1, \dots, S_N)} $ channel contributions. The color reproduction problem now can be written as the minimization problem to the distance of both reflectances:

\begin{equation}
   \mathrm{\left \| R'(\uplambda) -R(\uplambda) \right \| \rightarrow  0}
   \label{eq:reproductionmin}
\end{equation}

\noindent 
for each wavelength, for each illumination and for each sensor. The same formulation could be written when displaying images on a screen by changing $ \mathrm{R (\uplambda)} $ for $ \mathrm{I (\uplambda)} $ of the light emitting screen. 


Color reproduction is a wide open problem, and with each step towards its general solution, the goal of achieving image consistency when acquiring image datasets is nearer. Since color reproduction solutions aim at attaining better acquisition devices and better reproduction systems, the need for solving the image consistency problem will  eventually disappear. But this is not yet the case.


\subsection{Image consistency}

However, the image consistency problem is far simpler than the color reproduction problem. The image consistency problem can be seen as the problem to match the acquired signal of any camera, under any illumination for a certain object. This can be seen in \autoref{fig:imageconsistency}.a: an object (an apple), which has a reflectance $ \mathrm{R (\uplambda)} $, is illuminated by a light source $ \mathrm{I (\uplambda)} $ and it is captured by a camera with a sensor response $ \mathrm{D (\uplambda)} $. Now, in \autoref{fig:imageconsistency}.b, the object is not reproduced but exposed again to different illumination conditions $ \mathrm{I' (\uplambda)} $ and captured by a different camera $ \mathrm{D' (\uplambda)} $. 


\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{chapters/chapter3/figures/jpg/2.jpg}
    \caption{The imaging consistency problem: (a) a certain light source ($ \mathrm{I} (\uplambda) $) illuminates a certain object with a certain reflectance ($ \mathrm{R} (\uplambda) $), this scene is captured by a certain camera with its sensor response ($ \mathrm{D} (\uplambda) $) and (b) the same object is now illuminated by another light source ($ \mathrm{I'} (\uplambda) $) and captured by another camera ($ \mathrm{D'} (\uplambda) $).  }
    \label{fig:imageconsistency}
\end{figure}


Under their respective illumination, each camera will follow \autoref{eq:colorsingalintegral}, providing three different $ \mathrm{S_k} $ channels. Considering we can write a vector signal from the camera as: 

\begin{equation}
  \mathbf{s} = \mathrm{(S_1, \dots, S_N)} \ \mathrm{,}  
  \label{eq:colorvector}
\end{equation}

\noindent
the image consistency problem can be written as the minimization problem to the distance between acquired signals: 

\begin{equation}
    \left \| \mathbf{s}' - \mathbf{s} \right \| \rightarrow  0 
    \label{eq:consistencymin}
\end{equation}

\noindent
for each camera, for each illumination for a given object.

\newpage
The image consistency problem is easier to solve, as we have changed the problem from working with continuous spectral distributions (see~\autoref{eq:reproductionmin}) to N-dimensional vector spaces (see~\autoref{eq:consistencymin}). These spaces are usually called \textit{color spaces}, and the mappings between those spaces are usually called \textit{color conversions}. Deformations or corrections inside a given color space are often referred to as \textit{color corrections}. 
In this thesis, we will be using RGB images from digital cameras. Thus, we will  work with \textit{device-dependent color spaces}. 


This means that the mappings will be performed between RGB spaces. Then, we can rewrite the color vector definition for RGB colors following  \autoref{eq:colorvector} as:

\begin{equation}
  \mathbf{s} = \mathrm{(r, g, b),} \ \  \mathbf{s} \in \mathbb{R}^3  \ \mathrm{,}
  \label{eq:colorvectorrgb}
\end{equation}


\noindent
where $ \mathbb{R}^3 $ represents here a generic 3-dimensional RGB space. In \autoref{subsec:colorspaces}, we detail how color spaces are defined according to their bit resolution and color channels. 


\subsection{Color charts}


The traditional approach to achieve a general purpose color correction is the use of \textit{color rendition charts}, introduced by C.S. McCamy et. al. in 1976 \citep{McCamy1976} (see~\autoref{fig:colochecker}). Color charts are machine-readable patterns placed in a scene that embed reference patches of a known color, where in order to solve the problem, several color references are placed in a scene to be captured and then used in a post-capture color correction process.  

These color correction processes involve algorithms to map the color references seen in the chart to their predefined nominal colors. This local color mapping is then extrapolated and applied to the whole image. There exists many ways to correct  the color of images to achieve consistency. 

\begin{marginfigure}
\includegraphics[width=\linewidth]{chapters/chapter3/figures/jpg/3.jpg}
\caption{A ColorChecker chart. The first row shows a set of six “natural colors”; the second one shows a set of "miscellaneous colors"; the third, primary and secondary colors; and the last row, a gray scale gradient. This set of colors samples the RGB space in a limited way, but it is convenient to carry out a few color corrections manually.}
\label{fig:colochecker}
\end{marginfigure}

The most extended way to do so is to search for \textit{device-independent color spaces} (i.e. CIE Lab, CIE XYZ, etc.) \citep{Hunt2005}. But in the past decade, there have appeared solutions that involve direct corrections between \textit{device-dependent color spaces}, without the need to pass through device-independent ones. 

The most simple color correction technique is the \textit{white balance}, that only involves one color reference \citep{Gong2013}. A white reference inside the image is to be mapped to a desired white color and then the entire image is transformed using a scalar transformation. Beyond that, other techniques that use more than one color reference can be found elsewhere, using affine \citep{Gong2013}, polynomial \citep{Cheung2004, Finlayson2015}, root-polynomial \citep{Finlayson2015} or thin-plate splines \citep{Menesatti2012} transforms.

% . In this work, we focused on the implementation of thin-plate spline corrections [], specifically in the use of 3D Thin-Plate Splines (TPS3D) method to perform color correction in image datasets directly in RGB spaces [] (which are three-dimensional).

% Thin-plates splines (TPS) were popularized due to their potential to fit data linked by an elastic deformation, especially when it comes to shape deformation []; and they have been used widely to solve problems like morphing transformations [][][]. Regarding color, we can find works that apply them to screen technology [][] and printing technology []. Also, the TPS methods can implement different radial basis functions (RBF) to compute their splines []. All the above-mentioned correction methods are geometrical transformations, Section 3.4 focuses on this topic.

It is safe to say that, in most of these post-capture color correction techniques, increasing the number and quality of the color references offers a systematic path towards better color calibration results. This strategy however, comes along with more image area dedicated to accommodate these additional color references and therefore, a compromise must be found. 


This led X-Rite (a Pantone subsidiary company), to introduce improved versions of the ColorChecker, like the ColorChecker Passport Photo 2 ® kit (see Figure 4.a). Also in this direction, Pantone presented in 2020 an improved color chart called Pantone Color Match Card ® (see Figure 4.b), based on the ArUco codes introduced by S. Garrido-Jurado et al. in 2015 \citep{Garrido-Jurado2016} to facilitate the location of a relatively large number of colors. Still, the size of these color charts is too big for certain applications with size constraints (e.g. smart tags for packaging \citep{Driau2019, Engel2021}).


\begin{figure*}[h!t]
\centering
\includegraphics[width=0.85\linewidth]{chapters/chapter5/figures/jpg/1.jpg}
\caption{Previous state-of-the-art color correction charts from Pantone and X-Rite. (a) The X-Rite ColorChecker Passport Photo 2\textregistered~kit. (b) The Pantone Color Match Card\textregistered.}
\label{fig:colorcorrectioncharts}
\end{figure*}

\newpage



