\section{Computational implementation}

In 1990, Guido van Rosum released the first version of Python, an open-source, interpreted, high-level, general-purpose, multi-paradigm (procedural, functional, imperative, object-oriented) programming language \citep{python}. Since then, Python has released three major versions of the language: Python 1 (1990), Python 2 (2000) and Python 3 (2008) \citep{van2007python}. 

At the time we started to work in this thesis, Python was one of the most popular programming languages both in the academia and in the industry \citep{solem2012programming}. As Python is an interpreted language, the actual code of Python is executed by the Python Virtual Machine (PVM), this opens the door to create different PVM written with different compiled languages, the official \textit{Python distribution} is based on a C++ PVM, that is why the mainstream Python distribution is called 'CPython' \citep{Cao2015}. 


CPython allows the user to create \textit{bindings} to C/C++ libraries, this was specially useful for our research. OpenCV is a widely-known tool-kit for computer vision applications, which is written in C++, but presents bindings to other languages like Java, MATLAB or Python \citep{howse2016opencv}. 

Altogether, \textit{we decided to use Python as our main programming language}. Both achieving the rapid script capabilities that Python offers, alongside with standard libraries from Python and C++. The research started with Python 3.6 and ended with Python 3.8, due to the Python's development cycle. 

Let us detail the stack of standard libraries used in the development of this thesis:

\begin{itemize}
    \item \textbf{Python environment:} we started using Anaconda, an open-source Python distribution that contained pre-compiled packages ready to be used, such as OpenCV \citep{anaconda}. We adopted also \texttt{pyenv}, a tool to install Python distributions and manage virtual environments \citep{pyenv}. Later on, we started to use \texttt{docker} technology, light virtual machines to enclose the PVM and our programs \citep{merkel2014docker}.
    
    \item \textbf{Scientific and data:} we adopted the well-known \texttt{numpy} / \texttt{scipy} / \texttt{matplotlib} stack:
    \begin{itemize}
        \item \texttt{numpy} is a C++ implementation of array representation (MATLAB-like) for Python \citep{numpy},
        \item \texttt{scipy} is a compendium of common mathematical operations fully compatible with NumPy arrays. Often SciPy implements bindings to consolidated calculus frameworks written in C++ and Fortran, such as OpenBlas \citep{scipy2021},
        \item \texttt{matplotlib} is a 2D graphics environment we used to represent our data \citep{Hunter2007}. 
    \end{itemize}
    
    NumPy, SciPy and Matplotlib are the entry point to a huge ecosystem of packages that use them as their core. When processing datasets, two main packages were used, 
    
    \begin{itemize}
        \item \texttt{pandas} is an abstraction layer to the previous stack, where data is organized in spreadsheets (like Excel, Origin Lab, etc.) \citep{pandas},
        \item \texttt{xarray} is another abstraction layer to the previous stack, with labeled N-dimensional arrays, xarray can be regarded as the N-dimensional generalization of pandas \citep{xarry}. 
    \end{itemize}
    
    \item \textbf{Images manipulation:} there is a huge ecosystem regarding image manipulation in Python, previous to computer vision, we adopted some packages to read and manipulate images, 
    
    \begin{itemize}
        \item \texttt{pillow} is the popular fork from the unmaintained Python Imaging Library, we used Pillow specially to manipulate the image color spaces, e.g. profile an image from RGB to be printed in CMYK \citep{pillow2022},
        \item \texttt{imageio} was used as an abstraction layer from Pillow, which uses Pillow and other I/O libraries (such as \texttt{rawpy}) to read images and convert them directly to NumPy matrices. We standardized our code to read images using this package instead of using other solutions (SciPy, Matplotlib, Pillow, OpenCV, ...)  \citep{imageio},
        \item \texttt{imgaug} was used to expand image datasets, by tuning randomly parameters of the image (illumination, contrast, etc.). This image \textit{augmentation technique} is well-known to increase datasets when training computer vision models \citep{imgaug}.
        
    \end{itemize}
    
    \item \textbf{Computer vision:} we mainly adopted OpenCV as our main framework to perform feature extraction algorithms, affine and perspective corrections and other operations \citep{itseez2015opencv}. Despite this, other popular frameworks were used for some applications, such as \texttt{scikit-learn} \citep{pedregosa2011scikit}, \texttt{scikit-image} \citep{scikit-image}, \texttt{keras} \citep{ketkar2017introduction}, etc.  
    
    \item \textbf{QR Codes:} regarding the encoding of QR Codes we adopted mainly the package \texttt{python-qrcode} and used it as a base to create our Color QR Codes \citep{qrcode}; regarding the decoding of the QR Codes, there exists different frameworks. We worked with:
    
    \begin{itemize}
        \item \texttt{zbar} is a light C++ barcode scanner, which decodes QR Codes and other 1D and 2D barcodes \citep{zbar}. Among the available Python bindings to this library we chose the \texttt{pyzbar} library \citep{pyzbar},
        \item \texttt{zxing} is a Java bar code scanner, similar to ZBar (formerly maintained by Google), and it is the core of most of Android QR Code scanners \citep{zxing}. As this library was not written in Python we did not use it on a daily basis, but we kept it as secondary QR Code scanner.
    \end{itemize}
    
\end{itemize}