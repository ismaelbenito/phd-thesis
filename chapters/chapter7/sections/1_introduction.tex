In previous chapters, we presented the need to achieve consistency in image datasets, and how this need relates to the capacity to perform quantitative color measurements over those datasets. Also, we discussed how this need is relevant in several industries. In this chapter, we are going to focus on the application of color consistency in analytical chemistry \citep{Fernandes2020}, specifically in environmental sensing. 

Environmental sensing is a wide field of research. For example, one could tackle the problem using  electronic sensors \citep{Fbrega2018}. Or, one could use \textit{colorimetric indicators}. Colorimetric indicators are present in our daily life as humidity \citep{Jung2016}, temperature \citep{Seeboth2014} or gas sensors \citep{Zhang2018, Wang2014}. 

Usually, colorimetric indicators feature chemical reactions which act as a sensor or dosimeter for a certain substance or physical magnitude. A change on these magnitudes is then translated into a change in the color of the chemical solution, e.g. a pH change that induces a change in the chemical structure of a molecule which renders the color change (see~\autoref{fig:engel_chem}). 

Moreover, colorimetric indicators are inexpensive and disposable, and simple to fabricate, for example, printing them on top of a cellulose paper \citep{Zhang2016}.


\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{chapters/chapter7/figures/engel_chem.jpg}
    \caption{Reaction mechanism of the pH indicator bromocresol green (BCG, pH 3.8–5.4) for the detection of NH3. Increase of the NH3 concentration leads to a proton release, detectable as a color change from yellow over green to blue.}
    \label{fig:engel_chem}
\end{figure}


\newpage

In 2017, within a related research project, we presented a solution to detect nitrogen dioxide ($NO_2$) in the environment using a colorimetric indicator. In that work, the colorimetric indicators were prepared soaking sterilized absorbent cellulose into the reactive ink. The results successfully concluded that it was possible to measure air concentrations of $ NO_2 $ from \1 ppm to \textnum{300} ppm in air using a colorimetric indicator  \citep{Fernndez2017,Fabrega2017} (see~\autoref{fig:fabrega_no2}).

\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{chapters/chapter7/figures/fabrega_no2.jpg}
    \caption{UV–VIS diffuse reflectance of the soaked pads with Griess-Saltzman reagent exposed to different $ NO_2 $ concentrations and the corresponding images of the colors developed (insets, 3 replicas per concentration).}
    \label{fig:fabrega_no2}
\end{figure}

Later that year, we also presented a solution to detect ammonia ($NH_3$) in the environment with colorimetric indicators. In that case, the colorimetric indicators were created dip-coating a glass substrate in a solution containing the reactive ink (see~\autoref{fig:nh3_dip_coating}). Results showed that it was possible to measure concentrations of $ NH_3 $ from \textnum{10} to \textnum{100} ppm in air \citep{Schmitt2017}. 

\begin{figure}
    \centering
    \includegraphics{chapters/chapter7/figures/nh3_dip_coating.jpg}
    \caption{Left, an ammonia ($NH_3$) colorimetric indicator has been dip-coated into a glass substrate, which exhibits a yellow color when exposed to synthetic air. Right, the same sensor is exposed to 100 ppm of  $NH_3$ and it turns into purple.}
    \label{fig:nh3_dip_coating}
\end{figure}


In both works, we did not measure the color with digital cameras. On one hand, the $NO_2$ sensor was enclosed in a setup with a fixed one-pixel RGB sensor (TCS3200) and four LED acting as a light source. This research line was used in parallel as an alternative, a reference to understand color changes by enclosing colorimetric sensors in controlled compact and cost-effective fixed setups \citep{Driau2019,Driau2019_2,Driau2020}.

\newpage

On the other hand, the $NH_3$ sensor was studied using standard spectrophotometry and the sRGB color was computed from the measured spectra (see~\autoref{fig:human_vision_nh3}). The work presented here is the continuation of our contribution to solve this problem using digital cameras without the need to enclose the sensor in a setup \citep{Benito-Altamirano2018,Engel2020,Engel2021}. 

\vspace{1em}

\begin{figure*}[h!t]
    \centering
    \includegraphics[trim={0 0 0 1em},clip,width=0.9\textwidth]{chapters/chapter7/figures/human_vision.jpg}
    \caption{(a) Standard tristimulus X($\uplambda$), Y($\uplambda$), Z($\uplambda$) curves of the human eye. (b) The integrated sRGB colors represented in the RGB cube. (c) The rendered sequence of RGB colors corresponding to the gas sensing spectra c($\uplambda$).}
    \label{fig:human_vision_nh3}
\end{figure*}

\vspace{1em}

In this chapter, we present the different partial approaches to combine colorimetric indicators that led to the thesis proposal of Color QR Codes. The partial solutions were applied to different target gases, such as ammonia, ($NH_3$), hydrogen sulfide ($H_2S$), etc. 

Finally, we present here \textit{a carbon dioxide ($CO_2$) sensor featured in a Color QR Code}. The Color QR Code enables to: extract the sensor from any surface (\autoref{ch:4}), embed inside or outside the QR Code the sensor ink (\autoref{ch:5}) alongside with color references to perform a color correction using the whole range of corrections studied (\autoref{ch:6}).


\newpage