\section{Results}

\subsection{The color response}

Let us start with the response of the colorimetric indicator under the different $CO_2$ atmospheres. \autoref{fig:response_pulses} shows the signals obtained from the mean RGB channels for the colorimetric indicator for all the experiment captures in the D65 standard illuminant (6500K). In order to obtain these mean values,  we created a computer vision algorithm to extract the region of interest. This algorithm was based on the state-of-the-art, presented in \autoref{ch:3}, and our work, in \autoref{ch:4}. 

First, with these results, we could already confirm that we correctly selected our absolute reference to compute the color response, $ (r_{ref}, g_{ref}, b_{ref}) = (0, 0, 255)$. As the previous works suggested \citep{Zhang2016}, the color indicator moves from a blueish color to a yellowish color with the appearance of $ CO_2 $ in the atmosphere.  

\begin{figure*}
    \centering
    % \includegraphics[trim={0 16em 2em 16em},clip]{chapters/chapter7/figures/response_pulses_colors.jpg}
    \includegraphics[trim={0 0 1em 0},clip]{chapters/chapter7/figures/response_pulses.jpg}
    \caption{From top to bottom: a representation of the color of the sensor over time for the D65 standard illuminant (6500K); the same colors as RGB channel signals; and the responses (\%) for all the RGB channels.}
    \label{fig:response_pulses}
\end{figure*}

Then, \autoref{fig:response_pulses} displays the computed responses from the sensor. The responses were computed following \autoref{eq:model_response}. The results show the channel which achieved the best response was the \textbf{green channel}. This presented a higher response and less noise. Followed by the red channel, which performed close to the green channel in response, but with a more severe noise. The blue channel, performed with approximately the half of the response than the other channels at the lower concentration tested. And, its response saturated in the higher concentrations more rapidly than red and green channels.

\newpage

\begin{figure*}
    \centering
    \includegraphics[width=1\textwidth]{chapters/chapter7/figures/response_pulses_unified.jpg}
    \caption{The response of the green channel exposed to a D65 standard illuminant (6500K) with all the pulses overlapped in the same time frame. This results in 15 pulses, each reference target gas concentration (\textnum{20\%}, \textnum{30\%}, \textnum{35\%}, \textnum{40\%}, \textnum{50\%}) has two pulse replicas (\0, \1, \textnum{2}).}
    \label{fig:response_pulses_unified}
\end{figure*}

Moreover, in \autoref{fig:response_pulses_unified} we present the previous results but now stacked into the same time frame (the pulse duration of \textnum{100} minutes). This is interesting  from the gasometric standpoint. Our colorimetric indicator presented:

\begin{itemize}
    \item a fast response, achieving the \textnum{90\%} of the $ S_g (\%) $ response in less than 5 minutes, for the highest concentration; and in less than 10 minutes, for the lowest one, 
    \item a reasonable maximum response above \textnum{150\%} for the higher target concentration of $CO_2$ \textnum{50\%},
    \item a slight saturation in the upper region of concentrations, this will be reflected in our fitted models,
    \item good reproducibility among the replicas of each pulse, except for one pulse in the first triplet (\textnum{20\%} of target gas concentration),
    \item and, a drift in the lower concentration area, as pulses did not end in the same response they started. 
\end{itemize}

All in all, \autoref{fig:response_pulses_unified} presented our colorimetric indicator as a good choice to detect the concentration of $ CO_2 $ in modified atmosphere packaging (MAP). The caveats presented by the sensor: the saturation in the upper range (\textnum{50\%}) and the drift in the lower range (\textnum{0\%}) did not affect our further results as our models were targeting only to fit the data of the range that applies to MAP (\textnum{20\%-50\%}). 

\newpage

\begin{figure*}
    \centering
    % \hbox{
    % \hspace{-0.10\textwidth}
    % \includegraphics[trim={0.8em 0 1.25em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/response_pulses_unified_illuminations.jpg}
    % }
    \includegraphics[width=1\textwidth]{chapters/chapter7/figures/response_pulses_unified_illuminations.jpg}
    \caption{The response of the green channel exposed to nine illuminants (2500K to 6500K) with all the pulses overlapped in the same time frame. This results in 135 apparent pulses,  now each reference target gas concentration (\textnum{20\%}, \textnum{30\%}, \textnum{35\%}, \textnum{40\%}, \textnum{50\%}) has two pulse replicas (\0, \1, \textnum{2}) for each illuminant. Legend is omitted to favor clearness, results should be compared to \autoref{fig:response_pulses_unified}.}
    \label{fig:response_pulses_unified_illuminations}
\end{figure*}

However, these results became meaningless when we exposed the colorimetric indicator to other light conditions than the D65 standard illumination, as the apparent sensor response changed drastically, as expected. \autoref{fig:response_pulses_unified_illuminations} portrays how the different illuminations affected the measure of the response for all the previous 15 pulses.  

Finally, we exploited the color correction framework studied in \autoref{ch:6}. For each illumination, the 125 RGB colors placed inside the Color QR Code were extracted (see~\autoref{fig:rgb_color_illuminations}). These colors were used to apply each one of the color correction techniques described in \autoref{ch:6} (see~\autoref{tab:correctionssummary}). All the images from each illumination were corrected (>\textnum{10000} image/illumination).

The references of the D65 standard illumination were taken as the reference color space of our correction techniques. \autoref{fig:response_pulses_unified_illuminations_corrected} shows how a TPS3 improved the situation presented in \autoref{fig:response_pulses_unified_illuminations} recovering a more suitable scenario to fit a colorimetric model to the data. In the next subsection, we focus in how we measured each pulse and fitting our proposed model to the different color corrections.



\begin{figure*}[h!b]
    \centering
    \includegraphics[width=0.9\textwidth]{chapters/chapter7/figures/rgb_colors_illuminations.jpg}
    \caption{The captured color references from the Color QR Code in each illumination condition. D65 is the reference space. }
    \label{fig:rgb_color_illuminations}
\end{figure*}

\newpage

\begin{figure*}
    \centering
    % \hbox{
    % \hspace{-0.10\textwidth}
    % \includegraphics[trim={0.8em 0 1.25em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/response_pulses_unified_illuminations.jpg}
    % }
    \includegraphics[width=1\textwidth]{chapters/chapter7/figures/response_pulses_unified_illuminations_corrected.jpg}
    \caption{The response of the green channel exposed to nine illuminants (2500K to 6500K), then corrected using the TPS3 method (\autoref{tab:correctionssummary}), with all the pulses  overlapped in the same time frame. This results in 135 apparent pulses,  now each reference target gas concentration (\textnum{20\%}, \textnum{30\%}, \textnum{35\%}, \textnum{40\%}, \textnum{50\%}) has two pulse replicas (\0, \1, \textnum{2}) for each illuminant. Legend is omitted to favor clearness, results should be compared to \autoref{fig:response_pulses_unified}. The shadowed area is the area corresponding to the 5 minutes windows used to integrate the response of the sensor for the model computation.}
    \label{fig:response_pulses_unified_illuminations_corrected}
\end{figure*}


\subsection{Model fitting}

After applying the color correction techniques, we prepared the data to be suitable to fit our proposed linear-logarithmic model (\autoref{eq:model}). To do so we:

\begin{itemize}
    \item measured a mean corrected response value from the data during the last five minutes of gas exposition, (\autoref{fig:response_pulses_unified_illuminations_corrected} shows this time window shadowed),
    \item transformed these response measures applying a logarithm, 
    \item linked those responses to their respective gas concentration measures from Table 6.1, 
    \item and, split the available data into train (75\%) and validation subsets (25\%).
\end{itemize}


Then we fed the train subsets, one for each available color correction in \autoref{tab:correctionssummary}, to a linear model solver and obtained up to 22 different solutions, including the special NONE and PERF corrections described in \autoref{ch:6}. The validation subsets were used later to evaluate the models.

Let us start with these two reference corrections, \autoref{fig:model_NOPE_PERF} shows the fitting for both corrections, and \autoref{fig:model_NOPE_PERF_valid} shows the validation results. Results indicated that \textbf{NONE} was the \textit{worst case scenario}, thus without correction the measurement of the colorimetric indicator was impossible. And \textbf{PERF}, as expected, was the \textit{best case scenario}. 



\begin{figure}
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_NONE_None_PERF_None.jpg}
    }
    % \includegraphics[width=0.9\textwidth]{chapters/chapter7/figures/models_NONE_None_PERF_None.jpg}
    \caption{NONE and PERF fitted models, which represent the worst and the best case scenarios, respectively. The fitted model in NONE is the one performed without correcting any captured color. The PERF model is an ideal model in which each captured color has been mapped to its correspondent D65 color.}
    \label{fig:model_NOPE_PERF}
\end{figure}

\begin{figure}
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_valid_NONE_None_PERF_None.jpg}
    }
    % \includegraphics[width=0.9\textwidth]{chapters/chapter7/figures/models_valid_NONE_None_PERF_None.jpg}
    \caption{NONE and PERF regression for the validation data. The coefficient $ r^2 $ was computed for this data. This result confirms that NONE is the worst case scenario with a null $ r^2 $, and PERF is the best score in the whole set of results. }
    \label{fig:model_NOPE_PERF_valid}
\end{figure}

% AFF

\newpage


Specifically, when we say PERF is the best case scenario we mean the following: as the PERF model is the model of acquiring the data in a fixed setup --with a fixed camera configuration, a fixed light conditions, etc.--, the problem which we aim to solve in this thesis, the image consistency problem, is not present in this data. Therefore, it follows that the error seen in this model is the intrinsic error of the colorimetric indicator technology. 

Then, the PERF results showed the good performance of the colorimetric indicator to sense $ CO_2 $ in the target range of gas concentrations, scoring both $ r^2 $ metrics for training (\autoref{fig:model_NOPE_PERF}) and validation (\autoref{fig:model_NOPE_PERF_valid}) almost a perfect score. \textit{This confirmed our model proposal}.

Let us compare now the subsequent color corrections (\autoref{tab:correctionssummary}) with the before-mentioned extreme cases. The results are displayed from \autoref{fig:model_AFF} to \autoref{fig:model_TPS_valid}:

\begin{itemize}
    \item \textbf{AFF:} AFF0, AFF1 and AFF2 showed, in that order, the worst results in the dataset, both in training (see~\autoref{fig:model_AFF}) and validation (see~\autoref{fig:model_AFF_valid}). Although the experiment were biased towards this group of corrections. The bias is explained by the fact that we only changed color temperature of white illuminants in our setup. 
    
    However, AFF3 scored the best results above the whole other corrections groups, this is the correction which best approximated to the PERF solution. 
    
    This is explained again by the bias, but also because a general affine correction (AFF3) has contributions not present in a simple white-balance (AFF0, AFF1), like the cross-terms, or the affine without translation (AFF2). These contributions maximized the color correction quality by reducing the mean distance to corrected color of the sensor. 
    
    \item \textbf{VAN:} all corrections showed similar result to AFF3, but neither of them accomplished to outperform it. From the standpoint of the training model all four models showed the same fitting, if error correction is taken into account (see~\autoref{fig:model_VAN}). From validation, the results showed also the same metric once again, good approaches to PERF (see~\autoref{fig:model_VAN_valid}). 
    \item \textbf{CHE:} all corrections showed a similar result to AFF3 in training (see~\autoref{fig:model_CHE}), the same as all VAN corrections. Only CHE0 showed a slightly worse metric in validation (see~\autoref{fig:model_CHE_valid}), which is non-significant. 
    \item \textbf{FIN:} FIN0 presented similar results to AFF3 both in training (see~\autoref{fig:model_FIN}) and validation (see~\autoref{fig:model_FIN_valid}). However, FIN1, its root-polynomial version, presented worse results, both in training and validation. FIN2 and FIN3, the order 3 versions of FIN0 and FIN1, also failed to correct the color responses properly to fit the data.  
    \newpage
    \item \textbf{TPS:} all corrections showed good results, close to AFF3. In training, all the corrections showed the same fitting, if error correction is taken into account (see~\autoref{fig:model_TPS}). As for validation, TPS0 and TPS1 achieved the best scores. (see~\autoref{fig:model_TPS_valid}) Note here, TPS0 and TPS1 presented problems to ill-conditioned scenarios, this was solved using \autoref{eq:within_comparisom} criterion to detect and clean those scenarios.
\end{itemize}

\vspace{-1.5em}

\begin{figure}[h!]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_AFF0_AFF1_AFF2_AFF3.jpg}
    }
    \caption{AFF0, AFF1, AFF2 and AFF3 fitted models for the green channel of the measured sensor data. AFF0 to AFF2 scored the worst results in the whole dataset. However, AFF3 scored the best.}
    \label{fig:model_AFF}
\end{figure}

\vspace{-1.5em}

\begin{figure}[h!]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_valid_AFF0_AFF1_AFF2_AFF3.jpg}
    }
    \caption{AFF0, AFF1, AFF2 and AFF3 validation regressions. Once again, AFF0 to AFF2 present bad results, where their $r^2$ shows these models are meaningless. On the other hand, AFF3 presents a result really close to PERF.}
    \label{fig:model_AFF_valid}
\end{figure}

% VAN

\begin{figure}[p]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_VAN0_VAN1_VAN2_VAN3.jpg}
    }
    \caption{VAN0, VAN1, VAN2 and VAN3 fitted models for the green channel of the measured sensor data. All models scored slightly worse metrics than the AFF3 correction. Despite this, their training results are as good as the AFF3.}
    \label{fig:model_VAN}
\end{figure}

\begin{figure}[p]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_valid_VAN0_VAN1_VAN2_VAN3.jpg}
    }
    \caption{VAN0, VAN1, VAN2 and VAN3 validation regressions. All models scored good results ($ r^2 > 0.95 $) approximating to the PERF results.}
    \label{fig:model_VAN_valid}
\end{figure}

% CHE

\begin{figure}[p]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_CHE0_CHE1_CHE2_CHE3.jpg}
    }
    \caption{CHE0, CHE1, CHE2 and CHE3 fitted models for the green channel of the measured sensor data. All models scored slightly worse metrics than the AFF3 correction. Despite this, their training results are as good as the AFF3.}
    \label{fig:model_CHE}
\end{figure}

\begin{figure}[p]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_valid_CHE0_CHE1_CHE2_CHE3.jpg}
    }
    \caption{CHE0, CHE1, CHE2 and CHE3 validation regressions. All models scored good results ($ r^2 > 0.95 $), except CHE0 which scored $0.94$, approximating to the PERF results.}
    \label{fig:model_CHE_valid}
\end{figure}

% FIN

\begin{figure}[p]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_FIN0_FIN1_FIN2_FIN3.jpg}
    }
    \caption{FIN0, FIN1, FIN2 and FIN3 fitted models for the green channel of the measured sensor data. Only FIN0 resembles to AFF3. The other three methods performed worse.}
    \label{fig:model_FIN}
\end{figure}

\begin{figure}[p]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_valid_FIN0_FIN1_FIN2_FIN3.jpg}
    }
    \caption{FIN0, FIN1, FIN2 and FIN3 validation regressions. Only FIN0 scores good results, compared to AFF3 ($ r^2 = 0.95 $). The other methods scored worse, resulting in meaningless models.}
    \label{fig:model_FIN_valid}
\end{figure}

% TPS

\begin{figure}[p]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_TPS0_TPS1_TPS2_TPS3.jpg}
    }
    \caption{TPS0, TPS1, TPS2 and TPS3 fitted models for the green channel of the measured sensor data. All models scored slightly worse metrics than the AFF3 correction. Despite this, their training results are as good as the AFF3.}
    \label{fig:model_TPS}
\end{figure}

\begin{figure}[p]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[trim={0 0 0.75em 0},clip,width=1.15\textwidth]{chapters/chapter7/figures/models_valid_TPS0_TPS1_TPS2_TPS3.jpg}
    }
    \caption{TPS0, TPS1, TPS2 and TPS3 validation regressions. TPS0 and TPS1 good results ($ r^2 > 0.95 $), followed by TPS3 and TPS2.}
    \label{fig:model_TPS_valid}
\end{figure}


\newpage