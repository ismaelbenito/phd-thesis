\section{Experimental details}

\subsection{Sensor fabrication}

We had previously fabricated colorimetric indicators in several forms: soaked cellulose \citep{Fernndez2017}, dip-coating \citep{Schmitt2017} or screen-printing \citep{Engel2020}. The later method  provides a more reliable fabrication in terms of reproducibility. Also, screen-printing is the entry point to other printing techniques such as rotogravure or flexography, among other industrial printing technologies \citep{leach1993printing}.

Then, we fabricated our current sensors using a \textit{screen-printing} manual machine in our laboratory. The screens were created according to the designs presented in \autoref{sec:colorqrcodeforcolorimetricindicators}. The substrate was a \textit{coated white cardboard of 350 gr/m²}, the coating was \textit{mate polypropylene}. And, the Color QR Codes were previously printed using ink-jet technology (see~\autoref{fig:printing_screens}).

Sensors were printed in batches including, in each Color QR Code: a $ CO_2 $  sensor, based on the mCP+PR color indicators; and a $ NH_3 $ sensor \citep{Engel2021}, based on the BPB color indicator. Here, we focused only in the $ CO_2 $, which is the blueish sensor before exposing it to the $ CO_2 $ target concentrations (see~\autoref{fig:printing_qr_codes}).


\begin{figure}[h!b]
    \centering
    \includegraphics[width=0.8\textwidth]{chapters/chapter7/figures/mallas.jpg}
    \caption{Two screens and one substrate sheet. Each screen can print one color indicator, and both can be combined into the same pattern. The substrate has DINA4 measures, it also contains up to 10 Color QR Codes with an approximated size of 1 inch.}
    \label{fig:printing_screens}
\end{figure}

\begin{figure}[h!b]
    \centering
    \includegraphics[width=0.8\textwidth]{chapters/chapter7/figures/qr_impresos.jpg}
    \caption{Several substrate sheets already printed. Each sheet contains up to 10 $ CO_2 $ sensors and 10 $ NH_3 $ sensors.}
    \label{fig:printing_qr_codes}
\end{figure}



\subsection{Experimental setup}


\begin{figure*}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter7/figures/MGP_station.jpg}
    \caption{Schema of our laboratory setup. The setup features 3 subsystems: a massflow controller station, a capture station and a user-access computer. The massflow controller station provides modified atmospheres to a chamber where the gas sensors are placed. The capture station can see the chamber through an optical window, and take time-lapses with a controlled light setting. Finally, the user computer presents a web page interface to the user to operate the system.}
    \label{fig:MGP_schema}
\end{figure*}


We designed and built our experimental setup from scratch. This experimental setup was used in the research of this thesis and related activities, e.g. seeking for new colorimetric indicators. The setup consisted of a complex system that responds to the necessity to capture colorimetric indicators targeting specific gases. Thus, the setup needed to solve not only the optical measurement, but also the management of target gas flows (see~\autoref{fig:MGP_schema}). 

The setup consisted of three main subsystems: 

\begin{enumerate}
    \item \textbf{Mass-flow control station:} a tiny desktop computer, a Lenovo ThinkCentre M93 Tiny, implemented the software to control up to five BROOKS 5850S mass-flow controllers \citep{brooks2008}, which fed a sensor chamber with the desired gas mix (see~\autoref{fig:chamber_design}). The control over the BROOKS devices was done in LabVIEW~\textregistered~\citep{bitter2006labview}. 
    
    Also, a LabVIEW front-end screen was implemented to enable user interaction in this subsystem. The tiny computer was equipped with a touchscreen to ease user interaction, but usually this station is set to automatic when using the setup to perform long-term data acquisition. 
    
    \begin{marginfigure}
    \centering
    \includegraphics[width=0.8\textwidth]{chapters/chapter7/figures/chamber.jpg}
    \caption{The 3D design of the circular sensor chamber. The chamber is transparent to enable optical readings, and it is sealed using rubber (orange). The chamber also has four threaded inlets/outlets.}
    \label{fig:chamber_design}
    \end{marginfigure}
    
    Moreover, LabVIEW used a serial communication protocol using the BROOKS official Windows DLL \citep{brooks2013} with some hardware protocol adaptations (USB $ \Leftrightarrow $ RS232 $ \Leftrightarrow $ RS485).
    
    
    \newpage
    \item \textbf{Capture station:} a single-board computer, a Raspberry Pi 3B+, implemented the software to control a digital camera (Raspberry Pi Camera v2) \citep{Pagnutti2017} and a strip of RGB LEDs from Phillips Hue \citep{Cusano2016} that acted as a variable light source. 
    
    Then, the control software of both the camera and the light strip was implemented in Python \citep{van1995python}. Specifically, the \texttt{picamera} module was used to drive the camera, and the \texttt{phue} one to drive the LED strip \footnote{Note this part of the setup was also used in \autoref{ch:5} when exposing the Color QR Codes to the \textit{colorimetry setup} channel.}.
    
    \vspace{1em}
    
    \item \textbf{User station:} a desktop computer implemented the user control software to manage all the system. This software was implemented using Python again, but with a different stack in mind: \texttt{flask} was used as a back-end service \citep{grinberg2018flask}, and \texttt{bokeh} was used to present plots in the front-end \citep{bokeh2018}. The front-end was based upon a web-based technology that uses the popular Chromium Embedded Framework to contain the main application \citep{cefpython}. 
\end{enumerate}

\subsubsection{Gas concentrations}

The colorimetric indicator was exposed to a series of pulses of different controlled atmospheres. In total, it was exposed to \textnum{15} pulses, \textnum{100} minutes each, that consisted of exposing the sensor \textnum{30} min to the target gas $ CO_2 $, followed by an exposure of \textnum{70} min to a clean atmosphere of synthetic air (\textnum{21\%} oxygen, \textnum{79\%} nitrogen). With this, the experiment lasted 25 hours. 

Table 6.1 shows the expected concentration of the $ CO_2 $ pulses versus the measured and corrected ones against dilution laws, as indicated by the manufacturer \citep{brooks2008}. The target gas was diluted using synthetic air. We configured the experiment to repeat 3 times the same pulse for 5 different target concentrations: \textnum{20\%}, \textnum{30\%}, \textnum{35\%}, \textnum{40\%} and \textnum{50\%}. 

\begin{margintable}
    \centering
\begin{tabular}{r|rr}
\toprule
Pulse &  Expected [\%] &                            Measured [\%] \\
\midrule
% 0  &           0.0 &                             0.12$\ \pm \ $0.25 \\
1  &          20.0 &                            25.21$\ \pm \ $0.00 \\
2  &          20.0 &                            25.22$\ \pm \ $0.22 \\
3  &          20.0 &                            25.22$\ \pm \ $0.22 \\ \hline
4  &          30.0 &                            36.62$\ \pm \ $0.22 \\
5  &          30.0 &                            36.67$\ \pm \ $0.31 \\
6  &          30.0 &                            36.67$\ \pm \ $0.31 \\ \hline
7  &          35.0 &                            42.10$\ \pm \ $0.40 \\
8  &          35.0 &                            42.30$\ \pm \ $0.40 \\
9  &          35.0 &                            42.20$\ \pm \ $0.40 \\ \hline
10 &          40.0 &                            46.90$\ \pm \ $0.40 \\
11 &          40.0 &                            47.30$\ \pm \ $0.40 \\
12 &          40.0 &                            47.40$\ \pm \ $0.40 \\ \hline
13 &          50.0 &                            57.60$\ \pm \ $0.50 \\
14 &          50.0 &                            57.60$\ \pm \ $0.50 \\
15 &          50.0 &                            57.60$\ \pm \ $0.50 \\
\bottomrule

\end{tabular}
\label{tab:gas_concentrations}
\caption{The expected and the measured gas concentration for each gas pulse is shown. The measured values were taken from the BROOKS instrumentation reading while applying a correction algorithm provided by the manufacturer \citep{brooks2008}.}
\end{margintable}

These concentrations were selected as the $ CO_2 $ indicator was designed to tackle the scenario of modified atmosphere packages (MAP) -- \textnum{20\%} to \textnum{40\%} of $CO_2$ -- \citep{Church1995}. Also, this is why, the synthetic air was partially exposed to a humidifier to achieve proper work conditions for the colorimetric indicator, i.e. resembling a MAP containing some fresh food, like meet, fish or vegetable.  

\autoref{fig:gas_pulses} shows a detailed view on the above-mentioned gas pulses, all the measures are shown for the target gas channel ($CO_2$), and both the dry and the humid synthetic air atmospheres (SA). 


\newpage


\begin{figure*}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter7/figures/gas_pulses.jpg}
    \caption{The expected (black) and the measured gas concentration (red) for each gas pulse shown on a temporal axis along the experiment duration. The measured values were taken from the BROOKS massflows, reading while applying a correction algorithm provided by the manufacturer \citep{brooks2008}.}
    \label{fig:gas_pulses}
\end{figure*}


\subsubsection{Capture settings}

The sensor was exposed to different illumination conditions of white light. This was achieved using the above-mentioned Phillips Hue light system. Color temperatures ranged from 2500K to 6500K, in steps of 500K (see~\autoref{fig:chamber_illuminants}).  These are less aggressive light conditions than those we used in \autoref{ch:5} when studying the capacity of QR Codes in the colorimertry setup. 

Also, the camera settings were fixed, without auto-exposition nor auto white-balance, to capture consistent images through all the dataset. This ensured we can extract the color reference tables during color correction only in the 9 first images.


\begin{figure*}
    \centering
    \includegraphics[width=0.30\textwidth]{chapters/chapter7/figures/image-00000008432.714-2500K.jpg}
    \includegraphics[width=0.30\textwidth]{chapters/chapter7/figures/image-00096049350.055-4500K.jpg}
    \includegraphics[width=0.30\textwidth]{chapters/chapter7/figures/image-00096049350.055-6500.jpg}
    \caption{A printed sensor, featuring a Color QR Code and two colorimetric indicators is displayed inside the sensor chamber of our setup. Then is exposed to different light conditions. From left to right, the illumination changes following 3 color temperatures of white light: 2500K, 4500K and 6500K.}
    \label{fig:chamber_illuminants}
\end{figure*}


Notice, that the number of different light illuminations (9) was kept low to preserve an adequate sampling rate of the sensor response dynamics. Our setup performs the captures in a synchronous way: an image is taken, then the color illumination changes, then another image is taken, etc. The global sampling rate was \1 FPS, which is the maximum frame rate a Raspberry Pi Camera can process at Full HD quality (\textnum{1920 \times 1088} pixels). Then, the actual frame rate for each illumination stream was \textnum{1/9} FPS.  

\newpage
\subsection{Expected response model}

In previous works, we already studied the relation between the colorimetric response of an indicator and the presence of the target gas \citep{Engel2021,Benito-Altamirano2018,Schmitt2017,Engel2020}. The relation we found was:

\begin{equation}
    S [\%] = m \log(c) + n
    \label{eq:model}
\end{equation}

\noindent
where $ S $ is the colorimetric response of the sensor, $ c $ is the concentration of the target gas in the atmosphere, $ m $ and $ n $ are the constants of a linear law. Then, the colorimetric response is linear with the logarithm of the gas concentration. $ m $ represents the sensitivity towards the logarithm of the gas concentration, and $ n $ the response at very low concentrations. 

Also, we can recover the sensitivity as a function of the gas concentration using derivates and use it to compute the error of the model for each concentration. To do so, we will use error propagation rules: 

\begin{equation}
    \left. \frac{\Delta S}{\Delta c} \right|_c = \frac{m}{c} \Longrightarrow 
    \Delta c |_c =  \Delta S |_c \cdot \frac{c}{m}
    \label{eq:sensitivity}
\end{equation}

\noindent
where $ c $ is a given concentration recovered with the inverted \autoref{eq:model}, $ m $ depends on each fitted model, $ \Delta S |_c $ is the error of the measured signal response and  $ \Delta c |_c $ is the model error for this given concentration. 

Finally, the signal color response $ S [\%] $ is usually normalized following a metric. We defined this metric to resemble the normalization performed in an electronic gas sensor \citep{Engel2021}. This kind of normalization divides the measured signal by the signal value assigned to the zero gas concentration, this produces a metric that is not upper-bounded $[0, \infty) $, the closer the initial \textit{resistance value} is to zero, the greater the response. Let us adapt this normalization for a red channel of a colorimetric indicator:

\begin{equation}
    S_r [\%] = 100 \cdot \frac{r(c) - r_0}{r_0 - r_{ref}} 
    \label{eq:model_response}
\end{equation}


\noindent
where $ S_r $ is the response in \% of the red channel, $ c $ the concentration of the target gas in \%, $ r(c) $ the raw red sensor signal with an 8-bit resolution (0–255), $ r_0 $ the value of $ r(c=0 \textrm{\%})$ and $ r_{ref} $ an absolute color reference which acts as the \textit{"zero resistance"} compared to electronic sensors. For our sensor the value is $ (r_{ref}, g_{ref}, b_{ref}) = (0, 0, 255)$, as our measured blue channel signal decreases when the gas concentration increases \citep{Zhang2016}.  

\newpage