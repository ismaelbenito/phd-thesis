The rise of the smartphone technology developed in parallel to the popularization of digital cameras enabled an easier access to photography devices to the people. Nowadays, modern smartphones have onboard digital cameras that can feature good color reproduction for imaging uses \citep{ContrerasNaranjo2016}. 

Alongside with this phenomenon, there has been a popularization of color-based solutions to  detect biochemistry analytes \citep{PiriyaVS2017}. Both phenomena are probable to be linked, since the first one eases the second. Scientists who want to pursue research to discover new or improve existent color-based analytics found themselves with better and better imaging tools, spending fewer and fewer resources. 

Color-based sensing \citep{PiriyaVS2017} is often preferred over electronic sensing \citep{Fabrega2017} for three reasons: \textnum{1}) the rapid detection of the analytes; \textnum{2}) the high sensitivity; and \textnum{3}) the high selectivity of colormetric sensors.  Nevertheless, imaging acquisition on smartphone devices still presents some acquisition challenges, and how to overcome those challenges is still an open debate \citep{Fernandes2020}.

This is why, the ERC-StG \textit{BetterSense} project (ERC n. 336917) was granted the extension ERC-PoC \textit{GasApp} project (ERC n.727297). Bettersense aimed to address the high power consumption and the poor selectivity of electronic gas sensor technologies \citep{bettersense}. GasApp aimed to bring the capability to detect gases to smartphone technology, relying on color-based sensor technology \citep{gasapp}. 

The accumulated knowledge from BetterSense was translated into the GasApp project to create colorimetric indicators to sense target gases, the GasApp proposal is detailed in \autoref{fig:bettersense2gasapp}. Later on, the \textit{SnapGas} project  (Eurostars n. 11453) was also granted to carry on this research topic, and apply the new technology to other colorimetric indicators to sense environmental gases \citep{snapgas}. 

\newpage

\begin{figure*}
    \centering
    \includegraphics[width=0.45\textwidth]{chapters/chapter1/figures/bettersense_to_gasapp.jpg}
    \includegraphics[width=0.45\textwidth]{chapters/chapter1/figures/gasapp.pdf}
    \caption{The GasApp proposal. Left, GasApp changed the core sensing technology from electronic to colorimetric indicators. Right, the initial idea of the GasApp project, a card where colorimetric dyes are printed alongside with color charts and a QR Code.  }
    \label{fig:bettersense2gasapp}
\end{figure*}


The GasApp proposal was based on changing from electronic devices to colorimetric indicators, thus leveraging the electronic components of the sensor readout to handheld smartphones. To do so, GasApp proposed a solution implementing an array with colorimetric indicators placed on top of a card-sized substrate to be captured by a smartphone device (see~\autoref{fig:bettersense2gasapp}). 

The design of this array of colorimetric indicators presented several challenges, such as: detecting and extracting the card and the desired region of interest (sensors), embedding one or more color charts and later perform color correction techniques to achieve adequate sensor readouts at any possible scenario a mobile phone could take a capture. 


The research of this thesis started in this context, and the work here presented aims to tackle these problems and resolve them with an integral solution. Let us go deeper in some of these challenges to properly formulate our thesis proposal. 

\textit{First}, the fabrication of the color-based sensors presents a challenge itself. There exists is a common starting point in printed sensors technologies to use ink-jet printing as the first approach to the problem to fabricate a printed sensor \citep{Moya2017,Salim2017}. However, ink-jet printing is an expensive and often limited printing technology from the standpoint of view of mass-production \citep{leach1993printing}.

\textit{Second}, color reproduction is a wide-known challenge of digital cameras \citep{Hunt2005}. Often, when a digital camera captures a scene it can produce several artifacts during the capture (e.g. underexposure, overexposure, ...), this is represented in~\autoref{fig:color_artifacts}. 

The problem of color reproduction, involves a directly linked problem: the problem of achieving image consistency among datasets \citep{Afifi2019}. While \textit{color reproduction} aims at matching the color of a given object when reproduced in another device as an image (e.g. a painting, a printed photo, a digital photo on a screen, etc.), \textit{image consistency} is the problem of taking different images of the same object in different illumination conditions and with different capturing devices, to finally obtain the same apparent colors for this object. 

\newpage
Usually, both problems are solved with the addition of \textit{color rendition chart}s to the scene. Color charts are machine-readable patterns which contain several color references \citep{McCamy1976}. Color charts bring a systematic way of solving the image consistency problem by increasing the amount of color references to create subsequently better color corrections than the default white-balance \citep{Finlayson2005,Menesatti2012}. 



\begin{figure}[h!t]
    \centering
    \includegraphics[trim={1.5em 0 3em 0},clip,width=\textwidth]{chapters/chapter1/figures/color_artifacts.pdf}
    \caption{Simplified 1D representation of the color reproduction problem in reversible and in non-reversible conditions. For clarity only one color coordinate has been represented: $ x $  stands for R, G, or B, and $ x’ $  stands for R’, G’, or B’.  Object colors (x) appear to be different (x’) after being acquired by digital means. In some situations, these alterations cannot be removed, because the transformation from $ x' $ to $ x $ is not single-valued (the critical color ranges where this problem occurs are highlighted with the green markers).}
    \label{fig:color_artifacts}
\end{figure}


\textit{Third}, using smartphones to acquire image data often presents computer vision challenges. On the one hand, authors preferred to enclose the smartphone device in a fixed setup  \citep{Long2017,Shin2018}. On the other hand, there exists a consolidated knowledge on computer vision techniques, which could be applied to readout colorimetric sensors with handheld smartphones \citep{GonzalezWoodsEddins2011}. 

Computer vision often seeks to extract features from the captured scene to be able to perform the desired operations on the image, such as: projective corrections, color readouts, etc. These features are objects with unique contour metrics or shapes, like the \textit{ArUco codes} (see~\autoref{fig:aruco_codes}) used in augmented reality technology \citep{Garrido-Jurado2016}. 

Moreover, 2D barcode technology is based upon this principle: encode data into machine-readable patterns which are easy to extract from a scene thanks to their uniqueness. QR Codes are the most known 2D barcodes \citep{iso18004_2015}. 

\begin{marginfigure}
    \centering
    \includegraphics[width=0.75\textwidth]{chapters/chapter1/figures/aruco_layout.jpg}
    \caption{Four examples of ArUco codes. These codes present certain feature uniqueness (rotation, non-symmetry, etc.), which enables easy location and identification on a scene.  }
    \label{fig:aruco_codes}
\end{marginfigure}

This is why, other authors had proposed solutions to print QR Codes with using colorimetric indicators as their printing ink. Rendering QR Codes which change its color when the target substance is detected \citep{Xu2021}. Even, using colorimetric dyes as actuators, where authors enhanced the QR Code capacity instead of sensing any material \citep{Ramalho2018}. 


Altogether, the presented solutions did not fully resolve what GasApp needed: an integrated, disposable, cost-effective machine-readable pattern to allocate colorimetric environmental sensors. The state-of-the-art research presented partial solutions, i.e. the colorimetric indicator was tackled, but there was not a proposal on how to perform automated readouts. Or, the sensor was arranged in a QR Code layout, but color correction was not tackled. Or, the color calibration problem was approached, but any of the other two problems were tackled, etc. 

To solve those challenges, we proposed the creation of an integrated machine-readable pattern based on QR Codes, which would embed both the color correction patches and the colorimetric indicators patches. And, those embeddings ought to be back-compatible with the QR Code standard, to maintain the data storage capabilities of QR Codes for traceability applications \citep{iso18004_2015}. A representation of this idea is portrayed in \autoref{fig:thesis_proposal}. 

\vspace{1.5em}

\begin{figure*}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{chapters/chapter1/figures/gasapp_to_thesis.jpg}
    \caption{Our thesis proposal to create machine-readable patterns that can accommodate colorimetric sensors and color charts, alongside with the digital information of the QR Code.}
    \label{fig:thesis_proposal}
\end{figure*}

\vspace{1.5em}

The novelty of the idea led us to submit a patent application in 2018, which was granted worldwide in 2019, and now is being evaluated in National phases \citep{BenitoAltamirano2019}. Moreover, we launched \textit{ColorSensing}, a spin-off company from \textit{Universitat de Barcelona} to develop further the technology in industrial applications \citep{colorsensing}. 
\vspace{1em}
 
The strong points of the back-compatible proposal were:
\vspace{-0.75em}

\begin{itemize}
    \item the use of pre-existent computer vision algorithms to locate QR Codes, freeing the designed pattern of redundant computer vision features, as those 'circles' seen outside the GasApp card (\autoref{fig:thesis_proposal}), which are redundant with the finder patterns of the QR Code (corners of the QR Code); 
    \item the reduced size presented by a QR Code (\autoref{fig:thesis_proposal}), the original GasApp proposal was set to a business card size (\textnum{3.5} $\times$ \textnum{2.0} inches), while our QR Code proposal is smaller (\1 $ \times $ \1 inch);
    \newpage
    \item reducing the barrier between the new technology and the final users, as the back-compatible proposal maintains the mainstream standard of the QR Codes. By this, one could simply encode a desired URL in the QR Code data alongside with the color information and always be able to redirect the final user to a download link of the proper reader, which enables the color readout;
    \item and, the capacity to increase the color references embedded in a color chart, while also reducing the global size of the chart, (e.g. the usual size of a commercial ColorChecker is about 11 $ \times $ 8.5 inches, and it encodes 24 color patches), using modern machine-readable standard (such as QR Codes as an encoding base) enables a systematic path to increase the capacity per surface unit, and subsequently according to color correction theory, leading to a better color corrections having more color references.
\end{itemize}





\input{chapters/chapter1/sections/objectives}
\input{chapters/chapter1/sections/sctructure}

% \begin{figure}
%     \centering
%     \includegraphics{chapters/chapter1/figures/qr_code_zones.jpg}
%     \caption{Zones of a standard QR Code. Tampering with any of these parts (protected areas: 1.,2.,3.,4.) could lead to an unreadable QR Codes. Data is stored in the non-highlighted parts (5.), alongside with Reed-Solomon error correction blocks.}
%     \label{fig:qr_code_zones}
% \end{figure}
    
    
% \begin{figure}
%     \centering
%     \includegraphics[width=0.66\textwidth]{chapters/chapter1/figures/qrcolochecker_proposal.jpg}
%     \caption{Regions of an extended "QR-ColorChecker". The colors would be superimposed to the original QR Code, replacing the standard “white” and “back” QR Code modules.}
%     \label{fig:qr_colorchecker}
% \end{figure}

