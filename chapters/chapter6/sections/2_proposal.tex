% \newpage
\section{Proposal}
\label{sec:ch6_proposal}

In this chapter, we focus on the implementation of thin-plate spline color corrections, specifically in the use of the TPS3D method to perform color correction in image datasets directly in RGB spaces, while proposing an alternative radial basis function \citep{Buhmann2000} to be used to compute the TPS, and introducing a smoothing factor to approximate the solution in order to reduce corner-case errors \citep{Sprengel1996}.  All in all, we illustrate here the advantages and limitations of the new TPS3D methodology to color correct RGB images.

\newpage
Solving the image consistency problem, introduced in \autoref{ch:3}, using the TPS3D method requires the creation of an image dataset. The images on this dataset must contain some type of color chart. Also, the captured scenes in the images must be meaningful, and the color chart must be representative of those scenes. Here, we propose to use the widely-accepted Gehler’s ColorChecker dataset \citep{Gehler2008, Hemrit2018}, which contains 569 images with a 24 patch Macbeth ColorChecker placed in different scenes (see~\autoref{fig:gehler_example}). We do so, rather than creating our own dataset with the Back-compatible Color QR Codes proposed in \autoref{ch:5} because this is a standard dataset with a standard color chart. In \autoref{ch:7}, we will combine both techniques into a colorimetric sensor.  

\begin{marginfigure}
    \centering
    \includegraphics{chapters/chapter6/figures/gehrler_image_example.jpg}
    \caption{An example of a Gehler's ColorChecker dataset image.}
    \label{fig:gehler_example}
\end{marginfigure}


Moreover, we propose to apply a data augmentation technique to increase the size of the dataset by 100, to match in size other augmented image datasets that have appeared recently. We do not use those images as they do not always contain a ColorChecker \citep{Afifi2019}. 

Furthermore, we propose to benchmark our TPS3D against its former implementation \citep{Menesatti2012} and a range of alternative methods to correct the color in images, such as: white balance \citep{Gong2013}, affine \citep{Gong2013}, polynomial \citep{Cheung2004, Finlayson2015}  and root-polynomial \citep{Finlayson2015} corrections. Benchmarking includes both quantitative color consistency and computational cost metrics \citep{Menesatti2012, Luo2014}.


In the following we review the derivation of the above-mentioned color correction methods before introducing our improvements to the TPS3D method. Notice the formulation will remind to the 2D projection formulation from \autoref{ch:4}, however some differences have to be considered: 

\begin{itemize}
    \item as described in \autoref{ch:3}, color corrections are \textbf{mappings between 3D spaces},
    \item unlike projective transformations in 2D planes, we will \textbf{only be using affine terms} as basis, and
    \item our notation in this formulation avoids the use of homogeneous coordinates $ (p_0, p_1, p_2) $ in favor of more verbose notation using the \textbf{name of each color channel} $ (r, g, b) $.
\end{itemize}

\newpage

\subsection{Linear corrections}

In \autoref{ch:3} we defined color corrections as an application $ f $ between two RGB color spaces. If this application is to be linear, thus a \textit{linear correction}, we can use a matrix product notation to define the correction \citep{Finlayson2005,Gong2013}:

\begin{equation}
   \mathbf{s}' = f(\mathbf{s}) = \mathbf{M}  \cdot \mathbf{s} 
\end{equation}

\noindent
where $ \mathbf{M} $ is a 3 $\times$ 3 linear matrix that maps each color in the origin captured color space $ \mathbf{s} = (r, g, b) $  to the corrected color space $ \mathbf{s}' = (r', g', b') $ (see~\autoref{fig:NONE}). In order to solve this system of equations, we must substitute these vectors by matrices containing enough color landmarks (known pairs of colors in both spaces) to solve the system for $ \mathbf{M} $: 

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/comparison_NONE.jpg}
    \caption{RGB colors of the ColorChecker of an image projected in the \textit{red-green} plane: ($\circ$) show the original colors of the ColorChecker and ($\times$) show their altered version, as in their captured values.}
    \label{fig:NONE}
\end{marginfigure}

\begin{equation}
   \mathbf{M} \cdot \mathbf{P} = \mathbf{Q} 
   \label{eq:MPQ}
\end{equation}

\noindent
where $ \mathbf{Q} $ is matrix with $ \mathbf{s}' $ colors and $ \mathbf{P} $ is matrix with  $ \mathbf{s} $ colors. 

\subsubsection{White-balance correction}

White-balance is the simplest color transformation that can be applied to an RGB color. In the white-balance correction each channel of vector function $ f $ is independent:

\begin{equation}
    \begin{split}
        r' &= f_r (r) = \frac{r_{max}}{r_{white}} \cdot r \\
        g' &= f_g (g) = \frac{g_{max}}{g_{white}} \cdot g \\
        b' &= f_b (b) = \frac{b_{max}}{b_{white}} \cdot b 
    \end{split}
    \label{eq:whitebalance}
\end{equation}

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/comparison_AFF0.jpg}
    \caption{RGB colors of the ColorChecker of an image projected in the \textit{red-green} plane: ($\circ$) show the original colors of the ColorChecker and ($\times$) show the corrected values of the altered version shown in \autoref{fig:NONE} using a \textit{white-balance correction}. The whitest point (upper right) is the only one that is properly corrected.}
    \label{fig:AFF0}
\end{marginfigure}

\noindent
where $ (r_{max}, g_{max}, b_{max})  $ is the maximum value of each channel in the color corrected space, e.g. $ (255, 255, 255) $ for 24-bit images;  and $ (r_{white}, g_{white}, b_{white}) $ is the measured whitest color in the image (see~\autoref{fig:AFF0}). This relation can be easily written as a matrix, and only needs one color reference to be solved (from~\autoref{eq:MPQ}): 


\begin{equation}
    \begin{pmatrix} 
        a_r & 0 & 0 \\
        0 & a_g & 0 \\
        0  & 0  &  a_b 
    \end{pmatrix} \cdot \begin{pmatrix} 
        r \\ g \\ b 
    \end{pmatrix} = \begin{pmatrix} 
        r' \\ g' \\ b' 
    \end{pmatrix} 
\end{equation}

\noindent
where $ a_k $ are the weight contributions of \autoref{eq:whitebalance} for each $ k $ channel. 



The white-balance correction can be improved by subtracting the black level of the image before applying the white-balance correction (see~\autoref{fig:AFF1}). For example, this improvement looks like (shown for the red channel for simplicity):

\begin{equation}
    r' = f_r (r)  = r_{min} + \frac{r_{max} - r_{min}}{r_{white} - r_{black}} \cdot (r - r_{black})
    \label{eq:whitebalanceblacksubtraction}
\end{equation}

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/comparison_AFF1.jpg}
    \caption{RGB colors of the ColorChecker of an image projected in the \textit{red-green} plane: ($\circ$) show the original colors of the ColorChecker and ($\times$) show the corrected values of the altered version shown in \autoref{fig:NONE} using a \textit{white-balance with black-subtraction correction}. The whitest point (upper right) and the blackest point (lower left) are the only ones properly corrected.}
    \label{fig:AFF1}
\end{marginfigure}

\noindent
where $ r_{min} $ is the minimum value of the channel red possible in the color corrected space, e.g. 0 for 24-bit images; and $ r_{black} $ is the red value of the measured darkest color in the image. \autoref{eq:MPQ} is still valid for this linear mapping, but $ f $ becomes a composed application ($ f: \mathbb{R}^3 \to \mathbb{R}^4 \to \mathbb{R}^3  $), where $ \mathbf{M} $ becomes a 3 $\times$ 4 matrix, and we need to expand the definition of the $ \mathbf{P} $ colors using a homogeneous coordinate: 



\begin{equation}
    \begin{pmatrix} 
        a_r & 0 & 0 & t_r \\
        0 & a_g & 0 & t_g \\
        0  & 0  &  a_b & t_b 
    \end{pmatrix} \cdot \begin{pmatrix} 
        r_1 & r_2 \\
        g_1 & g_2 \\
        b_1 & b_2 \\
        1 & 1 
    \end{pmatrix} = \begin{pmatrix}  
        r'_1 & r'_2 \\
        g'_1 & g'_2 \\
        b'_1 & b'_2 
    \end{pmatrix} 
\end{equation}

\noindent
where $ a_k $ are the affine contributions and $ t_k $ are the translation contributions for each $ k $ channel, and now two points are required to obtain the color correction weights. 

\subsubsection{Affine correction}

White balance is only a particular solution of an affine correction. We can generalize \autoref{eq:whitebalance} for, e.g., the red channel to accept contributions from green and blue channels: 

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/comparison_AFF2.jpg}
    \caption{RGB colors of the ColorChecker of an image projected in the \textit{red-green} plane: ($\circ$) show the original colors of the ColorChecker and ($\times$) show the corrected values of the altered version shown in \autoref{fig:NONE} using an \textit{affine correction}. We could choose to fix 3 points, but here we applied an approximated solver to the system, so any of the points is strictly matched.}
    \label{fig:AFF2}
\end{marginfigure}

\begin{equation}
    r' = f_r (r, g, b)  = a_{r, r} \cdot r  + a_{r, g} \cdot g + a_{r, b} \cdot b = \sum_k^{r,g,b} a_{r, k} k
\end{equation}

\noindent
this expression is connected with the full matrix implementation, with 9 unknown weights:

\begin{equation}
     \begin{pmatrix} 
        a_{r,r} & a_{r,g} & a_{r,b} \\
        a_{g,r} & a_{g,g} & a_{g,b} \\
        a_{b,r} & a_{b,g} &  a_{b,b} 
    \end{pmatrix}\cdot \begin{pmatrix}  
        r_1 & r_2 & r_3 \\
        g_1 & g_2 & g_3 \\
        b_1 & b_2 & b_3 \\
        1 & 1 & 1 
    \end{pmatrix} = \begin{pmatrix}  
        r'_1 & r'_2 & r'_3 \\
        g'_1 & g'_2 & g'_3 \\
        b'_1 & b'_2 & b'_3 
    \end{pmatrix}
\end{equation}

\noindent
where $ a_{j,k} $ are the weights of the $ \mathbf{M} $ matrix, and we need 3 known colors references to solve the system (see~\autoref{fig:AFF2}). 

\newpage

In turn, white-balance with black-subtraction \autoref{eq:whitebalanceblacksubtraction} is a specific solution of an affine transformation, which handles translation and can be generalized as:

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/comparison_AFF3.jpg}
    \caption{RGB colors of the ColorChecker of an image projected in the \textit{red-green} plane: ($\circ$) show the original colors of the ColorChecker and ($\times$) show the corrected values of the altered version shown in \autoref{fig:NONE} using an \textit{affine correction with translation}. We could choose 4 points to fix, but here we applied an approximated solver to the system, so none of the points is strictly matched.}
    \label{fig:AFF3}
\end{marginfigure}


\begin{equation}
    r' = f_r (r, g, b)  = t_r + \sum_k^{r,g,b} a_{r, k} k
    \label{eq:affine}
\end{equation}

\noindent
also tied up to its matrix representation:

\begin{equation}
     \begin{pmatrix} 
        a_{r,r} & a_{r,g} & a_{r,b} & t_r \\
        a_{g,r} & a_{g,g} & a_{g,b} & t_g \\
        a_{b,r} & a_{b,g} & a_{b,b} & t_b 
    \end{pmatrix} \cdot \begin{pmatrix}  
        r_1 & r_2 & r_3 & r_4 \\
        g_1 & g_2 & g_3 & g_4 \\
        b_1 & b_2 & b_3 & b_4 \\
        1 & 1 & 1 & 1 
    \end{pmatrix} = \begin{pmatrix}   
        r'_1 & r'_2 & r'_3 & r'_4 \\
        g'_1 & g'_2 & g'_3 & g'_4 \\
        b'_1 & b'_2 & b'_3 & b'_4 
    \end{pmatrix} 
\end{equation}

\noindent
where $ a_{j,k} $ and $ t_k $ are the weights of the $ \mathbf{M} $ matrix, and we require 4 known colors to solve the system (see~\autoref{fig:AFF3}). 

\subsection{Polynomial corrections}

As we have seen with affine corrections, we can expand the definition of the measured color space matrix $ \mathbf{P} $ including additional terms to it. This is useful to compute non-linear corrections using a linear matrix implementation. Formally, this space expansion can be seen as $ f $ being now a composed application:

\begin{equation}
    f: \mathbb{R}^3 \rightarrow \mathbb{R}^{3+N} \rightarrow \mathbb{R}^3
\end{equation}

\noindent
where $ \mathbb{R}^{3+N} $ is an extended color space derived from the original color space $ \mathbb{R}^3 $. We can write a generalization of \autoref{eq:affine} for polynomial corrections as follows: 

\begin{equation}
    r' = f_r (r, g, b)  = t_r + \sum_k^{r,g,b} a_{r, k} k + \sum_i^N w_{r,i} \Phi_i (r, g, b)
    \label{eq:polynomial}
\end{equation}

\noindent
where $ \Phi (r,g,b) = \{\Phi_i (r,g,b) \}, \ i=1,\cdots,N $ is a set of monomials, $ w_i $ are the weight contributions for each monomial and $ N $ is the length of the monomial set \citep{Finlayson2015}. 

The monomials in the set $ \Phi (r,g,b) $ will have a degree 2 or more, because we do not unify the affine parts as monomials, we do so to emphasize their contribution to the correction. Also, notice that $ N $ is arbitrary, and we can choose how we construct our polynomial expansions by tuning the monomial generator $ \Phi_i (r, g, b) $. 

\newpage
Despite that, $ N $ always relates to the number of vectors needed in \autoref{eq:MPQ} to solve the system.  $ \mathbf{M} $ takes the form of a $ 3 \times (4+N) $ matrix, $ \mathbf{P} $ takes the form of a $ (4 + N) \times (4 + N) $ matrix and $ \mathbf{Q} $ takes the form of a $ 3 \times (4 + N) $ : 

\begin{fullwidth}
\begin{equation}
     \begin{pmatrix} 
        w_{r,N} & \cdots & w_{r,1} & a_{r,r} & a_{r,g} & a_{r,b} & t_r \\
        w_{g,N} & \cdots & w_{g,1} & a_{g,r} & a_{g,g} & a_{g,b} & t_g \\
        w_{b,N} & \cdots & w_{b,1} & a_{b,r}  & a_{b,g}  &  a_{b, b} & t_b 
    \end{pmatrix} \cdot \begin{pmatrix} 
        \Phi_{N,0} & \Phi_{N,2} & \dots & \Phi_{N,N+4}  \\
        \vdots & \vdots & \vdots & \vdots \\
        \Phi_{1,1}  & \Phi_{1,2} & \dots & \Phi_{1, N+4} \\
        r_1 & r_2 & \dots & r_{N+4} \\
        g_1 & g_2 & \dots & g_{N+4} \\
        b_1 & b_2 & \dots & b_{N+4} \\
        1 & 1 & \dots & 1 
    \end{pmatrix} = \begin{pmatrix}  
        r'_1 & r'_2 & \dots & r'_{N+4} \\
        g'_1 & g'_1 & \dots & g'_{N+4} \\
        b'_1 & b'_1 & \dots & b'_{N+4} 
    \end{pmatrix}
    \label{eq:polynomialmatrix}
\end{equation}
\end{fullwidth}

\subsubsection{Geometric polynomial correction}

The simplest polynomial expansion of a color space occurs when $ \Phi (r, g, b) $ generates a pure geometric set: 

\begin{equation}
     \Phi (r, g, b)  = \left\{k^\upalpha  : \ 2 \leq \upalpha \leq  D   \right\}
     \label{eq:polyset0}
\end{equation}

\noindent
where $ k \in \{r, g, b\} $ is any of the RGB channels, $ \upalpha $ is the degree of a given monomial of the set and $ D $ is the maximum degree we choose to form this set (see~\autoref{fig:VAN2}). For example, for $ D = 3 $ , it will produce the set:

\begin{equation}
    \Phi_{D=3} (r, g, b)  = \left\{r^2, \ g^2, \ b^2, \ r^3, \ g^3, \ b^3 \   \right\}
\end{equation}

Combining this expression with \autoref{eq:polynomialmatrix}, we can see we obtain a matrix that is directly related with the Vandermonde matrix \citep{Kalman1984}, but for 3D data instead of 1D data. 

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/comparison_VAN2.jpg}
    \caption{RGB colors of the ColorChecker of an image projected in the \textit{red-green} plane: ($\circ$) show the original colors of the ColorChecker and ($\times$) show the corrected values of the altered version shown in \autoref{fig:NONE} using a \textit{geometric polynomial correction of degree 4}. Many of the points are almost matched due to the polynomial expansion.}
    \label{fig:VAN2}
\end{marginfigure}

\subsubsection{Polynomial correction}

\autoref{eq:polyset0} can be generalized to take into account also cross-terms from any of the channels to create the monomial terms \citep{Cheung2004, Finlayson2015}. So, we can write now:

\begin{equation}
    \Phi (r, g, b)  = \left\{\prod_k^{rgb} k^{\upalpha_k} \  : \ 2 \leq | \upalpha | \leq  D   \right\}
    \label{eq:polyset1}
\end{equation}

\noindent
where $ | \upalpha | = \sum_k^{rgb} \upalpha_k $ is a metric, which is the sum of the degrees of each channel in the monomial, thus the degree of each monomial. 

\newpage

Following with the example where $ D = 3 $, now we obtain an expanded set:

\begin{fullwidth}
\begin{equation}
    \Phi_{D=3} (r, g, b)  = \left\{r^2, \ g^2, \ b^2, \ rg, \ gb, \ br, \ r^3, \ g^3, \ b^3, \  
rg^2, \ gb^2, \ br^2, \ gr^2, \ bg^2, \ rb^2, \ rgb \right\}
\end{equation}

\end{fullwidth}

\subsubsection{Root-polynomial correction}

Finally, a root-polynomial correction is defined modifying \autoref{eq:polyset1} to introduce the $ | \upalpha | $-th root to each monomial \citep{Finlayson2015}: 

\begin{equation}
     \Phi (r, g, b)  = \left\{\prod_k^{rgb} k^{\frac{\upalpha_k}{| \upalpha|}} \  : \ 2 \leq | \upalpha | \leq  D   \right\}
\end{equation}

So, this reduces the amount of terms of each set for a given degree $ D $. Then, our example with $ D= 3 $ becomes reduced to: 

\begin{fullwidth}
\begin{equation}
    \Phi_{D=3} (r, g, b)  = \left\{\sqrt{rg}, \sqrt{gb}, \sqrt{br}, \sqrt[3]{rg^2}, \sqrt[3]{gb^2}, \sqrt[3]{br^2}, \sqrt[3]{gr^2}, \sqrt[3]{bg^2}, \sqrt[3]{rb^2}, \sqrt[3]{rgb} \right\}
\end{equation}

\end{fullwidth}

Notice that all the terms present in a Vandermonde expansion have now disappeared, as they are now the same terms of the affine transformation due to the root application $ \{\sqrt[3]{r^3}, \sqrt[3]{g^3}, \sqrt[3]{b^3}\} = \{\sqrt{r^2}, \sqrt{g^2}, \sqrt{b^2}\} = \{r, g, b\} $, and the only remaining terms are the roots of the cross-products. 

\subsection{Thin-plate spline correction}

As an alternative to the former approaches, we can use thin-plate spline as the basis of the expansion to the color space in $ \mathbf{P} $ \citep{Menesatti2012}: 

% Thin-plate splines use the reference colors as centers to compute radial basis functions (RBF) []. RBF are real-valued functions:

% \begin{equation}
%     h: [0, \inf) \to \mathbb{R}
% \end{equation}

% that take into account a metric on a vector space. Their value only depends on the distance, according to this metric, to a reference fixed point, i.e. a reference color: 

% \begin{equation}
%     h_c(\mathbf{s}) = h(|| \mathbf{s} - \mathbf{c} ||)
% \end{equation}

% where  $ \mathbf{s} \in \mathbb{R}^3 $ is the unknown color in which the function is evaluated, $ \mathbf{c} \in \mathbb{R}^3 $ is the color reference used to compute the spline, $ h $ is a radial basis function, and Equation 28 reads as “$ h_c(\mathbf{s}) $ is a kernel of $ h $ in $ \mathbf{c} $ with the metric $ ||\cdot|| $”. Equation 16 becomes now: 

\begin{equation}
    r' = f_r (r, g, b)  = t_r + \sum_k^{r,g,b} a_{r, k} k + \sum_i^N w_{r,i} h_i (r, g, b)
    \label{eq:thinplate}
\end{equation}

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/comparison_TPS1.jpg}
    \caption{RGB colors of the ColorChecker of an image projected in the \textit{red-green} plane: ($\circ$) show the original colors of the ColorChecker and ($\times$) show the corrected values of the altered version shown in \autoref{fig:NONE} using a \textit{thin-plate spline correction}. All the points are strictly matched by the TPS definition.}
    \label{fig:TPS1}
\end{marginfigure}

\noindent
where $ w_i $ are the weight contributions for each spline contributions, and $ h_i (r, g, b) $ are kernels of $ h $ in the $ N $ known colors. We will follow the same formulation described in \autoref{ch:4}. A more detailed definition of $ h_i $ functions can be found there. Also, notice that this expression is really similar to \autoref{eq:polynomial} of polynomial corrections, the main difference is the fact that the number of $ N $ spline contributions equals to the number of color references (see~\autoref{fig:TPS1}). 

\newpage

\autoref{eq:MPQ} becomes now: 

\begin{fullwidth}
\begin{equation}
     \begin{pmatrix} 
        w_{r,1} & \cdots & w_{r,N} & a_{r,r} & a_{r,g} & a_{r,b} & t_r \\
        w_{g,1} & \cdots & w_{g,N} & a_{g,r} & a_{g,g} & a_{g,b} & t_g \\
        w_{b,1} & \cdots & w_{b,N} & a_{b,r}  & a_{b,g}  &  a_{b, b} & t_b 
    \end{pmatrix} \cdot \begin{pmatrix} 
        h_{1,1} & h_{1,2} & \dots & h_{1,N}  \\
        \vdots & \vdots & \vdots & \vdots \\
        h_{N,1}  & h_{N,2} & \dots & h_{N, N} \\
        r_1 & r_2 & \dots & r_{N} \\
        g_1 & g_2 & \dots & g_{N} \\
        b_1 & b_2 & \dots & b_{N} \\
        1 & 1 & \dots & 1 
    \end{pmatrix} = \begin{pmatrix}  
        r'_1 & r'_2 & \dots & r'_{N} \\
        g'_1 & g'_1 & \dots & g'_{N} \\
        b'_1 & b'_1 & \dots & b'_{N} 
    \end{pmatrix} 
    \label{eq:thinplatematrix}
\end{equation}
\end{fullwidth}

This system is unbalanced, as we have $ N $ colors vectors in $ \mathbf{P} $ and $ \mathbf{Q} $. In other corrections, we used four additional color references to solve the system, but here each new color is used to compute an additional spline, unbalancing the system again. Alternatively, the TPS formulation imposes two additional conditions \citep{bookstein-89}: the sum of $ w_{j,k} $ coefficients is to be \0, and their cross-product with the $ \mathbf{P} $ colors as well. As a consequence of such conditions, spline contributions tend to \0 at infinity, while affine contributions prevail. This makes our system of equations solvable, and it can be expressed as an additional matrix product: 

\begin{equation}
    \begin{pmatrix} 
        w_{r,1} & \cdots & w_{r,N} \\
        w_{g,1} & \cdots & w_{g,N} \\
        w_{b,1} & \cdots & w_{b,N} 
    \end{pmatrix}  \cdot \begin{pmatrix} 
        r_1 & ... & r_N \\
        g_1 & ... & g_N \\
        b_1 & ... & b_N \\
        1 & ... & 1 
    \end{pmatrix}^T  =  0 
\end{equation}

\subsubsection{Polynomial radial basis functions}

The radial basis functions (RBF) used to compute splines remains open to multiple definitions. The thin-plate approach to compute those splines implies using solutions of the biharmonic equation \citep{bookstein-89}: 

\begin{equation}
    \Delta^{2} U = 0
\end{equation}

\noindent
that minimize the bending energy functional described by many authors, thus resembling the spline solution to the trajectory followed by an n-dimensional elastic plate. These solutions are the polynomial radial basis functions and a general solution is provided for n-dimensional data as  \citep{Sprengel1996, Buhmann2000,Rohr2001}:

% \begin{fullwidth}
\begin{equation}
    h_c(\mathbf{s}) = U(\mathbf{s}, \mathbf{c}) = 
    \begin{cases}       
        || \mathbf{s} - \mathbf{c} ||^{2k-n} \ln|| \mathbf{s} - \mathbf{c} || & 2k-n \text{ is even} \\
        || \mathbf{s} - \mathbf{c} ||^{2k-n}  & \text{otherwise} 
    \end{cases} 
\end{equation}

% \end{fullwidth}

\noindent
where $ n $ is the number of dimensions, $ k $ is the order of the functional, $ \mathbf{s} $ and $  \mathbf{c} $ are the data points where the spline is computed and $ || \cdot || $ is a metric. 

% \break \setlength{\parskip}{1em}
For a bending energy functional (the metal thin-plate approach) $ k = 2 $ and $ n = 2 $ (2D data), we obtain the usual thin-plate spline RBF \citep{bookstein-89}:

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/comparison_TPS3.jpg}
    \caption{RGB colors of the ColorChecker of an image projected in the \textit{red-green} plane: ($\circ$) show the original colors of the ColorChecker and ($\times$) show the corrected values of the altered version shown in \autoref{fig:NONE} using a \textit{smoothed thin-plate spline correction}. Not all the points are strictly matched now, as we relaxed the TPS definition.}
    \label{fig:TPS3}
\end{marginfigure}

\begin{equation}
     h_c(\mathbf{s}) = || \mathbf{s} - \mathbf{c} ||^2 \ln|| \mathbf{s} - \mathbf{c} ||
     \label{eq:rbf2D}
\end{equation}

But for $ k = 2 $ and $ n = 3 $ (3D data) we obtain \citep{bookstein-89}: 

\begin{equation}
    h_c(\mathbf{s}) = || \mathbf{s} - \mathbf{c} || 
    \label{eq:rbf3D}
\end{equation}

It is unclear why in the TPS3D to color correct images, Menesatti et al. \citep{Menesatti2012} used the definition for 2D data (\autoref{eq:rbf2D}), rather than the actual 3D definition (\autoref{eq:rbf3D}) which according to the literature should yield to more accurate results. We will investigate here the impact of this change in the formal definition of the TPS3D.

So far, we have not defined a metric $ || \cdot || $ to solve the TPS contributions. We will follow Menesatti et al. and use the euclidean metric of the RGB space. We will also name this metric $ \Delta_{RGB} $, as it is commonly known in colorimetry literature \citep{Menesatti2012}:

% \begin{fullwidth}
\begin{equation}
     || \mathbf{s} - \mathbf{c} || = \Delta_{RGB}(\mathbf{s}, \mathbf{c}) = \sqrt{(r_s -  r_c)^2 + (g_s -  g_c)^2 + (b_s - b_c)^2}
     \label{eq:rgb_distance}
\end{equation}

% \end{fullwidth}

\subsubsection{Smoothing the thin-plate spline correction}

Approximating the TPS corrections is a well-known technique \citep{Rohr2001, Sprengel1996}. Specifically, this is performed in ill-conditioned scenarios where data is noisy or saturated, and strict interpolation between data points, leads to important error artifacts. We propose now adding  a smoothing factor to the TPS3D, to improve color correction in ill-conditioned situations. 

We approximated the TPS by adding a smoothing factor to the spline contributions, which reduces the spline contributions in favor of the affine ones (see~\autoref{fig:TPS3}). Taking \autoref{eq:thinplate}, we will introduce a smooth factor only for those color references where the center of the spline was those references themselves:  

\begin{equation}
    r'_j = f_r (r_j, g_j, b_j)  = t_r + \sum_k^{r_j,g_j,b_j} a_{r, k} k + \sum_i^N ( w_{r,i} h_i (r_j, g_j, b_j) + \lambda \delta_{ij})
\end{equation}

\noindent
where $ \lambda $ is the smoothing factor, and $ \delta_{ij} $ is a Kronecker delta. 

\break \setlength{\parskip}{1em}
Notice that in the previous TPS definition the spline contributions of a reference color to the same reference color were \0 under the euclidean metric we chose. Also, notice that the matrix product of \autoref{eq:thinplatematrix} is still valid, as we have only affected the diagonal of the upper part of the $ \mathbf{P} $ matrix. Thus,

\vspace{-1em}
\begin{fullwidth}
\begin{equation}
    \mathbf{P}_{smooth} = \mathbf{P} + 
        \begin{bmatrix} 
            \lambda \mathbf{I} \\
            \mathbf{O}(4,N) 
        \end{bmatrix}  = \begin{pmatrix} 
            h_{1,1} + \lambda & h_{1,2} & \dots & h_{1,N}  \\
            h_{2,1} & h_{2,2} + \lambda & \dots & h_{2,N}  \\
            \vdots & \vdots & \vdots & \vdots \\
            h_{N,1}  & h_{N,2} & \dots & h_{N, N}  + \lambda \\
            r_1 & r_2 & \dots & r_{N} \\
            g_1 & g_2 & \dots & g_{N} \\
            b_1 & b_2 & \dots & b_{N} \\
            1 & 1 & \dots & 1 
        \end{pmatrix} 
\end{equation}
\end{fullwidth}

\vspace{-0.5em}
\noindent
where $ \mathbf{P} $ is the matrix of color references and their TPS expansion, $ \mathbf{I} $ is the identity matrix and $ \mathbf{O} (4, N) $ is a matrix with \0s of size $ 4 \times N $.

% \newpage