\section{Experimental details}
\label{sc:ch6_experimental_details}

So far, we have reviewed the state-of-the-art methods to color correct images to achieve consistent datasets using color references as fixed points in color spaces to compute color corrections. Also, we have proposed two updates to the TPS3D method: using the suited RBF and smoothing the TPS contributions. 

In Table 1, we show a summary of all the corrections that we studied in this work using the dataset described in the next section. First, a perfect correction (PERF) and a non-correction (NONE) scenario are present as \textbf{reference}. Notice that perfect correction will display here the quantization error after passing from 12-bit images to 8-bit images. Then, several corrections have been implemented, that have been grouped by authorship of the methods and type of correction:

\begin{itemize}
    \item \textbf{Affine (AFF):} white-balance (AFF0), white-balance with black subtraction (AFF1), affine (AFF2), affine with translation (AFF3). 
    \item \textbf{Vandermonde (VAN):} four polynomial corrections from degree 2 to 5 (VAN0, VAN1, VAN2 and VAN3). 
    \item \textbf{Cheung (CHE):} from Cheung et al. \citep{Cheung2004}, four polynomial corrections with different terms: 5 (CHE0), 7 (CHE1), 8 (CHE2) and 10 (CHE3). 
    \item \textbf{Finlayson (FIN):} from Finlayson et al. \citep{Finlayson2015}, two polynomial and two root-polynomial, of degrees 2 and 3 (FIN0, FIN1, FIN2, FIN3). 
    \item \textbf{Thin-plate splines (TPS):}  TPS3D from Menesatti et al. \citep{Menesatti2012} (TPS0), our method using the proper RBF (TPS1) and the same method with two smoothing values (TPS2 and TPS3). 
\end{itemize}
\newpage

\input{chapters/chapter6/sections/3_experiments_corrections_table}

\newpage

\subsection{Dataset and pipeline}

As explained before, the usual approach to solve the image consistency problem is placing color references in a certain scene to later perform a color correction. There exists a widely spread usage of color charts, e.g. Macbeth ColorChecker of 24 colors \citep{McCamy1976}. Over the years, extensions of this ColorChecker have appeared, mostly presented by X-Rite, a Pantone company, or by Pantone itself, which introduced the Pantone Color Match Card \textregistered~ that features four AruCo patterns \citep{Garrido-Jurado2016} to ease the pattern extraction when acquiring the colors of the chart. 

Since in this chapter we do not propose improved versions of the charts themselves, we use an existing image dataset that contains images of the Macbeth ColorChecker of 24 colors in different scenes in order to evaluate our color correction with respect to image consistency; and benchmark it against other correction methods. The Gehler’s dataset is a widely used dataset with several versions, and there exists a deep discussion about how to use it. Despite the efforts of the dataset creators and other authors to promote the use of the last “developed” dataset \citep{Hemrit2018}, here we use the RAW original version of the dataset \citep{Gehler2008}, and we developed the images ourselves. We did so because we performed image augmentation over the dataset, as we want to control the developing process of the RAW images and also measuring the resulting augmented colors directly from the provided annotations in the original dataset (see~\autoref{fig:ccdatasetpipeline}). 

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/cc_dataset_pipeline.jpg}
    \caption{Our pipeline: for each Gehler’s dataset raw image (Bayer) we developed an RGB image, which are already the half size of the original image, also this image was down-sampled to reduce its size 4 times. Then we augmented this down-sampled image with 100 sample augmentation scenarios. For each augmented scenario we corrected it back before augmentation using the 21 different correction methods described in \autoref{tab:correctionssummary}. }
    \label{fig:ccdatasetpipeline}
\end{figure}


The Gehler’s dataset comprises images from two cameras: a Canon EOS 1DS (86 images) and a Canon EOS 5D (483 images), both cameras producing raw images of 12-bit per channel ($ \mathbb{N}^3_{[0, 4096]} $) with a RGGB Bayer pattern \citep{Bull2014}. This means we have twice as many green pixels than red or blue pixels. 

\newpage

Images have been processed using Python \citep{van1995python}, represented by \texttt{numpy arrays} \citep{van2011numpy,numpy}, and have been developed using \texttt{imageio} \citep{imageio} and \texttt{rawpy}, the Python wrapper of \texttt{craw} binary, the utility used elsewhere to process the Gehler’s dataset \citep{Gehler2008,Hemrit2018}. When developing the images, we implemented no interpolation, thus rendering images half the size of the raw image (see~\autoref{tab:ccdatasetdownsampling}). These are our \textit{ground-truth} images: the colors in these images are what we are trying to recover when performing the color corrections. 

We chose to work with 8-bits per channel RGB images as is the most commonly developed pixel format present nowadays. First, we cast the developed dataset 12-bit images ($ \mathbb{N}^3_{[0, 4096]} $) to 8-bit resolution ($ \mathbb{N}^3_{[0, 255]} $). The difference between the cast images and the groundtruth images is the quantization error, due to the loss of color depth resolution. 
To speeded up the calculations without losing statistical significance in the results we down-sampled the images by a factor 4. The down-sampling factor is arbitrary and depends on the level of redundancy of the color distribution in our samples. We selected a down-sampling factor that did not alter the color histogram of the images of the dataset, see \autoref{fig:ccdatasethistogram}. \autoref{tab:ccdatasetdownsampling} shows the final image sizes for each camera on the Gehler’s dataset. 

\begin{table*}[h]
    \renewcommand{\arraystretch}{1.6}
    \begin{tabular}{lrrr}
    \hline
    \textbf{Camera} & \textbf{Raw image}    & \textbf{Developed image} & \textbf{Down-sampled image} \\
    \hline
    Canon EOS 1DS   & \textnum{(4064,\ 2704)} & \textnum{(2041,\ 1359)}    & \textnum{(511,\ 340)}         \\
    Canon EOS 5D    & \textnum{(4368,\ 2912)} & \textnum{(2193,\ 1460)}    & \textnum{(549,\ 365)}        \\
    \hline \\
    \end{tabular}
\caption{Sizes in pixels (x, y) of the images along our pipeline. Notice raw pixels are natural pixels of the sensor, this means each pixel only represents one color (red, green or blue). }
\label{tab:ccdatasetdownsampling}
\end{table*}

Subsequently, we augmented the dataset using  \texttt{imgaug} \citep{imgaug} (see~\autoref{fig:ccdatasetaugmentation}) that generated image replicas simulating different acquisition setup conditions. The augmentations were performed with random augmentations that modeled: linear contrast, gamma contrast and channel cross-talk. Geometrical distortions were omitted because this work is focused on a colorimetry problem.

Finally, we corrected each developed, down-sampled and augmented image using the color corrections listed in \autoref{tab:correctionssummary}. These corrections were computed using color-normalized versions of those images ($ \mathbb{R}^{3}_{[0, 1]} $). White-balance corrections were implemented directly with simple array operations \citep{numpy}; while affine, polynomial and root-polynomial corrections were applied as implemented elsewhere \citep{colour-science}. We implemented our own version of the TPS with the corresponding RBFs, including support for smoothing, using a derivation of the \texttt{scipy} \citep{numpy}. 

% Our code can be found as a feature request in the  colour-science source code repository [].

\begin{figure}[h!t]
    \centering
    \hbox{
    \hspace{-0.15\textwidth}
    \includegraphics[width=1.15\textwidth]{chapters/chapter6/figures/cc_dataset_histogram.jpg}
    }
    \caption{An image from Gehler’s dataset (K=1) is down-sampled with 3 factors (K=4, 16, 64), where K is the down-sampling factor. The figure also shows the histogram associated with each image and the size in pixels of the image. Down-sampled images by a factor 4 maintain the histogram representation, but further down-sampling alters the color distribution.}
    \label{fig:ccdatasethistogram}
\end{figure}

\begin{figure}[h!t]
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/cc_dataset_augmetation.jpg}
    \caption{Different examples of color augmentation using \texttt{imgaug} in Python. The upper-left image is the developed original image from the Gehlre’s dataset. The other images are augmentations of these image with variations in color, contrast and saturation.  }
    \label{fig:ccdatasetaugmentation}
\end{figure}



\clearpage
\subsection{Benchmark metrics}

In order to benchmark the performance of all the correction methods, we implemented different metrics. First, a \textit{within-distance} ($ \overline{\Delta_{RGB}}_{,within} $) as the mean distance of all and only the colors in the ColorChecker to their expected corrected values \citep{Menesatti2012}:

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/whitin_distance_helper.jpg}
    \caption{The metric $ \overline{\Delta_{RGB}}_{,within} $ is represented. RGB colors of the ColorChecker of an image are projected in the \textit{red-green} plane. The colors are present as their ground-truth value ($ \circ $) and their altered copy ($ \times $). Dashed lines across the plane show the $ {\Delta_{RGB}}_{,within} $ between each color pair. Cyan, magenta and yellow pairs are highlighted above the other ColorChecker colors.}
    \label{fig:within_distance_helper}
\end{marginfigure}


\begin{equation}
  \overline{\Delta_{RGB}}_{,within} = \frac{\sum_{l=1}^{L} \Delta_{RGB} (\mathbf{s'}_l, \mathbf{c'}_l)  }{L}  
  \label{eq:within_distance}
\end{equation}

\noindent
where $ \mathbf{s'}_l $ is the corrected version of a certain ColorChecker captured color $ \mathbf{s}_l $, which has a ground-truth reference value of $ \mathbf{c'}_l $, and $ L $ is the number of reference colors in the ColorChecker (in our case $ L = 24 $). Alongside with this metric, a criterion was defined to detect \textit{failed corrections}. We consider failed corrections those which failed to \textit{reduce the within-distance} between the colors of the ColorChecker after the correction. Then, by comparing the $ \overline{\Delta_{RGB}}_{,within} $ of the corrected image and the image without correction (NONE):
    
\begin{equation}
\overline{\Delta_{RGB}}_{,within} - \overline{\Delta_{RGB}}_{,within,NONE} > 0 \ .
\label{eq:within_comparisom}
\end{equation}

Second, we defined a \textit{pairwise-distance set} ($ \mathbf{\Delta_{RGB}}_{,pairwise} $) as the set of the distances between all the colors in a ColorChecker in the same image: 

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/pairwise_distance_helper.jpg}
    \caption{The set $\mathbf{\Delta_{RGB}}_{,pairwise}$ is represented. RGB colors of the ColorChecker of an image are projected in the \textit{red-green} plane. The colors are present as their ground-truth value ($ \circ $). Dashed lines across the plane show the ${\Delta_{RGB}}_{,pairwise}$ between all the colors. The distances between cyan, magenta and yellow are highlighted above the other distances.}
    \label{fig:pairwise_distance_helper}
\end{marginfigure}

\begin{equation}
    \mathbf{\Delta_{RGB}}_{,pairwise} = \left\{  \Delta_{RGB} (\mathbf{c}'_l, \mathbf{c}'_m ) \ : \ l,m = 1, \dots, L   \right\}
    \label{eq:pairwise_distance}
\end{equation}

\noindent
where $ \mathbf{c'}_l $  and $ \mathbf{c'}_m $ are colors of the ColorChecker in a given image.  Also, we implemented another criterion to detect \textit{ill-conditioned corrections}. Ill-conditioned corrections are those failed corrections in which colors have also collapsed into extreme RGB values  (see~\autoref{fig:corner_case_tps0}). By using the \textit{minimum pairwise-distance} for a given color corrected image:

\begin{equation}
    min \left( \mathbf{\Delta_{RGB}}_{,pairwise} \right)  < \updelta \ ,
    \label{eq:pairwise_comparison}
\end{equation}

\noindent
where $ \updelta $ is a constant threshold which tends to zero. Note that somehow we were measuring here the opposite to the first criterion: we expected erroneous corrected colors to be pushed away from the original colors \autoref{eq:within_comparisom}. However, sometimes they also got shrunk into the borders of the RGB cube \autoref{eq:pairwise_comparison}, causing two or more colors to saturate into the same color. Also, notice that we did not define a \textit{mean pairwise-distance}, $\overline{\Delta_{RGB}}_{,pairwise}$, as it was useless to define a criterion around a variable which presented huge dispersion in ill-conditioned scenarios (e.g. colors pairs were at the same time close and far, grouped by clusters). 

\break \setlength{\parskip}{1em}

Third, we defined an \textit{inter-distance} ($ \overline{\Delta_{RGB}}_{,inter} $) as the color distance between all the other colors in the corrected images with respect to their values in the ground-truth images (measured as the mean RGB distance of all the colors in the image but subtracting first the ColorChecker area as proposed by Hemrit et al. \citep{Hemrit2018}):

\begin{marginfigure}
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter6/figures/inter_distance_helper.jpg}
    \caption{The metric $ \overline{\Delta_{RGB}}_{,inter} $ is represented. RGB colors of an entire image are projected in the \textit{red-green} plane. The colors are present as their ground-truth value (black points, $\circ$) and their altered copy (red points, $\times$). Then, three random colors are selected to show dashed lines across the plane to show the $ {\Delta_{RGB}}_{,inter} $ between each color pair.}
    \label{fig:inter_distance_helper}
\end{marginfigure}

\begin{equation}
  \overline{\Delta_{RGB}}_{,inter} = \frac{\sum_{m=1}^{M} \Delta_{RGB} (\mathbf{s'}_m, \mathbf{c'}_m)  }{M}  
  \label{eq:inter_distance}
\end{equation}

\noindent
where $ M $ is the total amount of pixels in the image other than those of the ColorChecker. This definition particularized the proposal of Menesatti et al., where in order to compute the $ \overline{\Delta_{RGB}}_{,inter} $, they used  all the colors of another color chart instead of the actual image. Specifically, Menesatti et al. used the GretagMacbeth ColorChecker SG \textregistered ~ with 140 color patches  \citep{Menesatti2012}.

Finally, to compare the computational performance of the methods, we measured the \textit{execution time} ($ \mathcal{T} $) to compute each corrected image, $ \mathcal{T} $ was also measured for images with different sizes to study its scaling with the amount of pixels in an image in all corrections \citep{Luo2014}.

\vspace{3cm}

\begin{figure*}[h]
    \centering
    \includegraphics[trim={0 0 0  2em},clip,width=0.9\textwidth]{chapters/chapter6/figures/corner_case_tps0.jpg}
    % \includegraphics[trim={6.75em 1em 6.75em 2.5em},clip,width=0.9\textwidth]{chapters/chapter6/figures/corner_case_tps0.jpg}
    % \includegraphics[trim={13.5em 1em 0 2.5em},clip,width=0.9\textwidth]{chapters/chapter6/figures/corner_case_tps0.jpg}
    
    \caption{An example of a failed and ill-conditioned correction. The altered image shows saturated colors: the yellowish colors and the whitish colors. The corrected image is computed with the TPS0 method rendering an erroneous result.}
    \label{fig:corner_case_tps0}
\end{figure*}


\newpage