\newpage
\section{Conclusions}
\label{sec:conclusions}
% Allargar
We have presented a method to pack a set of colors, useful for color calibration, in a QR Code in a fully back-compatible manner; this is, preserving its conventional ability to store digital data. By doing so, we enhanced the state-of-the-art color charts with two main features: first, we did leverage the computer vision  readout of the color references to the QR Code features; and two, \textit{de facto} we reduced the size of the color charts to the usual size of a QR Code, one or two inches. 

Also, we have demonstrated that the color capacity of the QR Codes constructed this way (up to a few thousand colors!) is orders of magnitude higher than that found in the traditional color charts, due to the image density and pattern recognition robustness of the QR Codes. 

Moreover, compared to other colored QR Codes, our proposal, based on the grayscale affinity of the colors to white or black, leads to much lower signal alteration levels and thus much higher readability, than the found in more naive approaches like, e.g., random assignment methods, which represent the aesthetic QR Codes (printing a logo). 

This work opens a way to explore further methods to embed color information in conventional 2D barcodes. Tuning how we defined our criteria of color embedding upon the affinity of colors to black and white would lead to more efficient embedding methods. We explored some of these ideas to seek those improved methods, and we expose them below.

First, the way in which we implemented the grayscale (a mean value of the RGB channels) is only one of the ways to compute a grayscale channel. For example, one could use the \textit{luma} definition a weighted mean based on the human eye vision:


$$f_{grasycale}(r, g, b) = 0.2126\cdot r + 0.7152\cdot g + 0.0722\cdot b \ ,$$ 

\noindent
or the \textit{lightness} one:

$$f_{grasycale}(r, g, b) = \frac{1}{2} \left( \max(r, g, b) + \min(r, g, b) \right) \ .$$

These grayscale definitions are often part of colorspaces definitions \citep{Hunt2005}, such as CIELab, CIELuv, HSL, etc.  All these different grayscales will generate different color distributions, displacing colors between black and white regions of the QR Code. 

\newpage

Second, we defined a way to select the area inside the QR Code to embed the colors (see~\autoref{algorithm}), that could also be improved. For example, we could decide to implement $ f_{threshold} $ in a more complex fashion. Let us imagine a certain set of colors $ G_{rgb} $ to encode in a certain QR Code. One could create more than two subsets to define the black-white affinity, i.e. four sets, namely: \textit{blackest} colors (0), \textit{blackish} colors (1), \textit{whitish} colors (2) and \textit{whitest} colors (3): 

\begin{equation*}
 f_{threshold}(G_{gray}) = 
 \begin{cases} 
     0 & 0.00 < G_{gray}(k) \leq 0.25  \\
     1 & 0.25 < G_{gray}(k) \leq 0.50   \\
     2 & 0.50 < G_{gray}(k) \leq 0.75   \\
     3 & 0.75 < G_{gray}(k) \leq 1.00  \\
 \end{cases}  
\end{equation*}

And accommodate \autoref{algorithm} to this new output from $ f_{grayscale} $ by assigning those colors with higher potential error (\textnum{1, \ 2}) to the DATA zone and those with lower potential error (\textnum{0, \ 3}) to the EC zone. Theoretically, this would outperform our current approach, as in this work we demonstrated that DATA zones are more resilient to error than EC zones, thus displacing away from EC critical colors would lead to a systematic increase of color capacity. 


Third, many authors have contributed to create aesthetic QR Codes which embed trademarks and images with incredibly detailed results. We wanted to highlight some solutions that might be combined with our technology to embed even further colors or to improve the control over the placement of the colors. 

\textit{Halftone QR Codes} were proposed by Chu et al. \citep{Chu2013}, they substituted QR Code modules for subsampled modules that contained white and black colors (see~\autoref{fig:qrswithlogos}). These submodules presented a dithering pattern that followed the encoded image shape. One could use the dithering idea to embed also colors inside subsampled pixels. 

\textit{QArt Codes} were introduced by Cox \citep{qartcodes}, the proposal aims to force the QR Code encoding to block certain areas of the QR Code to be black or white no matter what the encoded data is, note this is only possible for some kinds of data encoding (see~\autoref{fig:qrswithlogoscolor}). One could use this feature to preserve dedicated areas for color embeddings, as a complement to \autoref{algorithm}. Note that the need for a back-compatible criteria is still-present, since the QArt Code only provides us the certainty if a module of the original QR Code is black or white, but they \textit{must remain} black or white during the decoding process. This has a potential impact in reducing the cost of producing the Color QR Codes, because one could fix the position of the colored modules before encoding the data using our grayscale criteria, then print the QR Code using cost-effective printing technologies (rotogravure, flexography, etc.) and the black and white pixels using also cost-effective monochromatic printing technologies (laser printing), rather than using full digital ink-jet printing to print the whole code. 

Finally, we have assumed that our \textit{back-compatible Color QR Codes} are meant to be for colorimetric measurement, as this is the preamble of our research. 

Nevertheless, the above-presented results could be applied to encode data in the color of the QR Codes in a back-compatible manner. This means, including more digital information in the same space. Other authors have presented their approaches to this solution, none of them in a back-compatible way. Les us propose a couple of ideas to achieve these \textit{digital back-compatible Color QR Codes}, based on other ideas to color encode data in QR Codes.



First, Blasinki et al. \citep{Blasinski2013} introduced a way to multiplex 3 QR Codes in one, by encoding a QR Code in each CMY channel of the printed image. And then, when they recovered the QR Code, they applied a \textit{color interference cancellation algorithm} to extract the QR Codes from the RGB captured image, they also discussed how to approach the color reproduction problem and manipulated further the remaining black patterns (finder, alignment and timing) to include color references. 

All in all, this rendered non back-compatible Color QR Codes. Now, using our proposed method, one could turn this approach to back-compatibility again by simply doing the following: keeping the first QR Code to multiplex as the 'default' QR Code; then, taking another 3 additional QR Codes and creating a barcode following Blasinki et al. method; in turn, creating a "pseudo-white" and "pseudo-black" version of this color QR Code; and finally, re-encoding the default QR Code with the pseudo-colors in a back-compatible manner. Also, note that this proposal is not restricted by the number of channels an image has, as we are exploiting intermediate values, not only the extreme ones. There should exist a limit yet to discover of how many QR Code can be multiplexed in this fashion. 


Second, other authors like Berchtold et al. -- JAB Code (see~\autoref{fig:2dbarcodes}) \citep{Berchtold2020} or Grillo et al. -- HCCBC (see~\autoref{fig:othercolorqrs}) \citep{Grillo2010} fled from the original QR Code standard to redefine entirely the data encoding process. The main caveat of their technological proposals is the lack of back-compatibility, as we have discussed before. One could combine both technologies to create more adoptable technology. Grillo et al. proposal seems the easiest way to go, as they kept the factor form of QR Codes. Theoretically, one could simply multiplex one HCCBC with one QR Code as described with the previous method and achieve a digital back-compatible Color QR Code. 
