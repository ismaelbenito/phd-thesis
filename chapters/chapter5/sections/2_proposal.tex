
% \section{Theoretical foundations 5}
\section{Proposal}
\label{sec:theoreticalfoundations5}

Linking the colorimetric problem to a set of digital information opens the door to many potential uses related to automation. For example, the digital data could store a unique ID to identify the actual color calibration references used in the image, or other color-measurement properties e.g. by pointing at a data storage location. When used, for example, in smart packaging, this enables the identification of each package individually, gathering much more refined and granular information. 

In this chapter, we propose a solution for that, by placing altogether digital information and color references without breaking the QR Code standard in a back-compatible Color QR Code implementation for colorimetric applications. Our solution modifies the above-presented default QR Code encoding process (see~\autoref{fig:defaultqrflow}), to enhance the QR Code to embed colors in a back-compatible way (see~\autoref{fig:colorqrflow}). 

\begin{figure}[h!t]
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter5/figures/jpg/8.jpg}
    \caption{Block diagram for a back-compatible encoding-decoding process of a QR Code that features the embedding of a color layer for colorimetric applications. The process can be seen as a global encoding process (digital encode and color encode), followed by a channel (print and capture) and a global decoding process (extract colors and decode digital information). This process is back-compatible with state-of-the-art scanners, which remove colors and achieve the decoding of the data; and also compatible with new decoders, which can benefit from color interrogation. The back-compatibility is achieved by following certain rules in the color encoding process (i.e. use the same threshold when placing the colors than when removing them).}
    \label{fig:colorqrflow}
\end{figure}


This solution is inspired by, but not directly based on, previous Color QR Codes proposals that aimed at enhancing the data storage capacity of a QR Code by replacing black and white binary pixels by color ones (see~\autoref{fig:othercolorqrs}) \citep{Garateguy2014, Blasinski2013, Querini2014, VizcarraMelgar2012}. Those approaches offer non-back-compatible barcodes that cannot be decoded with standard readers. Instead, our work offers a design fully compatible with conventional QR Codes. Evidently, without a specialized reader, the color calibration process cannot be carried out either, but back-compatibility assures that any standard decoder will be able to extract the digital data to, e.g. point at the appropriate reader software to carry out the color correction in full. From the point of view of the usability, back-compatibility is key to enable a seamless deployment of this new approach to color calibration, using only the resources already available in smartphones (i.e. the camera and a standard QR decoder).

\begin{figure}[h!t]
     \centering
     \includegraphics[width=\textwidth]{chapters/chapter5/figures/jpg/2.jpg}
     \caption{Previous state-of-the-art QR Code variants that implement colors in some fashion. (a) A back-compatible QR Code which embeds an image (© 2014 IEEE) \citep{Garateguy2014}. (b) A RGB implementation of QR Codes where 3 different QR Codes are packed, one in each CMY channel. Each channel is back-compatible, although the resulting image is not (© 2013 IEEE) \citep{Blasinski2013}. (c) A High Capacity Color Barcode, a re-implementation of a QR Code standard using colors, which is not back-compatible with QR Codes (© 2010 IEEE) \citep{Grillo2010}.}
     \label{fig:othercolorqrs}
\end{figure}



% \subsection{The QR Code life-cycle}
% \label{ss:qrcodelifecycle}

% QR Codes (Quick-Response Codes) are 2D barcodes introduced in 1994 by Denso Wave \citep{iso18004_2015}, which aimed at replacing traditional 1D barcodes in the logistic processes of this company. However, the use of QR Codes has escalated in many ways and are now present in manifold industries: from manufacturing to marketing and publicity, becoming a part of the mainstream culture. In all these applications, QR Codes are either printed or displayed and later acquired by a reading device, which normally includes a digital camera or barcode scanner.

% The process of encoding and decoding a QR Code could be  considered as a form of communication through a visual channel: a certain message is created, then split into message blocks, these blocks are encoded alongside error correction blocks, and finally encoded in a 2D binary array. This 2D binary array is an image that is transmitted through a visual channel (printed, observed under different illuminations and environments, acquired as a digital image, located, resampled, etc.). On the decoder side, the binary data of the 2D binary array is retrieved, the binary stream is decoded, and finally the original message is obtained. \autoref{fig:colorqrflow} shows the block diagram for this process and also shows our back-compability proposal explained in the following subsections. 

% From the standpoint of a visual communication channel, many authors before explored the data transmission capabilities of the QR Codes, especially as steganographic message carriers (data is encoded in a QR Code, then encoded in an image) due to their robust error correction algorithm \citep{Al-Otum2018, Rosales-Roldan2018}.  

% \begin{marginfigure}
%     \centering
%     \includegraphics[width=\textwidth]{chapters/chapter5/figures/jpg/3.jpg}
%     \caption{Block diagram for a general encoding-decoding process of a QR Code which features the embedding of a color layer. This color layer could be used for a wide range of applications, such as placing a brand logo inside a QR Code. The process can be seen as a global encoding process (digital encode and color encode), followed by a channel (print and capture) and a global decoding process (remove colors and decode digital information). }
%     \label{fig:defaultqrflow}
% \end{marginfigure}




% \subsection{The QR Code encoding}
% \label{subsec:qrcodeencoding}
% When QR Codes are encoded, a huge amount of space is reserved for the error correction blocks (EC), much more than that for data blocks (D). This error correction feature, based on the Reed-Solomon error correction codes \citep{iso18004_2015}, is indirectly responsible for the popularity of QR Codes, since it  makes them extremely robust while allowing for a large amount of pixel tampering to accommodate aesthetic features like logos. During the generation of a QR Code, the level of error correction can be selected, from high to low capabilities: H (30 \%), Q (25\%), M (15\%) and L (7\%). This should be understood as the maximum number of error bits that a certain barcode can support (maximum Bit Error Ratio, see below).

% To compensate for the loss of data space, especially at higher error correction levels, the QR Code standard provides a way to increase the total data space by increasing the amount of pixels in the QR Code. This is the so-called version of a QR Code, that ranges from v1 (\(21 \times 21\) pixels) to v40 (\(177 \times 177\) pixels), by adding 4 rows and 4 columns each time the version increases \citep{iso18004_2015} (see more information in online Supplementary Materials section B).



\vspace{-0.5cm}
\subsection{Color as a source of noise}

Before being able to formulate our proposal, it is necessary to study how the additions of color affects the QR Code as carrier in our proposed communication framework (see~\autoref{fig:colorqrflow}). As QR Codes are equipped with error correction blocks, we can think of color as a source of noise to be corrected with the help of those correction blocks. Deliberate image modifications, like the insertion of a logo, or the inclusion of a color reference chart like we do here, can be regarded as additional noise to the channels. As such, the noise related to this tampering of pixels can be characterized with well-known metrics like the signal-to-noise ratio (SNR) and the bit error ratio (BER). 


Let’s exemplify this with a QR Code that encodes a website URL (see~\autoref{fig:qrwithlogo}.a.). First, this barcode is generated and resized (\autoref{fig:qrwithlogo}.b.) to fit a logo inside (\autoref{fig:qrwithlogo}.c.). The scanning process (\autoref{fig:colorqrflow}) follows a sequence of sampling --to detect the where QR Code is-- (\autoref{fig:qrwithlogo}.d.), desaturation --turning the color image into a grayscale image-- (\autoref{fig:qrwithlogo}.e.) and thresholding --to binarize the image-- (\autoref{fig:qrwithlogo}.f.). The original binary barcode (\autoref{fig:qrwithlogo}.a.) and the captured one (\autoref{fig:qrwithlogo}.f.) will be clearly different, and here is where the error correction plays a key role to retrieve the correct encoded message -the URL in this example-.

\begin{figure}[h!t]
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter5/figures/jpg/5.jpg}
    \caption{A QR Code is overlaid with a logo, which accumulates error due to the presence of the logo. (a) The QR Code is encoded. (b) The code is resized to accommodate the logo. (c) The logo is placed on top of the QR Code. (d) The code is “captured” and down-sampled again. (e) The sampled image is passed to grayscale. (f) The image is binarized, the apparent QR Code differs from the original QR Code (a). }
    \label{fig:qrwithlogo}
\end{figure}


We usually represent signal-to-noise ratio (SNR) from the point of view of signal processing. Thus SNR is the ratio between the ‘\emph{signal power}’ and the ‘\emph{noise power}’. Usually, as signals are evaluated over time, this ratio is presented as a root-mean-square average (RMS): 

\begin{equation}
\label{eq:snrdef}
\mathrm {SNR} ={\frac {P_{\mathrm {RMS, signal} }}{P_{\mathrm {RMS, noise} }}}
\end{equation}

\noindent
where $ P_{\mathrm {RMS, signal} } $ and $ P_{\mathrm {RMS, noise} } $ are the average power of the signal and the noise, respectively. Which in turn is equal to: 

\begin{equation}
\label{eq:snrdef}
\mathrm {SNR} =\left({\frac {A_{\mathrm {RMS, signal} }}{A_{\mathrm {RMS, noise} }}}\right)^{2}
\end{equation}

\noindent
where $ A_{\mathrm {RMS, signal} } $ and $ A_{\mathrm {RMS, noise} } $ are the root-mean-square (RMS) amplitude of the signal and the noise. The RMS of a discrete $ x $ variable can be written as:

\begin{equation}
    {\displaystyle x_{\text{RMS}}={\sqrt {{\frac {1}{n}}\left(x_{1}^{2}+x_{2}^{2}+\cdots +x_{n}^{2}\right)}}}
\end{equation}

Then, using this RMS expression and having into account grayscale images can be defined as two-dimensional discrete variables, we can rewrite SNR as follows: 

\begin{equation}
\label{eq:snrcalc}
\mathrm {SNR} = {\frac {\sum_0^n \sum_0^m (A_{gray}(i, j))^2}{\sum_0^n \sum_0^m (A_{gray}(i, j) - C_{gray}(i, j))^2}}  
\end{equation}

where $ A_{gray} \in [0,1]^{n\times m} $ are the pixels of the QR Code original image (\autoref{fig:snrberdefinition}.a), which act as a ‘\emph{signal image}’, $ C_{gray} \in [0,1]^{n\times m} $ are the pixels of the QR Code with the logo in a normalized grayscale (\autoref{fig:snrberdefinition}.b), the difference between both images acts as the ‘\emph{noise image}’ (\autoref{fig:snrberdefinition}.c), and the ratio between their variances is the SNR. Finally, the SNR values can be expressed in decibels using the standard definition:

\begin{equation}
  \mathrm {SNR_{dB}} = 10 \ \mathrm {log_{10}}(\mathrm {SNR}).  
\end{equation}


The bit error ratio (BER) is defined as the probability to receive an error when reading a set of  bits or, in other words, the mean probability to obtain a \0 when decoding a \1 and to obtain a \1 when decoding a \0:

\begin{equation}
\label{eq:berdef}
\mathrm {BER} = \frac{E(N)}{N}
\end{equation}

\noindent
where $ N $ is the total amount of bits received, and $ E(N) $ the errors counted in the $ N $ bits. In our case, this translates into the mean probability to obtain a black pixel when decoding a white pixel, or to obtain a white one when decoding a black one. A reformulated BER expression for our binary images is as follows:


\begin{equation}
\label{eq:bercalc}
\mathrm {BER} = \frac{\sum_0^n \sum_0^m | A_{bin}(i, j) - C_{bin}(i, j) |}{N}
\end{equation}

\noindent
where $ A_{bin} \in \{0,1\}^{n\times m} $ is the binarized version of $ A_{gray} \in [0,1]^{n\times m} $ (\autoref{fig:snrberdefinition}.d), $ C_{bin} \in \{0,1\}^{n\times m} $ is the binarized version of $ C_{gray} \in [0,1]^{n\times m} $ (\autoref{fig:snrberdefinition}.e) and $ N = n \cdot m $ are the total pixels in the image. The pixels contributing to the BER are shown in \autoref{fig:snrberdefinition}.f.

%% PAPER BER - SNR

% SNR is computed following \autoref{eq:snrcalc} and \autoref{eq:snrcalclog}:

% \begin{equation}
% \label{eq:snrcalc}
% \mathrm {SNR} = {\frac {\sum_0^n \sum_0^m (A_{gray}(i, j))^2}{\sum_0^n \sum_0^m (A_{gray}(i, j) - C_{gray}(i, j))^2}}  
% \end{equation}

% \begin{equation}
% \label{eq:snrcalclog}
%   \mathrm {SNR_{dB}} = 10 \ \mathrm {log_{10}}(\mathrm {SNR}).  
% \end{equation}

% where $ A_{gray} \in [0,1]^{n\times m} $ are the pixels of an original QR Code image, which act as a ‘\emph{signal image}’ (see~\autoref{fig:qrwithlogo}.a), $ C_{gray} \in [0,1]^{n\times m} $ are the pixels of the QR Code with the logo in a normalized grayscale (see~\autoref{fig:qrwithlogo}.e), the difference between both images acts as the ‘\emph{noise image}’, and the ratio between their variances is the SNR.

% BER is computed following \autoref{eq:bercalc}:

% \begin{equation}
% \label{eq:bercalc}
% \mathrm {BER} = \frac{\sum_0^n \sum_0^m | A_{bin}(i, j) - C_{bin}(i, j) |}{N}
% \end{equation}

% where $ A_{bin} \in \{0,1\}^{n\times m} $ are the binarized version of $ A_{gray} \in [0,1]^{n\times m} $ pixels, $ C_{bin} \in \{0,1\}^{n\times m} $ (see~\autoref{fig:qrwithlogo}.f) are the binarized version of $ C_{gray} \in [0,1]^{n\times m} $ and $ N = n \cdot m $ are the total pixels in the image. 


% In the following, the SNR and BER will be calculated on the basis of monochromatic images, because the original QR Code that only contains data is a monochromatic image. Thus, for the SNR computation, the grayscale image before tampering the pixels (see~\autoref{fig:snrberdefinition}.a.) is used as the ‘\emph{signal image}’ and the difference between this image and the one to be decoded (\autoref{fig:snrberdefinition}.b.) is used as the ‘\emph{noise image}’ (\autoref{fig:snrberdefinition}.b.). This leads us to the following SNR definition: 

% \begin{equation}
% \label{eq:snrdef}
% \mathrm {SNR} ={\frac {P_{\mathrm {RMS, signal} }}{P_{\mathrm {RMS, noise} }}}=\left({\frac {A_{\mathrm {RMS, signal} }}{A_{\mathrm {RMS, noise} }}}\right)^{2}
% \end{equation}

% where $ P_{\mathrm {RMS, signal} } $ and $ P_{\mathrm {RMS, noise} } $ are the average power of the signal image and the noise image; and $ A_{\mathrm {RMS, signal} } $ and $ A_{\mathrm {RMS, noise} } $ are the root mean square (RMS) amplitude of the signal image and the noise image. This way, the SNR can be calculated for our two images with the following discrete formula: 

% \begin{equation}
% \label{eq:snrcalc}
% \mathrm {SNR} = {\frac {\sum_0^n \sum_0^m (A_{gray}(i, j))^2}{\sum_0^n \sum_0^m (A_{gray}(i, j) - C_{gray}(i, j))^2}}  
% \end{equation}

% where $ A_{gray} \in [0,1]^{n\times m} $ are the pixels of the QR Code original image, which act as a ‘signal image’, $ C_{gray} \in [0,1]^{n\times m} $ are the pixels of the QR Code with the logo in a normalized grayscale, the difference between both images acts as the ‘noise image’, and the ratio between their variances is the SNR. Finally, the SNR values can be expressed in decibels using the standard definition $ \mathrm {SNR_{dB}} = 10 \ \mathrm {log_{10}}(\mathrm {SNR}) $.

% For BER computation, rather than using the grayscale images, we need to binarize them to assess how many errors our pixel tampering has led to. After desaturation, the conversion from grayscale to black and white pixels (\autoref{fig:snrberdefinition}.e) is carried out by means of a simple threshold placed in the middle of the grayscale value range. The BER is defined as the probability to receive an error when reading a set of  bits or, in other words, the mean probability to obtain a 0 when decoding a 1 and to obtain a 1 when decoding a 0. In our case, this translates into the mean probability to obtain a black pixel when decoding a white pixel, and to obtain a white one when decoding a black one (\autoref{fig:snrberdefinition}.f). Thus, BER can be written as:

% \begin{equation}
% \label{eq:berdef}
% \mathrm {BER} = \frac{E(N)}{N}
% \end{equation}

% where $ N $ is the total amount of bits received and $ E(N) $ the errors counted in the $ N $ bits; a reformulated for our binary images as:

% \begin{equation}
% \label{eq:bercalc}
% \mathrm {BER} = \frac{\sum_0^n \sum_0^m | A_{bin}(i, j) - C_{bin}(i, j) |}{N}
% \end{equation}

% where $ A_{bin} \in \{0,1\}^{n\times m} $ are the binarized version of $ A_{gray} \in [0,1]^{n\times m} $ pixels, $ C_{bin} \in \{0,1\}^{n\times m} $ are the binarized version of $ C_{gray} \in [0,1]^{n\times m} $ and $ N = n \cdot m $ are the total pixels in the image. 

\begin{figure}[h!t]
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter5/figures/jpg/6.jpg}
    \caption{A QR Code with a logo is created and read, which accumulates error due to the presence of the logo. (a) The original QR Code encoded. (b) The captured sampled grayscale QR Code. (c) The power difference between (a) and (b). (d) The original grayscale QR Code encoded is binarized, which it is represented exactly as (a). (e) The captured sampled grayscale image from (b) is binarized. (f) The difference between (d) and (e) is shown: light blue pixels correspond to white pixels turned into black by the logo, and dark blue pixels correspond to black pixels turned into white by the logo.}
    \label{fig:snrberdefinition}
\end{figure}

\newpage
As an example of these calculations, \autoref{tab:snrber} shows the results for the computation of the SNR and BER figures for ~\autoref{fig:qrwithlogo} images. As we can see, adding a logo to the pattern represents a noise source that reduces the SNR to 10.53 dB, further noise sources (printing, capture, etc.) will add more noise thus reducing the SNR even more. BER metric shows us the impact of the logo when recovering the digital bits. As we have mentioned before this quantity is directly related to the error correction level needed to encode the QR Code. In this example, with a BER of 8.54\%, the poorest error correction level (L, 7\%) would not suffice to ensure safe readout of the barcode. 

\begin{table}[h!t]
    \centering
    % \setlength{\tabcolsep}{1ex}
    % \renewcommand{\arraystretch}{1.5}
    % \begin{footnotesize}
    \begin{tabular}{l@{\hspace{8ex}}r@{\hspace{5ex}}r}
    \hline
    Measure               & Acronym              & Value                \\
    \midrule[\heavyrulewidth]
    Signal-to-Noise ratio & SNR                  & \textnum{10.53 \ dB}             \\
    Bit error ratio       & BER                  & \textnum{8.54} \%              \\
    \hline
    \end{tabular}
    % \end{footnotesize}
    \caption{The values for the SNR and BER computed for the QR Code with a logo from \autoref{fig:qrwithlogo}. The SNR is computed using grayscale images. The BER is computed using binary images (see~\autoref{fig:qrwithlogo}). }
    \label{tab:snrber}
\end{table}


\subsection{Back-compatibility proposal}

We want to achieve back-compatibility with the QR Code standard. This means that we must still be able to recover the encoded data message from the colored QR Code using a standard readout process (capturing, sampling, desaturating and thresholding).

To make it possible we must place these colors inside the barcode avoiding the protected key areas that ensure its readability. In the rest of the available positions, the substitution of black and white pixels with colors can be regarded as a source of noise added to the digital data pattern. We propose here a method to reduce the total amount of noise and miss-classifications introduced in the QR Code when encoding colors, that is based on the affinity of those colors to black and white (i.e. to which color it resembles the most). To that end, we classify the colors of the palette to be embedded in two groups: \emph{pseudo-black} and \emph{pseudo-white} colors. 

Initially, let $ G'_{rgb} \in [0,255]^{l \times 3} $ be a set of colors with size $ l $ we want to embed in a QR Code. Then, let us start with the definition of the main steps of our proposal to encode these colors inside a QR Code:

\begin{enumerate}
    \item \textbf{Normalization,} the 8-bit color channels (RGB) are mapped to a normalized color representation: 
    
        \begin{equation}
        \label{eq:normalizationdef}
        f_{normalize}: [0,255]^{l \times 3} \to [0,1]^{l \times 3} 
        \end{equation}

    \item \textbf{Desaturation,} the color channels (RGB) are then mapped into a monochromatic grayscale channel (L): 

        \begin{equation}
        \label{eq:desaturationdef}
        f_{grayscale}: [0,1]^{l \times 3} \to [0,1]^{l}
        \end{equation}

    \item \textbf{Binarization,} the monochromatic grayscale channel (L) is converted to a monochromatic binary channel (B): 

        \begin{equation}
        \label{eq:binarizationdef}
         f_{threshold}: [0,1]^{l} \rightarrow \{0,1\}^{l} 
        \end{equation}

    \item \textbf{Colorization,} the binary values of the palette colors represent the affinity to black (zero) and white (one) and can be used to create a mapping between the position in the color palette list and the position inside the QR Code matrix (a binary image). This mapping will also depend on the geometry of the QR Code matrix (where are the black and the white pixels placed) and an additional matrix that protects the key zones of the QR Code (a mask that defines the key zones):
    
        \begin{equation}
        \label{eq:mappingdef}
        f_{mapping} : \{0,1\}^{l} \times \{0,1\}^{n\times m} \times \{0, 1\}^{n \times m} \to  \{0, \dotsc, l+1\}^{n \times m} 
        \end{equation}
        
    Once the mapping is computed, a function is defined to finally colorize the QR Code, which renders an RGB image of the QR Code with embedded colors: 
    
        \begin{equation}
        \label{eq:colorizationdef}
        f_{colorize} : \{0,1\}^{n\times m} \times [0,1]^{l \times 3} \times \{0, \dotsc, l+1\}^{n \times m} \to  [0,1]^{n\times m \times 3} 
        \end{equation}

\end{enumerate}


Subsequently, to create the pseudo-black and pseudo-white colors subsets, we must define the implementation of these functions. These definitions are arbitrary, i.e. it is possible to compute a grayscale version of a color image in different ways. Our proposed implementation is intended to resemble the QR Code readout process: 

\begin{enumerate}
    \item \textbf{Normalization,} $ f_{normalize} $ will be a function that transforms a 24-bit color image (RGB) to a normalized color representation. We used a linear rescaling factor for this:
    
        \begin{equation}
        \label{eq:normalizationimpl}
         G_{rgb} (k, c) = f_{normalize} (G'_{rgb}) = \frac{1}{255} G'_{rgb} (k, c)
        \end{equation}
    
    where $ G'_{rgb} \in [0,255]^{l \times 3} $ is a list of colors with a 24-bit RGB color depth and $ G_{rgb} \in [0,1]^{l \times 3} $ is the normalized RGB version of these colors. 

    \item \textbf{Desaturation,} $ f_{grayscale} $ will be a function that transforms the color channels (RGB) to a monochromatic grayscale channel. We used an arithmetic average of the RGB pixel channels:

        \begin{equation}
        \label{eq:desaturationimpl}
         G_{gray}(k) = f_{grayscale}(G_{rgb}) =  \frac{1}{3} \sum_{c=0}^3 G_{rgb}(k,c)
        \end{equation}
        
    where $ G_{rgb} \in [0,1]^{l \times 3} $  is the normalized RGB color palette and $ G_{gray} \in [0,1]^{l} $ is the grayscale version of this color palette.

    \item \textbf{Binarization,} $ f_{threshold} $ will be a function that converts the monochromatic grayscale channel (L) to a binary channel (B). We used a simple threshold function with a thresholding value of \textnum{0.5}: 

        \begin{equation}
        \label{eq:binarizationimpl}
         G_{bin} (k) = f_{threshold}(G_{gray}) = 
         \begin{cases} 
             0 & G_{gray}(k) \leq 0.5  \\
             1 & G_{gray}(k) > 0.5   \\
         \end{cases}  
        \end{equation}
    
    where $ G_{gray} \in [0,1]^{l}  $ is the grayscale version of the color palette and $  G_{bin} \in \{0,1\}^{l} $ is its binary version, which describes the affinity to black (\0) and white (\1) colors. 

    \item \textbf{Colorization,} $f_{color}$ will be a function that will render a RGB image from the QR Code binary image and the palette colors by using a certain mapping. We used this function to implement it:
    
    % \begin{fullwidth}
    \begin{equation}
    \label{eq:colorizationimpl}
    \begin{split}
     C_{rgb} (i, j,k) = f_{color} (A_{bin}, G_{rgb}, M) = \begin{cases} 
     A_{bin}(i,j) & M(i,j) = 0 \\
     G_{rgb}(p-1,k) & M(i,j) = p > 0 \\
     \end{cases} 
     \end{split}
    \end{equation}
    % \end{fullwidth}
    
    where $ A_{bin} \in \{0,1\}^{n\times m} $ is the original QR Code binary image, $ G_{rgb} \in [0,1]^{l \times 3} $ is a color palette to be embedded in the image, $ C_{rgb} \in [0,1]^{n\times m \times 3} $ is the colorized QR Code image, and $ M \in \{0, \dotsc, t\}^{n \times m} $ is an array mapping containing the destination of each one of the colors of the palette into the 2D positions within the image. We propose to use $ G_{bin} $ (\autoref{eq:binarizationimpl}) to create $ M $. This mapping will also depend on the geometry of the QR Code image (where are the black and the white pixels placed) and an additional matrix that protects the key zones of the QR Code (a mask which defines the key zones), this mapping will be $ f_{mapping} $, it has the general form:  
    
    \begin{equation}
    \label{eq:mappingimpl}
      M = f_{mapping} (G_{bin}, A_{bin}, Z) 
    \end{equation}
    
    where $ M \in \{0, \dotsc, t\}^{n \times m} $ is the array mapping, $  G_{bin} \in \{0,1\}^{l} $ is the affinity to black or white of each color in the palette, $ A_{bin} \in \{0,1\}^{n\times m} $ is the original QR Code binary image and $ Z \in \{ 0, 1 \}^{n \times m} $ is a mask that protects the QR Code key patterns to be overwritten by the palette. One possible implementation of $ f_{mapping} $ (\autoref{eq:mappingimpl}) is shown in \autoref{algorithm}, where the colors of the palette are mapped to positions of the QR Code based on their affinity to black and white. For each one of these two classes, the particular assignment of a color to one of the many possible pixels of the class (either black or white) is fully arbitrary and allows for further design decisions. In this implementation of the mapping, we choose to assign the colors in random positions within the class. In other applications, interested e.g. in preserving a certain color order, additional mapping criteria can be used as shown below. Anyhow, preserving the assignment to the black or white classes based on the color affinity is key for back-compatibility.

\end{enumerate}


\begin{algorithm}[p]
	\DontPrintSemicolon

	\KwInput{\(G_{bin} \in \{0, 1\}^{l}\), \(A_{bin}\in \{0, 1\}^{n \times m}\), and \(Z \in \{0, 1\}^{n \times m}\)}
	\KwOutput{\(M \in \{0, \dotsc, l+1\}^{n \times m}\)}

	\(W_{color} \assign []\)\\
	\(B_{color} \assign []\)\\
	\For{\(k = 0, \dotsc, l\)}{
		\If{\(G_{bin}(k) == 1\)}{
			Append \(k\) to \(W_{color}\)
		}
		\Else{
			Append \(k\) to \(B_{color}\)
		}
	}
	\(p \assign lenght(W_{color})\)\\
	\(q \assign lenght(B_{color})\)\\

	\(W_{pos} \assign []\)\\
	\(B_{pos} \assign []\)\\
	\For{\(i = 0, \dotsc, n\)}{
		\For{\(j = 0, \dotsc, m\)}{
			\If{\(Z(i, j) == 1\)}{
				\If{\(A_{bin}(i, j) == 1\)}{
					Append \((i, j)\) to \(W_{pos}\)
				}
				\Else{
					Append \((i, j)\) to \(B_{pos}\)
				}
			}
		}
	}

	\(W'_{pos} \assign\) Select \(p\) random values of \(W_{pos}\)\\
	\(B'_{pos} \assign\) Select \(q\) random values of \(B_{pos}\)\\

	\(M \assign \{0\}_{i, j}\ \ \forall i \in \{0, \dotsc, n\}\) and \(j \in \{0, \dotsc, m\}\)\\
	\For{\(k = 0, \dotsc, p\)}{
		\(M(W'_{pos}(k)) \assign W_{color}(k) + 1\)
	}

	\For{\(k = 0, \dotsc, q\)}{
		\(M(B'_{pos}(k)) \assign B_{color}(k) + 1\)
	}

	\Return{\(M\)}

	\caption{Creation of the mask for grayscale insertion method}
	\label{algorithm}
\end{algorithm}



\newpage

\begin{figure*}[h!t]
    \centering
    \includegraphics[width=.75\textwidth]{chapters/chapter5/figures/jpg/7.jpg}
    \caption{The color information from the ColorSensing logo is distributed using different criteria, each one of these distributions compute different measures of SNR and BER, although the total amount of colors is the same, the way they are distributed affects the signal quality. (a) The original QR Code with the logo. (b) The logo colors are sorted at the top of the QR Code. (c) The logo colors are randomly distributed among the QR Code. (d) The logo colors are distributed by using a threshold criterion among blacks and white colors.}
    \label{fig:snrbercomparisson}
\end{figure*}

Moreover, to illustrate how different placement mappings affect the readout process, we will consider 4 different situations, where $ f_{mapping} $ plays different roles, and we will compute their SNR and BER metrics: 

\begin{itemize}
    \item \textbf{Logo.} When a logo-like pattern is encoded, $ G_{rgb} $ will be the colors of the logo and $ M_{logo} $ will be a mapping that preserves the logo image, overlaid on top of the original QR Code image (\autoref{fig:snrbercomparisson}.a.).

    \item \textbf{Sorted.} We are going to use the colors of the logo (thus $ G_{rgb} $ will be the same as before), but we are going to place them on top of the QR Code, sorting them as they appear in the color list. $ M_{sorted} $ will establish that the first color goes to the first available position inside $ A_{gray} $ pixels, etc. (\autoref{fig:snrbercomparisson}.b.). 
    
    \item \textbf{Random.} Again we use the same colors of the logo ($ G_{rgb} $ remains the same) but now $ M_{random} $ defines a random mapping of the palette into the available positions of $ A_{gray} $ (\autoref{fig:snrbercomparisson}.c.). 
    
    \item \textbf{Grayscale.} Our proposed method. Same as before, but the now random assignment of $ M_{gray} $ respects the rule that pseudo-white colors are only assigned to white pixels of $ A_{gray} $, and pseudo-black only to the black ones, as described in \autoref{algorithm} (\autoref{fig:snrbercomparisson}.d.). 

\end{itemize}


\begin{table}[h!t]
    \centering
    % \setlength{\tabcolsep}{1.5ex}
    % \renewcommand{\arraystretch}{1.33}
    % \begin{footnotesize}
    \begin{tabular}{rrrrr}
        \hline
        Measure              & Logo                 & Sorted               & Random               & Grayscale            \\
        \midrule[\heavyrulewidth]
        SNR                  & \textnum{10.53 \ dB}             & \textnum{10.27 \ dB}             & \textnum{10.35  \ dB}             & \textnum{12.23 \ dB}             \\
        BER                  & \textnum{8.55} \%              & \textnum{8.33} \%              & \textnum{8.62} \%              & \textnum{0.00} \%              \\
        \hline
    \end{tabular}
    % \end{footnotesize}
    \caption{Values of SNR and BER computed for each criteria in \autoref{fig:snrbercomparisson}. Using the logo as it is, the sorted criteria and random criteria yield to similar results. However, the use of a simple grayscale threshold criteria slightly increases the SNR and hugely depletes the BER, showing a good result for encoding colors in a back-compatible way.}
    \label{tab:snrberlogocolors}
\end{table}

\vspace{1em}
Finally, \autoref{tab:snrberlogocolors} shows the SNR and BER figures for the four mappings (exemplified in the images of \autoref{fig:snrbercomparisson}). Using the grayscale approach to encode colors by their resemblance to black and white colors leads to much lower noise levels. Since the original data of the QR Code can be seen as a random distribution of white and black pixels, $ M_{sorted} $ and $ M_{random} $ mappings yield similar results to $ M_{logo} $, encoding the logo itself. Meanwhile, $ M_{gray} $ mapping shows us a \0\% BER, and an almost \textnum{2dB} SNR increase. This suggests that our proposal can be an effective way to embed colors into QR Code in a back-compatible manner (see \autoref{fig:colorqrflow}), as it is demonstrated in the following sections. 





% \begin{figure}[h!t]
%     \centering
%     \includegraphics[width=\textwidth]{chapters/chapter5/figures/jpg/8.jpg}
%     \caption{Block diagram for a back-compatible encoding-decoding process of a QR Code which features the embedding of a color layer for colorimetric applications. The process can be seen as a global encoding process (digital encode and color encode), followed by a channel (print and capture) and a global decoding process (extract colors and decode digital information). This process is back-compatible with state of the art scanners which remove colors and achieve the decoding of the data and compatible with new decoders which can benefit from color interrogation. The back-compatibility is achieved by following certain rules in the color encoding process (i.e. use the same threshold when placing the colors than when removing them).}
%     \label{fig:colorqrflow}
% \end{figure}


\newpage