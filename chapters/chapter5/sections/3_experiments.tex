\section{Experimental details}
\label{sec:experimentaldetails5}

Experiments were designed to test our proposed method,  we carried out 3 different experiments where QR Codes were filled with colors and then transmitted through different channels. In all experiments, we calculated the SNR and BER as a measure of the signal quality of each QR Code once transmitted through different channels. Also, we checked the direct readability by using a QR Code scanner before and after going through the channels. \autoref{tab:experiments} contains a summary of each experiment designed. A detailed explanation of the experimental variables is provided below. 

\begin{table*}[h!t]
    \centering
    % \setlength{\tabcolsep}{2ex}
    % \renewcommand{\arraystretch}{1.5}
    % \begin{footnotesize}
    \begin{tabular}{lp{0.4\textwidth}l}
        \hline
        \textbf{All experiments} & Values                                        & Size     \\
        \midrule[\heavyrulewidth]
        Color substitution (\%)  & 1, 5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 100 & 12       \\
        Colorized zone           & EC, D, EC\&D                                  & 3        \\
        Colorizing method        & Random, Grayscale                             & 2        \\
        \hline
        \textbf{Experiment 1}    & Values                                        & Size     \\
        \midrule[\heavyrulewidth]
        Digital IDs              & from 000 to 999                               & 1000     \\
        QR version               & 5, 6, 7, 8, 9                                 & 5        \\
        Channels                 & Empty, Image augmentation                     & 1 + 1    \\
        \hline
        \textbf{Experiment 2}    & Values                                        & Size     \\
        \midrule[\heavyrulewidth]
        Digital IDs              & 000                                           & 1        \\
        QR version               & 5, 6, 7, 8, 9                                 & 5        \\
        Channels                 & Empty,\\ Image augmentation                   & 1 + 1000 \\
        \hline
        \textbf{Experiment 3}    & Values                                        & Size     \\
        \midrule[\heavyrulewidth]
        Digital IDs              & 000                                           & 1        \\
        QR version               & 5                                             & 1        \\
        Channels                 & Empty, Colorimetry setup                      & 1 + 25   \\
        \hline \\
    \end{tabular}
    % \end{footnotesize}
    \caption{Summary of parameter values for each experiment designed. All experiments share common parameters, at least each experiment has 72 different QR Codes that will be generated using as reference the multiplication of the shared parameters. Experiment 1 generates 360.000 different QR Codes. }
    \label{tab:experiments}
\end{table*}


\subsection{Color generation and substitution}

We choose our random color palette $ G_{rgb} $ for the experiments to be representative of the  RGB space. Nevertheless,  $ G_{rgb} $ should be random in a way that it is uniformly random in the grayscale space L. But if we define three uniform random RGB channels as our generator, we will fail to accomplish a grayscale uniform random channel. This is due to the fact that when computing the L space as a mean of the RGB channels, we are creating a so-called Irwin-Hall uniform sum distribution \citep{Trenkler1996} (see~\autoref{fig:colorgenarots}.b.). In order to avoid this, we propose to first generate the L channel as a uniform random variable, then generate RGB channels which will produce these generated L channel values (see~\autoref{fig:colorgenarots}.b.). 

\newpage

\begin{figure}[h!t]
    \centering
    \includegraphics[width=0.85\textwidth]{chapters/chapter5/figures/jpg/10.jpg}
    \caption{Histogram comparison between uniform randomly generated RGB channels: (a) that yields to a non-uniform grayscale L and uniform randomly generated grayscale L, (b) with derived pseudo-uniform RGB channels. }
    \label{fig:colorgenarots}
\end{figure}

During the different experiments, we will be filling QR Codes with a palette of random colors  $ G_{rgb} $. The color substitution factor ranged from only 1\% of the available pixel positions in a QR Code replaced with colors up to 100\% (see~\autoref{fig:colorplacement}). Evidently, each QR Code version offers different numbers of pixels and thus positions available for color substitution.  


\begin{figure}[h!t]
    \centering
    \includegraphics[trim={0 2px 0 0},clip,width=0.90\textwidth]{chapters/chapter5/figures/jpg/9.jpg}
    \caption{The same QR Code is populated with different amounts of colors. (a) 1\% of the pixels are substituted using a random placement method (yellow arrows show the colorized pixels). (b) 100\% of the pixels are substituted using a random placement method. }
    \label{fig:colorplacement}
\end{figure}


\subsection{Placing colors inside the QR Code}
\label{subsec:placingcolors}

Our back-compatibility proposal starts avoiding the substitution of colors in key protected areas of the QR Code. This can be implemented with a $ Z $ mask (see~\autoref{algorithm}). In our experiments, we used 3 masks (see~\autoref{fig:coloredareas}): 

\begin{enumerate}[I]
    \item $ Z_{EC\&D} $, that excludes only the key protected areas and allows covering with colors all the error correction and data regions (see details in \autoref{ch:3}),
    \item $ Z_{EC} $, that only allows embedding colors in the error correction,
    \item $ Z_D $, with colors only in the data.
\end{enumerate}

Once we have restricted ourselves to these $ Z $ masks, we will embed the colors following a $ M $ mapping. We propose to use $ M_{random} $ and $ M_{gray} $  presented before (see \autoref{fig:snrbercomparisson}.c. and \autoref{fig:snrbercomparisson}.d.). 


\begin{figure}[h!t]
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter5/figures/jpg/11.jpg}
    \caption{The same QR Code is populated  in different areas with 80\% of colors for each area. (a) the whole QR Code is populated (EC\&D). (b) Only the error correction area is populated (EC). (c) Only the data area is populated.}
    \label{fig:coloredareas}
\end{figure}


\subsection{QR Code versions and digital IDs}
\label{subsec:qrcodeversionsandids}

The encoded data and the version of the QR Code will shape the actual geometry of the barcode, thus it will determine the $ A_{gray} $ pixels. To generate the barcodes, we choose as payload data a URL with a unique identifier such as \url{https://color-sensing.com/#000}, where the numbers after ‘\texttt{\#}’ range from \texttt{000} to \texttt{999} to make the barcodes different from each other. Also, the QR Code selected versions ranged from 5 to 9, to test and exemplify the most relevant computer vision pattern variations defined in the QR Code standard. For all of these barcodes, we used the highest level of error correction of QR Code standard: the H level, which provides a 30\% of error correction. 

% \begin{figure*}[h!t]
%     \centering
%     \includegraphics[width=.9\textwidth]{figures/jpg/12.jpg}
%     \caption{Comparison between QR Code versions, from left to right versions 5, 6, 7, 8 and 9, which each QR Code contains the same data. Each QR Code adds an additional 4 rows and 4 columns to the original matrix, from $37 \times 37$ pixels to $53 \times 53$ pixels.}
%     \label{fig:qrversions}
% \end{figure*}

\subsection{Channels}
\label{subsec:channels}

The use of QR Codes in real world conditions imply additional sources of error, like differences in printing, different placements, ambient light effects, effects of the camera and data processing, etc. All these factors can be regarded as sources of noise in a transmission channel.

% :

% \begin{equation}
%     \label{eq:noise}
%     R_{rgb} (i,j,k) = C_{rgb}(i,j,k) + N_{rgb}(i,j,k)
% \end{equation}

% where $ R_{rgb} \in [0, 1]^{n \times m} $ are the pixels of the received image when sampled, $ C_{rgb} \in [0, 1]^{n \times m} $ are the pixels of the colorized barcode and $ N_{rgb} \in [0, 1]^{n \times m}$ represents the distortion of the channel process (printing, environment, capturing, …) for each pixel of the sampled image. 

We considered 3 different channels for the experiments:


\begin{itemize}
    \item \textbf{Empty.}  A channel where there is no color alteration due to the channel. It was used as a reference, to measure the noise level induced by the colorization process (see \autoref{fig:channels}.a).

    \item \textbf{Image augmentation.} With a data augmentation library \citep{Jung2018} we generated images that mimic different printing processes and exposure to different light conditions. With this tool we also applied Gaussian blur distortions, crosstalk interferences between the RGB channels and changed contrast conditions. (see~\autoref{fig:channels}.b). 
    
    \item \textbf{Physical printouts exposed to controlled illumination.} We actually printed the QR Codes and captured them with a fixed camera (Raspberry Pi 3 with a Raspberry Pi Camera v2) \citep{Pagnutti2017} under different illumination-controlled conditions (Phillips Hue Light strip) \citep{Cusano2016}. The camera was configured to take consistent images. The light strip was configured to change its illumination conditions with two subsets of lights: white light (9 color temperatures from 2500K to 6500K) and colored light (15 different colors sampling evenly the CIExyY space) (see \autoref{fig:channels}.c).

\end{itemize}


\begin{figure*}[h!t]
    \centering
    \includegraphics[width=.85\textwidth]{chapters/chapter5/figures/jpg/13.jpg}
    \caption{The same QR Code with the same amount of colors (80\% of the data area) is exposed to different channels. (a) The image passed-through an empty channel. (b) The image passed-through an augmentation channel that resembles a warm light scene. (c) The image passed-through a real environment channel, actually printed and captured in a scene with a lamp at 2500K (warm light). }
    \label{fig:channels}
\end{figure*}

\newpage