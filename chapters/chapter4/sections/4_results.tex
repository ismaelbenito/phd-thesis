\section{Results}
\label{sec:results}

\subsection{Qualitative surface fitting}

We fitted the four transformations (AFF, PRO, CYL and TPS)  to the surface underneath all the  QR Code samples from the three datasets (SYNT, FLAT and SURF). To evaluate visually how accurate each transformation was, a squared lattice of equally spaced points on the predicted QR Code surface was back-projected into the original image space. For illustration purposes, results on representative samples of the SYNT, FLAT and SURF datasets can be seen in \autoref{fig:resultsSYNT}, \autoref{fig:resultsFLAT} and \autoref{fig:resultsSURF}, respectively. 

Our first dataset, SYNT, contained samples with affine (\autoref{fig:resultsSYNT}.a) and projective (\autoref{fig:resultsSYNT}.b) deformations. We observed that all four transformations achieved good qualitative fittings with images presenting affine deformations. This is an expected result, since all transformations implement affirm terms. Consequently, when it comes to projective deformations, the AFF transformation failed to adjust the fourth corner (the one without any finder pattern, see~\autoref{fig:qrcodeparts}.a), as expected. Comparatively, the PRO and the CYL transformations lead to similarly good results, since both can accommodate perspective effects. Finally, TPS fitted the surface well, specially inside the QR Code, and a slight non-linear deformation was present outside the boundaries of the barcode, but these are irrelevant for QR Code decoding purposes.

\begin{figure*}[h!b]
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter4/figures/jpg/6.jpg}
    \caption{Two examples (a), (b) from the SYNT dataset. The surfaces were fitted by the four methods described (AFF, PRO, CYL and TPS). The surface fitting is shown as a lattice of red points back-projected into the original image.}
    \label{fig:resultsSYNT}
\end{figure*}

\newpage

The FLAT dataset involved QR Codes that were actually printed and then imaged with a smartphone camera. These QR Codes were captured in projective deformations (\autoref{fig:resultsFLAT}.b), some of them resembling affine deformations (\autoref{fig:resultsFLAT}.a), and most of them just a combination of both. Qualitative performance comparison is similar to that of the SYNT dataset. Again, the AFF transformation failed to correctly approach the fourth corner. Also, we confirmed that PRO, CYL and TPS performed well for the FLAT images, but TPS showed a non-linear, irrelevant, overcorrection outside the barcode. 

\begin{figure*}[h!b]
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter4/figures/jpg/7.jpg}
    \caption{Two examples (a), (b) from the FLAT dataset. The surfaces were fitted by the four methods described (AFF, PRO, CYL and TPS). The surface fitting is shown as a lattice of red points back-projected into the original image.}
    \label{fig:resultsFLAT}
\end{figure*}

\break

The SURF dataset was the most challenging dataset in terms of modeling adverse surface topographies. QR Codes here were imaged again with a smartphone, but in this case the surface under the barcode was distorted in several ways: randomly deformed by hand (\autoref{fig:resultsSURF}.a), placed on top of a small bottle (\autoref{fig:resultsSURF}.b), a large bottle (\autoref{fig:resultsSURF}.c), etc. Results showed that AFF, PRO and CYL methods were not able to correctly match a random surface (i.e. deformed by hand), as expected. Instead, TPS worked well in these conditions, being a great example of the power of the spline decomposition to match slow varying topographies, if a sufficiently high number of landmarks is available. For cylindrical deformations (i.e. QR Codes in bottles), AFF and PRO methods were again unsuccessful. CYL performed better with the small bottles than with the large ones. Apparently, higher curvatures (i.e. lower bottle radius $ r $) facilitate the fitting of this parameter and improve the quality of the overall prediction radius of the projected cylinder before fitting the surface. Thus, the CYL method properly fits the cylinder radius from one of the sides of the QR Codes with 2 finder patterns and often fails to fit the opposite side. Interestingly, The TPS method performed opposite to the CYL method in the cylindrical deformations, tackling better surfaces with mild curvatures.

% \newpage
\begin{figure*}[h!t]
    \centering
    \includegraphics[width=\textwidth]{chapters/chapter4/figures/jpg/8.jpg}
    \caption{Three examples (a), (b), (c) from  the SURF dataset. The surfaces were fitted by the four methods described (AFF, PRO, CYL and TPS). The surface fitting is shown as a lattice of red points back-projected into the original image.}
    \label{fig:resultsSURF}
\end{figure*}

\newpage

\subsection{Quantitative data readability}


In order to evaluate the impact of these surface prediction capabilities on the actual reading of the QR Code data, we run the full decoding pipeline mentioned in \autoref{fig:pipeline} for all the images in the three datasets (SYNT, FLAT and SURF) with the four transformations (AFF, PRO, CYL and TPS). There, once surface deformation was corrected, the QR Code data was extracted with one of the most widespread barcode decoder (ZBar \citep{Chu2013,VanGennip2015}). Therefore, in this experiment we are actually evaluating how the error made on the assessment of the QR Code geometry, due to surface and perspective deformations, impacts on the evaluation of the individual black/white pixel bits; and to what extent the native QR Code error correction blocks (based on Reed-Solomon according to the standard) can revert the errors made. 
We then defined a success metric of data readability\citep{TARJAN20141} ($ \mathcal{R} $)  as: 

\begin{equation}
    \mathcal{R} = 100 \cdot \frac{N_{decoded}}{N_{total}} [\%]
\end{equation}

where $ N_{decoded} $ is the number of QR Codes successfully decoded  and $ N_{total} $ is the total amount of QR Codes of a given dataset and transformation. Such a number has a direct connection with the user experience. In a manual reading scenario, it tells us how often the user will have to repeat the picture (e.g. $ \mathcal{R} = 95 \% $ means 5 repetitions out of every 100 uses). In applications with automated QR Code scanning, this measures how long it will take to pick up the data. 


\autoref{fig:readability_dataset} summarizes the readability performance of the four transformations with the three datasets. For the SYNT and FLAT datasets, PRO, CYL and TPS scored 100\% or close. AFF scored only 78\% and 60\% for the SYNT and the FLAT datasets, respectively. This is because AFF lacks the perspective components that PRO and CYL incorporate to address this problem. It is noteworthy that the TPS scored similar to the PRO and CYL for these two datasets: despite TPS does not include perspective directly, it is composed of affine and non-linear terms, and the later ones can fit a perspective deformation. 



This behavior is also confirmed for the segregated data on the SYNT dataset (see~\autoref{fig:readability_SYNT}), where the TPS performed slightly worse on images with a perspective deformation, similarly to the AFF. Also in \autoref{fig:readability_SYNT}, we see that AFF showed its best  performance (97\%) in the subset of images where only affine transformation was present, rendering lower  in the projective ones (70\%). 



\newpage

\begin{figure}[h!t]
    \centering
    \includegraphics[width=0.95\textwidth]{chapters/chapter4/figures/jpg/9.jpg}
    \caption{Data readability ($ \mathcal{R} $) of each dataset (SYNT, FLAT, SURF)  for each transformation method (AFF, PRO, CYL and TPS).}
    \label{fig:readability_dataset}
\end{figure}

\begin{figure}[h!t]
    \centering
    \includegraphics[width=0.95\textwidth]{chapters/chapter4/figures/jpg/10.jpg}
    \caption{Data readability ($ \mathcal{R} $) of the  SYNT dataset, segregated by the kind of deformation (affine or perspective) that the QR Codes were exposed to, for each transformation method (AFF, PRO, CYL and TPS).}
    \label{fig:readability_SYNT}
\end{figure}


\begin{figure}[h!t]
    \centering
    \includegraphics[width=0.95\textwidth]{chapters/chapter4/figures/jpg/11.jpg}
    \caption{Data readability ($ \mathcal{R} $) of the SURF dataset segregated by the kind of deformation (cylindrical or other) that the QR Codes were exposed to, for each transformation method (AFF, PRO, CYL and TPS). }
    \label{fig:readability_SURF}
\end{figure}

\newpage

\autoref{fig:readability_SURF} shows the segregated data for the SURF dataset, neither the AFF nor the PRO transformations decoded almost any QR Code (1\%-2\%). CYL performed well for cylindrical surfaces in the SURF dataset (62\%), but got beaten by the TPS results by 13 points (from 62\% to 75\%). Moreover, CYL scored less than 30\% in images without explicit cylindrical deformations, as expected; while the TPS remained well over 85\%. This is a remarkable result for the TPS, considering that the rest of transformations failed completely at this task. 

Finally, we wanted to benchmark the methodology proposed here with a popular, state-of-the-art decoder like ZBar. To that end, we fed ZBar with all our datasets of images (without pre-processing and with surface geometry corrections made). \autoref{fig:readability_ZBAR} shows that the ZBar implementation (consisting of reading QR Code pixels out of reading each line of the QR Code as one dimensional barcode\citep{zbar}) performs very well with the SYNT dataset. But, in the more realistic smartphone-captured images from FLAT,  ZBar performed poorly, succeeding only in approximately in \nicefrac{3}{4} of the dataset. 


Surprisingly, ZBar was still able to decode some SURF dataset images. We compared these results with a combined sequence of CYL and TPS transformations that can be regarded  as TPS with a fall-back to the CYL method, since CYL has its own fall-back into PRO.  Our solution, improved the good results of ZBar in the  SYNT dataset, obtained a perfect score in the FLAT dataset where ZBar struggles (100\% vs 75\%), and displayed a remarkable advantage (84\% vs 19\%) in decoding the most complex SURF dataset. We can therefore state that the here-proposed methodology outperforms the state-of-the-art when facing complex surface topographies. 

\begin{figure}[h!t]
    \centering
    \includegraphics[width=0.95\textwidth]{chapters/chapter4/figures/jpg/12.jpg}
    \caption{Data readability ($ \mathcal{R} $) of the three datasets (SYNT, FLAT and SURF) when processed with ZBar and our combined CYL and TPS methods.}
    \label{fig:readability_ZBAR}
\end{figure}



