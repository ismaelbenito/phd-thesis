% \newpage
\section{Proposal}
% \section{Theoretical foundations 4}
\label{sec:theoreticalfoundations4}

Here, since a general solution for the decoding of QR Codes placed on top of arbitrary topographies is missing, we present our proposal on this matter based on the thin-plate spline 2D transformation \citep{bookstein-89}. Thin-plate splines (TPS) are a common solution to fit arbitrary data and have been used before in pattern recognition problems: Bazen et al. \citep{Bazen2003} and Ross et al. \citep{ROSS200595} used TPS to match fingerprints; Shi et al. used TPS together with Spatial Transformer Networks to improve handwritten character recognition by correcting arbitrary deformations \citep{Shi2019}, and Yang et al. reviewed the usage of different TPS derivations in the point set registration problem \citep{Yang2015}. 


In order to investigate the advantages of the TPS with respect to former approaches,  we take here the above-mentioned geometric surface fittings as reference cases, namely: (i) affine coplanar transformations (see~\autoref{fig:projections}.a), (ii) projective transformations (see~\autoref{fig:projections}.b), and (iii) cylindrical transformations (see~\autoref{fig:projections}.c). 

\newpage

Then we introduce our proposal for arbitrary surfaces based on (iv) the thin-plate spline 2D transformation (see~\autoref{fig:projections}.d) and benchmark against each other. With all four methods we use a commercial barcode scanner, ZBar \citep{zbar}, to decode the corrected image and observe the impact of each methodology, not just on the geometrical correction but also on the actual data extraction.    


\begin{figure*}[h]
    \centering
    \includegraphics[width=\linewidth]{chapters/chapter4/figures/jpg/3.jpg}
    \caption{Projection of  different surfaces into the capture plane (img) when acquiring images from a digital camera. A QR Code placed on each one of these surfaces will show different deformations (a) an affine (coplanar) plane, (b) a projective (noncoplanar) plane, (c) a cylindrical surface and (d) a thin-plate spline surface, it is continuous and derivable.}
    \label{fig:projections}
\end{figure*}

% \subsection{Feature extraction}
% \label{ss:featureextraction}

% All the image transformations studied in this work require establishing a correspondence between the apparent position of a set of features in the captured image and the actual positions in the underlying 3D surface topography.These pairs of features in both images are called \emph{landmarks}. In QR Codes we can find plenty of predefined graphical features, that can be used as landmarks \citep{iso18004_2015}:


% \textbf{Finder patterns} are the corners of the QR Code, it has three of them to break symmetry and orientate the QR in a scene (see~\autoref{fig:qrcodeparts}.a).

% \textbf{Alignment patterns} are placed inside the QR Code to help in the correction of noncoplanar deformations (see~\autoref{fig:qrcodeparts}.b).

% \textbf{Timing patterns} are located alongside two borders of the QR Code, between a pair of finder patterns, to help in the correction of coplanar deformations (see~\autoref{fig:qrcodeparts}.c).

% The \textbf{fourth corner} is the corner not marked with one of the three finder patterns. It can be found as the crosspoint of the straight extensions of the outermost edges of two finder patterns (see~\autoref{fig:qrcodeparts}.d). It is useful in linear, coplanar and noncoplanar deformations.  

% % \begin{figure*}[h]
% %     \centering
% %     \includegraphics[width=.75\textwidth]{chapters/chapter4/figures/jpg/2.jpg}
% %     \caption{Computer vision patterns featured in a QR Code. (a) Three finder or position patterns, (b) six alignment patterns, (c) two timing patterns and (d) the fourth corner that can be inferred from the external edges of the finder patterns.  }
% %     \label{fig:qrcodeparts}
% % \end{figure*}

% For each surface model, a different number of landmarks will be needed to compute a transformation to correct those deformations (see~\autoref{subsec:proposedtransforms}). In general, more landmarks than the strictly minimum required can be found and  maximum likelihood estimation (MLE) solvers can be used to fit a surface to that data \citep{MarquezNelia2013}. 



\subsection{Fundamentals of projections}
\label{ss:fundamentalsofprojections}

In \autoref{ch:3} we have defined images as mappings from an $ \mathbb{R}^2 $ plane to a scalar field $ \mathbb{R} $, assuming they are grayscale.~\autoref{fig:projections} shows this $ \mathbb{R}^2 $ plane and labels it as \textbf{img}. Let us define a projective transformation of this plane as an application between two planes:

\begin{equation}
 f: \mathbb{R}^2 \to \mathbb{R}^2    
\end{equation}.

Also, let the points $ (x,y) \in \mathbb{R}^2 $ and $ (x', y') \in \mathbb{R}^2 $, we can then define an analytical projective mapping between those two points as: 

\begin{equation}
\label{eq:projectionsmap}
    \begin{split}
    x' = f_x (x, y)  = a_{0,0} \cdot x + a_{0,1} \cdot y + a_{0,2} \\
    y' = f_y (x, y)  = a_{1,0} \cdot x + a_{1,1} \cdot y + a_{1,2}         
    \end{split}
\end{equation} 

\noindent
where $ a_{i,j} \in \mathbb{R} $ are the weights of the projective transform. For a more compact notation, $ (x, y) $ and $ (x', y') $ can be replaced by homogeneous coordinates \citep{casasprojective} $ (p_0, p_1, p_2) \in {\rm P}^2 \mathbb{R} $ and $ (q_0, q_1, q_2) \in {\rm P}^2 \mathbb{R} $, respectively, that allow expressing the transformation in a full matrix notation\footnote{Homogeneous coordinates introduce an additional coordinate $ p_2 $ and $ q_2 $ in our system, which extends the point representation from a plane ($ \mathbb{R}^2 $) to a projective space ($ {\rm P}^2 \mathbb{R} $). We will define that $ p_2 = q_2 = 1 $ only for our landmarks \citep{casasprojective}.}: 

\begin{equation}
\label{eq:projectionmatrix}
    \begin{pmatrix} a_{0,0} & a_{0,1} & a_{0,2} \\
                    a_{1,0} & a_{1,1} & a_{1,2} \\
                    a_{2,0}  & a_{2,1}  &  1 
    \end{pmatrix} \cdot \begin{pmatrix} 
    p_{0} \\  p_{1} \\ 1 
    \end{pmatrix} = \begin{pmatrix} 
    q_{0} \\ q_{1} \\ 1 
    \end{pmatrix}
\end{equation}

Finally, we can simplify this expression by naming our matrices as:

\begin{equation}
\label{eq:projectionproduct}
 \mathbf{A} \cdot \mathbf{P} = \mathbf{Q}   
\end{equation}
 
Here, we will work with four projective transformations: the affine transformation (AFF), the projective transformation (PRO), the cylindrical transformation (CYL) and the thin-plate spline transformation (TPS). We can define all of them as subsets or extensions of projective transformations, so we will have to specifically formulate $ \mathbf{A} $ for each one of them. To do so, we need to know the landmarks in the captured image (acting as vector $ \mathbf{Q} $) and their “correct” location in a non-deformed corrected image (acting as vector $ \mathbf{P} $). 


\subsection{Proposed transformations}
\label{subsec:proposedtransforms}


\textbf{Affine (AFF).} This transformation uses the landmarks to fit a coplanar plane to the capturing device sensor (see~\autoref{fig:projections_affine}). It can accommodate translation, rotation, zoom and shear deformations \citep{casasprojective}. An affine transformation can be expressed in terms of \autoref{eq:projectionmatrix}, only taking  $ a_{2,0} = a_{2,1} = 0 $: 

\begin{marginfigure}
    \centering
    \includegraphics[width=\linewidth]{chapters/chapter4/figures/jpg/projections_affine.jpg}
    \caption{Projection of an affine surface into the capture plane (img) when acquiring images from a digital camera. }
    \label{fig:projections_affine}
\end{marginfigure}


\begin{equation}
\label{eq:projectionmatrix_affine}
    \begin{pmatrix} a_{0,0} & a_{0,1} & a_{0,2} \\
                    a_{1,0} & a_{1,1} & a_{1,2} \\
                    0  & 0  &  1 
    \end{pmatrix} \cdot \begin{pmatrix} 
    p_{0} \\  p_{1} \\ 1 
    \end{pmatrix} = \begin{pmatrix} 
    q_{0} \\ q_{1} \\ 1 
    \end{pmatrix}
\end{equation}

% \break
This yields to a system with only 6 unknown $ a_{i, j} $ weights. Thus, if we can map at least 3 points in the QR Code surface to a known location (e.g. finder pattern centers) we can solve the system for all $ a_{i, j} $ using the expression of \autoref{eq:projectionproduct} with:  

\begin{equation}
    \begin{split} 
        \mathbf{A} &= \begin{pmatrix} 
            a_{0,0} & a_{0,1} & a_{0,2} \\
            a_{1,0} & a_{1,1} & a_{1,2} \\
            0 & 0  &  1 
        \end{pmatrix}  , \\
        \mathbf{P} &= \begin{pmatrix} 
            p_{0,0} & p_{0,1} & p_{0,2}  \\
            p_{1,0} & p_{1,1} & p_{1,2}  \\
            1 & 1 & 1  
        \end{pmatrix} \ and \\
        \mathbf{Q} &=   \begin{pmatrix} 
            q_{0,0} & q_{0,1} & q_{0,2} \\
            q_{1,0} & q_{1,1} & q_{1,2} \\
            1 & 1 & 1  
        \end{pmatrix}.  \end{split}     
\end{equation}


% where $ p_{i,k} $ and $ q_{j,k} $ are the homogeneous coordinates of 3 landmarks. 
\break
\noindent \textbf{Projective (PRO).} This transformation uses landmarks to fit a noncoplanar plane to the capturing plane (see~\autoref{fig:projections_proj}). Projective transformations use \autoref{eq:projectionmatrix} without any further simplification. Also, \autoref{eq:projectionproduct} is still valid, but now we have up to 8 unknown $ a_{i, j} $ weights to be determined. Therefore, we need at least 4 landmarks to solve the system for  $ \mathbf{A} $, then: 

\begin{marginfigure}
    \centering
    \includegraphics[width=\linewidth]{chapters/chapter4/figures/jpg/projections_projective.jpg}
    \caption{Projection of a projective surface into the capture plane (img) when acquiring images from a digital camera. }
    \label{fig:projections_proj}
\end{marginfigure}

\begin{equation}
    \begin{split} 
        \mathbf{A} &= \begin{pmatrix} 
            a_{0,0} & a_{0,1} & a_{0,2} \\
            a_{1,0} & a_{1,1} & a_{1,2} \\
            a_{2,0} & a_{2,1}  &  1 
        \end{pmatrix}  , \\
        \mathbf{P} &= \begin{pmatrix} 
            p_{0,0} & p_{0,1} & p_{0,2} & p_{0,3} \\
            p_{1,0} & p_{1,1} & p_{1,2} & p_{1,3} \\
            1 & 1 & 1 & 1 
        \end{pmatrix} \ and \\
        \mathbf{Q} &=   \begin{pmatrix} 
            q_{0,0} & q_{0,1} & q_{0,2} & q_{0,3} \\
            q_{1,0} & q_{1,1} & q_{1,2} & q_{1,3} \\
            1 & 1 & 1 & 1 
        \end{pmatrix}.  \end{split}     
\end{equation}

Notice that the four points in  must not be collinear three-by-three, if we want the mapping to be invertible \citep{casasprojective}. 


\setlength{\parskip}{1em}
\noindent \textbf{Cylindrical (CYL).} This transformation uses landmarks to fit a cylindrical surface, which can be decomposed into a projective transformation and a pure cylindrical deformation (see~\autoref{fig:projections_cyl}). Thus, the cylindrical transformation extends the projective general transformation (\autoref{eq:projectionsmap}) and adds a non-linear term to the projection: 

\begin{marginfigure}
    \centering
    \includegraphics[width=\linewidth]{chapters/chapter4/figures/jpg/projections_cylindrical.jpg}
    \caption{Projection of a cylindrical surface into the capture plane (img) when acquiring images from a digital camera. }
    \label{fig:projections_cyl}
\end{marginfigure}


\begin{equation}
\label{eq:projectionmap_cyl}
    \begin{split}
        x' = f_x (x, y)  = a_{0,0} \cdot x + a_{0,1} \cdot y + a_{0,2} + w_0 \cdot g(x,y) \\ 
        y' = f_y (x, y)  = a_{1,0} \cdot x + a_{1,1} \cdot y + a_{1,2} + w_1 \cdot g(x,y) 
    \end{split}
\end{equation}

\noindent
where $ g(x, y) $ is the \emph{cylindrical term}, which takes the form of \citep{Li2013ReconstructAO,Li_2019}:  

\begin{equation}
     g(x, y) = \begin{cases} 
        \sqrt{r^2 - (c_0 - x)^2} & if \ \  r^2 - (c_0 - x)^2 \geq 0 \\
        0 & if \ \ r^2 - (c_0 - x)^2 < 0 
    \end{cases} 
\end{equation}

\noindent
where $ r \in \mathbb{R} $ is the radius of the cylinder, and $ c_0 \in \mathbb{R} $ is the first coordinate of any point in the centerline of the cylinder. Now, \autoref{eq:projectionmatrix} becomes extended with another dimension for cylindrical transformations: 

\begin{equation}
     \begin{pmatrix} 
        w_0 & a_{0,0} & a_{0,1} & a_{0,2} \\
        w_1 &  a_{1,0} & a_{1,1} & a_{1,2}\\
        w_2 & a_{2,0}  & a_{2,1}  &  1 
    \end{pmatrix} \cdot \begin{pmatrix} 
        g(p_0, p_1) \\ p_{0} \\ p_{1} \\ 1 
    \end{pmatrix} = \begin{pmatrix} 
        q_{0} \\ q_{1} \\ 1 
    \end{pmatrix} 
\end{equation}

\break
Applying the same reasoning as before, we have now 8 unknown $ a_{i,j} $ plus 3 unknown $ w_{j} $ weights to fit. Equivalent matrices (\autoref{eq:projectionproduct}) for cylindrical transformations need now at least 6 landmarks and look like: 

\begin{equation}
    \begin{split}
        \mathbf{A} &= 
            \begin{pmatrix} 
                w_0 & a_{0,0} & a_{0,1} & a_{0,2} \\
                w_1 & a_{1,0} & a_{1,1} & a_{1,2} \\
                w_2 & a_{2,0} & a_{2,1}  &  1 
            \end{pmatrix}  , \\
        \mathbf{P} &= 
            \begin{pmatrix} 
                g(p_{0,0}, p_{1,0}) & ... & g(p_{0,5}, p_{1,5}) \\
                p_{0,0} & ... & p_{0,5} \\
                p_{1,0} & ... & p_{1,5} \\
                1 & ... & 1 
            \end{pmatrix} \ and \\ 
        \mathbf{Q} &=   
            \begin{pmatrix} 
                q_{0,0} & ... & q_{0,5} \\
                q_{1,0} & ... & q_{1,5} \\
                1 & ... & 1 
        \end{pmatrix}.
    \end{split}
\end{equation}


% \break
\noindent \textbf{Thin-plate splines (TPS).} This transform uses the landmarks as centers of radial basis splines to fit the surface in a non-linear way that resembles the elastic deformation of a metal thin-plate bent around fixed points set at these landmarks \citep{bookstein-89} (see~\autoref{fig:projections_tps}). The radial basis functions are real-valued functions:

\begin{marginfigure}
    \centering
    \includegraphics[width=\linewidth]{chapters/chapter4/figures/jpg/projections_thinplate.jpg}
    \caption{Projection of an arbitrary surface into the capture plane (img) when acquiring images from a digital camera.}
    \label{fig:projections_tps}
\end{marginfigure}

\begin{equation}
    h: [0, \inf) \to \mathbb{R}
\end{equation}

\noindent
that  take into account a metric on a vector space. Their value only depends on the distance to a reference fixed point: 

\begin{equation}
    h_c(v) = h(|| v - c ||)
    \label{eq:tpskernel}
\end{equation}

\noindent
where  $ v \in \mathbb{R}^n $ is the point in which the function is evaluated, $ c \in \mathbb{R}^n $ is the fixed point,  $ h $ is a radial basis function.~\autoref{eq:tpskernel} reads as "$ h_c(v) $ is a kernel of $ h $ in $ c $ with the metric $ ||\cdot|| $". Similarly to cylindrical transformations (\autoref{eq:projectionmap_cyl}), we extended the affine transformation (\autoref{eq:projectionsmap}) with $ N $ nonlinear spline terms:

\begin{equation}
    \begin{split}
        x' &= f_x (x, y)  = a_{0,0} \cdot x + a_{0,1} \cdot y + a_{0,2} + \sum_{k=0}^{N-1} w_{0,k} \cdot h_k ((x, y)) \\
        y' &= f_y (x, y)  = a_{1,0} \cdot x + a_{1,1} \cdot y + a_{1,2} + \sum_{k=0}^{N-1} w_{1,k} \cdot h_k ((x, y)) 
    \end{split}
\end{equation}

\noindent
where $ w_{j,k} $ are the weights of the spline contributions, and $ h_k (x, y) $ are kernels of $ h $ in $ N $ landmark points. 

\break

These radial basis function remains open to multiple definitions. Bookstein \citep{bookstein-89} found  that  the second order polynomial radial basis function is the proper function to compute splines in $ \mathbb{R}^2 $ mappings in order to minimize the bending energy, and mimic the elastic behavior of a metal thin-plate. Thus, let $ h $ be:

\begin{equation}
    h(r) = r^2 \ln(r)
\end{equation}

\noindent
with the corresponding kernels computed using the euclidean metric:

\begin{equation}
     || (x, y) - (c_x, c_y) || = \sqrt{(x -  c_x)^2 + (y -  c_y)^2}
\end{equation}


% \break 
Finally, in matrix representation, terms from \autoref{eq:projectionproduct} are expanded as follows:

% \vspace{1cm}
\begin{equation}
    \begin{split}
         \mathbf{A} &= 
            \begin{pmatrix} 
                w_{0,0} & ... & w_{0, N-1}  & a_{0,0} & a_{0,1} & a_{0,2} \\
                w_{1,0} & ...  & w_{1,N-1} & a_{1,0} & a_{1,1} & a_{1,2} 
            \end{pmatrix}  , \\
        \mathbf{P} &= 
            \begin{pmatrix} 
                h_0(p_{0,0}, p_{1,0}) & ... & h_0(p_{0,N-1}, p_{1,N-1}) \\
                \vdots & ... & \vdots \\
                h_{N-1}(p_{0,0}, p_{1,0}) & ... & h_{N-1}(p_{0,N-1}, p_{1,N-1}) \\
                p_{0,0} & ... & p_{0,N-1} \\
                p_{1,0} & ... & p_{1,N-1}  \\
                1 & ... & 1 
            \end{pmatrix} \ and \\
        \mathbf{Q} &=   
            \begin{pmatrix} 
                q_{0,0} & ... & q_{0,N-1} \\
                q_{1,0} & ... & q_{1,N}  
            \end{pmatrix}. 
    \end{split}
    \label{eq:qr_tps_APQ}
\end{equation}

% \vspace{1cm}
First, notice that only $ a_{i,j} $ affine weights are present, since this definition does not include a perspective transformation. Second, in contrast with previous transformations, this system is unbalanced: we have a total of $ 2N + 6 $ weights to compute ($ 2N $ $ w_{j,k} $ spline weights plus 6  $ a_{i,j} $ affine weights), however, we only have defined $ N $ landmarks. 

In the previous transformations,  we used additional landmarks to solve the system, but Bookstein imposed an additional condition of the spline contributions: the sum of $ w_{j,k} $ coefficients to be \0, and also their cross-product with the $ p_{i,k} $ landmark coordinates \citep{bookstein-89}. With such a condition, spline contributions tend to \0 at infinity, while affine contributions prevail. This makes our system of equations solvable and can be expressed as: 

% \vspace{1cm}
\begin{equation}
    \begin{pmatrix} 
         w_{0,0} & ... & w_{0, N-1} \\
         w_{1,0} & ...  & w_{1,N-1} 
    \end{pmatrix}  \cdot \begin{pmatrix} 
        p_{0,0} & ... & p_{0,N-1} \\
        p_{1,0} & ... & p_{1,N-1} \\
        1 & ... & 1 
    \end{pmatrix}^T  =  0 
\end{equation}

\break
